# > System
import os
from os import getenv
import sys
from sys import path, argv, stderr

from pathlib import Path
import subprocess
from subprocess import Popen, PIPE

# import subprocess
from json import load as load_json
from time import sleep


from app.core.utilities.blender_console import BlenderConsole


# > Web
import platform
from queue import Queue, Empty

# > Own libraries
from app.core.utilities.blender_console import BlenderConsole
from app.core.setting import Setting


# Fix Module
module = Path(__file__).parent.resolve()
path.append(str(module))
cli_interface = BlenderConsole()
setting = Setting()

platform_os = platform.system().lower()
ON_POSIX = "posix" in sys.builtin_module_names

blendfile = module / "app" / "blender_scenes" / "webready_bake_2.93.1_2.2.blend"
pythonfile = module / "app" / "core" / "blender.py"

# adjust path according to the platform
if platform_os == "linux" or platform_os == "linux2":
    blender_executable = "blender"
    python_path = "/bin/2.93/python/bin/python3.9"
elif platform_os == "darwin":
    # for mac user.
    blender_folder = module / "blender_dir" / "Blender"
    blender_executable = blender_folder/ "Contents/MacOS/Blender"
    python_folder = blender_folder / "Contents/Resources/2.93/python/bin"
    python_executable = python_folder.joinpath("python3.9")
else:
    blender_folder = module / "blender_dir" / "Blender"
    blender_executable = blender_folder / "blender.exe"
    python_folder = blender_folder.joinpath("2.93/python")
    python_executable = python_folder.joinpath("bin/python.exe")

# TODO: put stuff in separate, sensible files (?)


def enqueue_output(out, queue):
    for line in iter(out.readline, b""):
        queue.put(line)
    out.close()


def encode_output(response):
    response = response.split("@")
    dict_response = {}
    for index, item in enumerate(response):
        if item.startswith("--"):
            if index != len(response) - 1 and not response[index + 1].startswith("--"):
                dict_response[item[2:]] = response[index + 1]
    return dict_response


def print_response(response):
    dict_response = encode_output(response)
    cli_interface.update_interface(dict_response)
    cli_interface.print()


# #~~~~~~~~~~~~~~~ end stuff


def start_blender(args=argv[1:]):
    arguments = ""
    for argument in args:
        arguments += str(argument) + " "
    try:

        call = f"{blender_executable} --background {os.path.abspath(blendfile)} --python {pythonfile} -- {arguments}"
        #call = f"{blender_executable} {os.path.abspath(blendfile)} --python {pythonfile} -- {arguments}"
        process = subprocess.Popen(
            args=call,
            shell=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
            stderr=stderr,
        )

        if os.name == "nt":
            clear = lambda: os.system("cls")
            clear()
        cli_interface.print()
        for line in iter(process.stdout.readline, b""):
            if len(line) > 0 and line[0] == "@":
                # Updating Interface
                if os.name == "nt":
                    clear()
                # print(line)
                print_response(line)
                if "END_PIPELINE" in line:
                    if os.name == "nt":
                        clear()
                    print_response(line)

                    # Ending Program
                    break

            if len(line) > 0 and line == "$":
                print(line[1::])

    except Exception as e:
        print(e)


def main():
    start_blender()


if __name__ == "__main__":
    main()
