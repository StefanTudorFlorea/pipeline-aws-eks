#----------------------------------------------------------------------------------------------------------------------
# BUILD: DOCKER HUB IMAGE
#----------------------------------------------------------------------------------------------------------------------
# Build and push to the docker hub as private repo using account 'developer@roooom.com'
#   - we use ':latest' tag for 'development' branch
#   - we use ':preview' tag for 'main' branch
# 
# How it works:
#   - uses the following already defined variables: DOCKER_USER, DOCKER_HOST, DOCKER_TLS_CERTDIR, DOCKER_DRIVER
#   - defined in the gitlab env hidden section: DOCKER_HUB_TOKEN (sensitive var must always be defined in gitlab)
#   - build the current directory and uses different tag depending on the branch
#   - these images are build only for 'master' and 'development' branches which are used by developers    
#----------------------------------------------------------------------------------------------------------------------
.build docker hub image:
    stage: build

    # get docker image and make sure service is running
    image: docker:stable
    services:
        - docker:dind

    variables:
        IMAGE_TAG: IMAGE_TAG
        IMAGE_NAME: test-app

    # we use different tags depending on the branch
    rules:
        - if: $CI_COMMIT_BRANCH == 'main'
          variables:
            IMAGE_TAG: latest

        - if: $CI_COMMIT_BRANCH == 'development'
          variables:
            IMAGE_TAG: preview

    # login to docker hub
    before_script:
        - docker login -u ${DOCKER_USER} -p ${DOCKER_HUB_TOKEN}

    # build and push image
    script:
        - docker build -t ${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG} .
        - docker push ${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG}

#----------------------------------------------------------------------------------------------------------------------
# BUILD: AWS ECR IMAGE
#----------------------------------------------------------------------------------------------------------------------
# Build and push to the AWS ECR private image repo. Uses incremental tag as best practice
# The incremental tag is needed to accurately keep track of which version is running in which environment
#
# How it works:
#   - before building the image we use dependency injection to inject version information about the current commit
#   - this info is important to cross check that correct image has been deployed
#   - uses these already defined vars: AWS_ECR_REGISTRY, AWS_DEFAULT_REGION, INFO_FILE
#   - uses secret vars defined in gitlab: AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID
#----------------------------------------------------------------------------------------------------------------------
build aws ecr image:
    stage: build

    only:
        - main           # used for 'production' env
        - development    # used for 'staging' env
        - merge_requests # used for dynamic env and app review

    # get the official aws-cli image and make sure docker is running
    services:
        - docker:dind
    image:
        name: amazon/aws-cli
        entrypoint: [""]

    # config
    variables:
        IMAGE_TAG: ${CI_PIPELINE_IID} # incremental tag
        INFO_FILE: server.py          # file in which to inject deployment information

    # aws-cli does not come with docker installed
    before_script:
        - amazon-linux-extras install docker

    script:
        # inject deployment information into ${INFO_FILE}
        - sed -i "s/%%CI_PIPELINE_IID%%/${CI_PIPELINE_IID}/" ${INFO_FILE}
        - sed -i "s/%%CI_COMMIT_SHORT_SHA%%/${CI_COMMIT_SHORT_SHA}/" ${INFO_FILE}
        - sed -i "s/%%CI_COMMIT_BRANCH%%/${CI_COMMIT_BRANCH}/" ${INFO_FILE}
        - sed -i "s/%%CI_COMMIT_TIMESTAMP%%/${CI_COMMIT_TIMESTAMP}/" ${INFO_FILE}

        # build and deploy the image
        - docker login -u ${DOCKER_USER} -p ${DOCKER_HUB_TOKEN}
        - docker build -t $AWS_ECR_REGISTRY/${IMAGE_NAME}:${IMAGE_TAG} .
        - docker logout
        - aws ecr get-login-password | docker login --username AWS --password-stdin $AWS_ECR_REGISTRY
        - docker push $AWS_ECR_REGISTRY/${IMAGE_NAME}:${IMAGE_TAG}
    