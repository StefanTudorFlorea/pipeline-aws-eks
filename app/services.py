import json
import logging.handlers
import os
from os import path

from app.api_services import backends
from app.api_services.pipeline import Pipeline
from app.core.utilities import keywords
from app.api_services.blender import BlenderTask
from app.api_services.values import ConstTask
from app.api_services.future import FutureTask
from app.api_services.utilities import module, store, logs, get_log_formatter, split_id
from app.api_services.srv_types import (
    WebReadyArgs,
    PolycountArgs,
    PreflightArgs,
    RooomReadyArgs,
    ObjectStates,
)
from app.api_services.srv_types import DictModel
from app.api_services.download import DownloadTask
from app.api_services.sendback import SendTask
from app.api_services.senderror import SendErrorTask
from app.api_services.utilities import logs, open_dir

import pathlib
from app.core.log import Log


class BaseSrv:
    def __init__(
        self,
        name,
        debug,
        id,
        token,
        domain,
        callback_url=None,
        status=ObjectStates.WAITING,
        upload=None,
        root_dir=None,
        cb_task_start=None,
        cb_task_err=None,
        cb_task_done=None,
        download_check=None,
        cmds=[],
    ):
        self.name = name
        self.debug = debug
        self.id = id
        backend_id, mode = split_id(id)
        self.logger = Log()
        # logger.set_file(logger_name="pipeline","logs / f'{id}_log.txt'")
        # logger.set_url(logger_name="backend", host=domain, url=callback_url)

        # self.logger = logging.getLogger(f'Server.{id}')
        self.logfile = logs / f"{id}.log"
        for handler in self.logger["backend"].handlers:
            if isinstance(handler, logging.handlers.HTTPHandler) and self.response_url:
                handler.host = ""
                handler.url = self.response_url
            if isinstance(handler, logging.FileHandler) and self.logfile:
                # handler.baseFilename = str(logs / f'{id}_log.txt')
                handler.baseFilename = self.logfile

        self.logger.info("Configuring service", loggernames="backend")
        self.logger.warning("Test error", loggernames="backend")
        # if len(self.logger.handlers) == 0:
        #    handler = logging.FileHandler(self.logfile, mode='w')
        #    handler.setFormatter(get_log_formatter())
        #    self.logger.addHandler(handler)
        # handler = logging.StreamHandler()
        # handler.setFormatter(utils.get_log_formatter())
        # self.logger.addHandler(handler)
        # self.logger.setLevel(logging.DEBUG)

        self.cb_task_start = cb_task_start
        self.cb_task_err = cb_task_err
        self.cb_task_done = cb_task_done

        if root_dir is None:
            root_dir = module
        self.store = open_dir(root_dir, "store", keep_files=True)
        self.logs = open_dir(root_dir, "logs", keep_files=True)

        if upload is None:

            # data will be downloaded from backend via domain, ID and token
            self.receive_url = (
                f"{domain}/resourceBackend/autobakeFile/{backend_id}?token={token}"
            )
            self.receive_url = self.receive_url.replace("localhost", "rooom.com")
            if not self.receive_url.startswith("http"):
                self.receive_url = "https://" + self.receive_url
        else:
            # data will be uploaded in a zip file - upload can be an url or a file descriptor
            self.receive_url = upload

        if callback_url is None:
            if domain is None:
                self.response_url = None
            else:
                self.response_url = f"{domain}/resourceBackend/receiveAutobake"
        else:
            self.response_url = callback_url
        self.response_url = self.response_url.replace("localhost", "rooom.com")
        if self.response_url is not None and not self.response_url.startswith("http"):
            self.response_url = "https://" + self.response_url

        self.status = status
        self.pipeline = Pipeline(id)
        self.cmds = cmds

        self.import_folder = open_dir(
            store, append_path=f"input/{backend_id}", keep_files=True
        )
        # TODO: suffix
        self.settings_suffix = f"_"
        self.export_folder = open_dir(
            store, append_path=f"output/{id}", keep_files=debug
        )
        if self.status == ObjectStates.WAITING:
            task = DownloadTask(
                id=id,
                from_url=self.receive_url,
                temp_path=open_dir(append_path="Temp", keep_files=True)
                / f"{backend_id}.zip",
                extract_path=self.import_folder,
                check_mode=download_check,
            )
            task.set_handlers(self.on_task_start, self.on_task_err, self.on_task_done)
            self.pipeline.append(task)
        main_task = self.create_task()
        main_task.set_handlers(self.on_task_start, self.on_task_err, self.on_task_done)
        self.pipeline.append(main_task)
        if self.response_url is not None:
            if self.status.value < ObjectStates.SENT.value:
                task = SendTask(
                    id=id,
                    from_path=self.export_folder,
                    to_url=self.response_url,
                    zipped=not isinstance(self, PolycountSrv),
                )
                task.set_handlers(
                    self.on_task_start, self.on_task_err, self.on_task_done
                )
                self.pipeline.append(task)
        self.logger.info("Service created")

    def run(self):
        self.pipeline.execute()
        for handler in self.logger["backend"].handlers:
            if isinstance(handler, logging.FileHandler):
                handler.close()
        self.logger.info("Service finished")

    def on_task_start(self, task, status):
        self.status = status
        if self.cb_task_start:
            self.cb_task_start(task, status)

    def on_task_err(self, task, err: Exception = None):
        for h in self.logger.handlers:
            h.flush()
        self.logger.info(
            f"Sending error data {self.logfile} to {self.response_url}",
            loggernames="backend",
        )
        self.status = ObjectStates.ERROR
        SendErrorTask(
            id=self.id,
            logfile=self.logfile,
            to_url=self.response_url,
            task=task.__class__.__name__,
            error=err,
        ).execute()
        if self.cb_task_err:
            self.cb_task_err(task, err)
        else:
            raise err

    def on_task_done(self, task, result):
        self.status, time = result
        if not isinstance(self.status, ObjectStates):
            # Tasks returns a value and not the new state
            value = self.status
            # TODO: process returned value
            self.status = ObjectStates.RAN
            result = (ObjectStates.RAN, time)
        if self.cb_task_done:
            self.cb_task_done(task, result)

class RooomReadySrv(BaseSrv):
    def __init__(
        self,
        debug,
        id,
        token,
        domain,
        callback_url=None,
        status=ObjectStates.WAITING,
        upload=None,
        root_dir=None,
        cb_task_start=None,
        cb_task_err=None,
        cb_task_done=None,
        cmds=[]
    ):
        super(RooomReadySrv, self).__init__(
            "Rooomready",
            debug,
            id,
            token,
            domain,
            callback_url,
            status,
            upload,
            root_dir,
            cb_task_start,
            cb_task_err,
            cb_task_done,
            cmds=cmds,
        )

    def create_task(self):
        return BlenderTask(
            srv_name=self.name,
            id=self.id,
            pipeline_args=self.cmds+[
                "--rooomready"
            ],
            input_folder=self.import_folder,
            export_folder=self.export_folder,
            log_args={"file": self.logfile, "url": self.response_url},
        )
class WebreadySrv(BaseSrv):
    def __init__(
        self,
        debug,
        id,
        token,
        domain,
        callback_url=None,
        status=ObjectStates.WAITING,
        upload=None,
        root_dir=None,
        cb_task_start=None,
        cb_task_err=None,
        cb_task_done=None,
        cmds=[],
    ):
        super(WebreadySrv, self).__init__(
            "Autobake",
            debug,
            id,
            token,
            domain,
            callback_url,
            status,
            upload,
            root_dir,
            cb_task_start,
            cb_task_err,
            cb_task_done,
            cmds=cmds,
        )

    def create_task(self):
        return BlenderTask(
            srv_name=self.name,
            id=self.id,
            input_folder=self.import_folder,
            export_folder=self.export_folder,
            # blender_executable=blender_path,
            # blendfile=module / 'blendfiles/webready_bake_2.93.1_2.0.blend',
            # pyfile=module / 'scripts/main.py',
            pipeline_args=self.cmds,
            log_args={"file": self.logfile, "url": self.response_url},
        )


class PolycountSrv(BaseSrv):
    def __init__(
        self,
        debug,
        id,
        token,
        domain,
        callback_url=None,
        status=ObjectStates.WAITING,
        upload=None,
        root_dir=None,
        cb_task_start=None,
        cb_task_err=None,
        cb_task_done=None,
    ):
        super(PolycountSrv, self).__init__(
            "Polycount",
            debug,
            id,
            token,
            domain,
            callback_url,
            status,
            upload,
            root_dir,
            cb_task_start,
            cb_task_err,
            cb_task_done,
            download_check="single_geometry",
        )

    def create_task(self):
        return FutureTask(id=self.id, fn=self.run_later)

    def run_later(self):
        geometry_files = [
            f
            for f in os.listdir(self.import_folder)
            if f.endswith(tuple(keywords.supported_geometries))
        ]
        try:
            input_file = pathlib.Path(self.import_folder) / geometry_files[0]
        except:
            raise Exception("Geometries are empty")
        # self.json_path = self.export_folder / f'pc_{self.id}.json'
        self.json_path = self.export_folder / f"polycount.json"
        self.logger.info("-- Counting start --", loggernames="backend")
        if input_file.suffix == ".obj":
            polycount = len(
                [l for l in input_file.read_text().splitlines() if l.startswith("f")]
            )
            json.dump({"polycount": polycount}, open(self.json_path, "w"))
            return ConstTask(id=self.id, polycount=polycount).execute()
        if input_file.suffix == ".stl" and input_file.read_bytes().decode(
            "utf-8", errors="ignore"
        ).startswith("solid"):
            polycount = len(
                [
                    l
                    for l in input_file.read_text().splitlines()
                    if l.startswith("facet")
                ]
            )
            json.dump({"polycount": polycount}, open(self.json_path, "w"))
            return ConstTask(id=self.id, polycount=polycount).execute()
        elif input_file.suffix in keywords.supported_geometries:
            return BlenderTask(
                srv_name=self.name,
                id=self.id,
                pipeline_args=["--polycount", "--write_info", self.json_path],
                input_folder=self.import_folder,
                export_folder=self.export_folder,
                log_args={"file": self.logfile, "url": self.response_url},
            ).execute()


class PreviewSrv(BaseSrv):
    def __init__(
        self,
        debug,
        id,
        token,
        domain,
        callback_url=None,
        status=ObjectStates.WAITING,
        upload=None,
        root_dir=None,
        cb_task_start=None,
        cb_task_err=None,
        cb_task_done=None,
    ):
        super(PreviewSrv, self).__init__(
            "Preview",
            debug,
            id,
            token,
            domain,
            callback_url,
            status,
            upload,
            root_dir,
            cb_task_start,
            cb_task_err,
            cb_task_done,
        )

    def create_task(self):
        json_path = self.export_folder / f"info.json"
        img_path = self.export_folder / f"preflight.png"
        return BlenderTask(
            srv_name=self.name,
            id=self.id,
            pipeline_args=[
                "--preview",
                "True",
                "-use_gpu",
                "--render_image",
                str(img_path),
            ],
            input_folder=self.import_folder,
            export_folder=self.export_folder,
            log_args={"file": self.logfile, "url": self.response_url},
        )



