from sys import platform
from os import makedirs
from requests import get, post
import pathlib
import shutil
import logging

module = pathlib.Path(__file__).parent.resolve()
store = module / "store"
logs = module / "logs"
platform_os = platform.lower()
if platform_os in ["linux", "linux2"]:
    blender_path = "blender"
elif platform_os == "darwin":
    # for mac user.
    blender_path = "/Applications/Blender.app/Contents/MacOS/Blender"
else:
    blender_path = module.parent.parent / "blender-2.93.1-windows-x64/blender.exe"


def download_file(url, filepath, chunk_size=1024):
    """
    Downloads file from given url to filepath
    """
    #
    response = get(url, stream=True)
    if response.status_code == 404:
        raise Exception(
            f"Server replied with {response.status_code}, file cannot be found"
        )
    elif response.status_code != 200:
        raise Exception(f"Server replied with {response.status_code}")

    filepath = str(filepath)
    file = open(filepath, "wb")
    for chunk in response.iter_content(chunk_size=chunk_size):
        file.write(chunk)
    file.close()


def open_dir(root_path=None, append_path=None, keep_files=False):
    """
    Creates a Path from root_path and append_path. If folder for path already exists, files in that
    folder can be removed optionally. If folder does not exist, it will be created
    :param root_path: Path for root. <App base>/store if None
    :param append_path: Path to append to the root path
    :param keep_files: If true, existing folder for constructed path will be emptied
    :return: The constructed path root_path/append_path
    """
    folder = store if root_path is None else root_path
    if isinstance(folder, str):
        folder = pathlib.Path(folder)
    if append_path is not None:
        folder = folder / append_path
    if folder.exists() and not keep_files:
        shutil.rmtree(folder, ignore_errors=True)
    if not folder.exists():
        makedirs(folder)
    return folder


def get_log_formatter():
    return logging.Formatter(
        fmt="%(asctime)s %(name)s [%(levelname)s] %(message)s",
        datefmt="%d.%m.%Y %H:%M:%S",
    )


def split_id(id_mode):
    for suffix in ["_autobake", "_preview", "_polycount", "_rooomready"]:
        if id_mode.endswith(suffix):
            return id_mode[: -len(suffix)], suffix[1:]
    return id_mode, None
