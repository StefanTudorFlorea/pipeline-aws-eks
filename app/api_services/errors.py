class ProcessError(RuntimeError):
    def __init__(self, message=None):
        super(ProcessError, self).__init__()
        self.message = message
