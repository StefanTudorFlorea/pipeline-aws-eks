from __future__ import annotations
from abc import ABC, abstractmethod


class AbstractTask(ABC):
    """
    The Task Interface declares a set of visiting methods that correspond to
    model classes. The signature of a visiting method allows the task to
    identify the exact class of the model that it's dealing with.

    Args:
        None
    """

    def __init__(self):
        self.priority = 0
        self.on_start = None
        self.on_err = None
        self.on_done = None

    def set_handlers(self, cb_start, cb_err, cb_done):
        self.on_start = cb_start
        self.on_err = cb_err
        self.on_done = cb_done

    def poll(self) -> bool:
        return True

    @abstractmethod
    def execute(self) -> None:
        """[Execute a collection of actions bundled in a task]"""
        pass
