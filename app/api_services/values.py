from app.api_services.srv_types import ObjectStates
from app.api_services.abstract import AbstractTask


class ConstTask(AbstractTask):
    def __init__(self, id, **kwargs):
        super(ConstTask, self).__init__()
        self.id = id
        self.status = ObjectStates.RUNNING
        self.values = kwargs

    def execute(self):
        return self.values
