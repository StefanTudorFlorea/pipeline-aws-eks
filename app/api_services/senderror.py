from os import path

from app.api_services import dispatcher
from app.api_services.abstract import AbstractTask
from app.api_services.errors import ProcessError
from app.api_services.srv_types import ObjectStates
from app.api_services.utilities import logs, store, split_id
import requests

from app.core.log import Log


class SendErrorTask(AbstractTask):
    def __init__(self, id, logfile, to_url, task, error=None):
        super(SendErrorTask, self).__init__()
        self.id = id
        self.status = None
        self.url = to_url
        self.err_file = logfile
        self.task = task
        self.error = error
        self.logger = Log()

    def execute(self):
        self.logger.info(
            f"sending errors to {self.url}", loggernames=["main", "backend"]
        )
        backend_id, mode = split_id(self.id)
        rows = dispatcher.Dispatcher.get_instance().storage.get(self.id)
        self.duration = rows.iloc[0].CalcTime
        server_data = rows.iloc[0].Ext
        data = {
            "objectId": backend_id,
            "error": "Error unknown" if self.error is None else str(self.error),
            "message": f"Error in {self.task}",
            "duration": self.duration,
            "server_data": server_data,
        }
        if isinstance(self.error, ProcessError):
            data["error"] = self.error.message
            self.logger.debug(self.error.message, loggernames=["main", "backend"])
        for retries in range(1, 4):
            try:
                response = requests.post(self.url, data=data)
                self.logger.debug(response, loggernames=["main", "backend"])
                break
            except Exception as send_err:
                self.logger.error(
                    f"{retries}/3: Error sending: {send_err}",
                    loggernames=["main", "backend"],
                )
                if retries == 3:
                    raise send_err
        self.logger.info("Error sent", loggernames=["main", "backend"])
        # return ObjectStates.SENT
        return ObjectStates.ERROR
