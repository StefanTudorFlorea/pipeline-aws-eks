import os
import sys
import traceback
import time
from app.core.log import Log


class Pipeline:
    def __init__(self, id):
        self.queue = []
        self.id = id
        self.logger = Log()

    def append(self, task):
        self.queue.append(task)

    def execute(self):
        index = 0
        task_count = len(self.queue)
        while len(self.queue) > 0:
            index += 1
            task = self.queue.pop(0)
            time_start = time.perf_counter()
            self.logger.info(
                f"current_task: {task.__class__.__name__} {index}/{task_count}",
                loggernames=["main", "backend"],
            )
            result = task.status
            try:
                if not task.poll():
                    self.logger.info(
                        "Task poll returned false - skipping",
                        loggernames=["main", "backend"],
                    )
                    continue
                else:
                    if task.on_start:
                        task.on_start(task, task.status)
                    result = task.execute()
            except Exception as e:
                self.logger.error(
                    f"Failed to execute task {task.__class__.__name__}: {e}",
                    loggernames=["main", "backend"],
                )
                self.logger.debug(
                    traceback.format_exc(), loggernames=["main", "backend"]
                )
                self.logger.warning(
                    "Aborting pipeline", loggernames=["main", "backend"]
                )
                self.queue.clear()
                if task.on_err:
                    task.on_err(task, e)
                else:
                    raise e
            finally:  # post execution
                time_elapsed = time.perf_counter() - time_start
                self.logger.info(
                    f"Task {type(task).__name__} finished in {round(time_elapsed, 3)}s",
                    loggernames=["main", "backend"],
                )
                if task.on_done:
                    task.on_done(task, (result, time_elapsed))
        self.logger.info("Pipeline completed", loggernames=["main", "backend"])
