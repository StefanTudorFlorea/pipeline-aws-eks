from enum import Enum
from typing import Optional, List, Any
from pydantic import BaseModel, Field, conint
from pydantic.types import Tuple
import json

presets = {
    "normal": {
        "settings": {
            "--texture_format": "jpg",
            "--shadow_resolution": "512",
            "--normal_format": "png",
        },
        "operations": [
            "--gpu",
            "--flatten",
            "--decimate",
            "--filter_meshes",
            "--join",
            "--parent_additional_geometries_to_main_mesh",
            "--remove_doubles",
            "--edge_split",
            "--recenter",
            "--smooth",
            "--bake_shadow",
            # "--normalize_uv_space",
            # "--validate_uvs",
            "--export_geometry_as_babylon",
            "--misc_edit_babylon",
        ],
    }
}


class ObjectStates(Enum):
    SRV_ERROR = -2
    ERROR = -1
    WAITING = 0
    DOWNLOADING = 1
    DOWNLOADED = 2
    RUNNING = 3
    RAN = 4
    SENDING = 9
    SENT = 10


# class Environment(str, Enum):
#    production = "production"
#    development = "development"


class Presets(str, Enum):
    normal = "normal"


class TextureFormats(str, Enum):
    jpeg = "jpg"
    jpg = "jpg"
    png = "png"


class ExportFormats(str, Enum):
    obj = "obj"
    glb = "glb"
    babylon = "babylon"


class DictModel(BaseModel):
    """
    Allows subclasses to update data via dictionary. Intended to be used with parsed json data
    """

    def update(self, dict: dict):
        for k, v in dict.items():
            print(f"Updating {k} in {self.__class__.__name__} with {v}")
            if hasattr(self, k):
                if isinstance(self.__getattribute__(k), DictModel):
                    self.__getattribute__(k).update(v)
                else:
                    self.__setattr__(k, v)


# "backend.dev.rooom.com"
# "OtyGl0qbGJoSCGkXfBuZ1oJmhQ0nJCdRux_O5kIUvQ2PoKk6iaDnFpVJgvnfpnInkB"


class ExtraOptions(DictModel):
    """
    Extra parameters sent from backend
    """

    objectId: Any = Field(
        "61cae8469b8b0a10cf124e52",
        description="Id of object<br/>Required for POST method as data is loaded from backend based on this id",
    )
    domain: str = Field(None, description="Backend domain")
    name: Optional[str] = Field("Model", description="Name of the model")
    authtoken: str = Field(None, description="Token to access backend")
    server_data: Optional[str] = Field(
        "", description="Additional data passed from backend as json"
    )


class MergeMaterialsArgs(DictModel):
    """
    Bake multiple materials of object into one material
    """

    format: Optional[TextureFormats] = Field(
        TextureFormats.jpg, description="File format of target texture"
    )
    resolution: Optional[conint(ge=1024, le=8 * 1024, multiple_of=1024)] = Field(
        1024, description="Resolution of target texture"
    )
    bake_resolution: Optional[conint(ge=1024, le=6 * 1024, multiple_of=1024)] = Field(
        1024, description="Resolution used during baking"
    )
    island_margin: Optional[float] = Field(
        10.0, description="Island margin for UVPackMaster"
    )


class ImportArgs(DictModel):
    materials: Optional[bool] = Field(True, description="Import materials")
    merge_materials: Optional[MergeMaterialsArgs] = Field(
        None, description="If given, enables material merging"
    )
    join_geometry: Optional[bool] = Field(True, description="Join multiple geometries")


class CompactArgs(DictModel):
    max_cage_offset: Optional[float] = Field(0.1, description="TODO doc")
    tilesize: Optional[int] = Field(16, description="TODO doc")
    bake_margin: Optional[int] = Field(5, description="TODO doc")
    samples: Optional[int] = Field(1, description="TODO doc")
    target_polycount: Optional[int] = Field(120000, description="TODO doc")


class ResizeArgs(DictModel):
    scale: Optional[float] = Field(1.0, description="TODO doc")


class RotateArgs(DictModel):
    x: Optional[float] = Field(0.0, description="Rotation around x-axis")
    y: Optional[float] = Field(0.0, description="Rotation around y-axis")
    z: Optional[float] = Field(0.0, description="Rotation around z-axis")


class EdgeSplitArgs(DictModel):
    angle: Optional[float] = Field(80.0, description="TODO doc")


class RemoveDoublesArgs(DictModel):
    distance: Optional[float] = Field(0.0001, description="TODO doc")


class GeometryArgs(DictModel):
    resize: Optional[ResizeArgs] = Field(ResizeArgs(), description="TODO doc")
    rotate: Optional[RotateArgs] = Field(RotateArgs(), description="Rotation")
    smooth: Optional[bool] = Field(False, description="TODO doc")
    edge_split: Optional[EdgeSplitArgs] = Field(
        None, description="If set, enables edge split"
    )
    remove_doubles: Optional[RemoveDoublesArgs] = Field(
        None, description="If set, enables edge split"
    )
    recenter: Optional[bool] = Field(True, description="TODO doc")
    recalc_normals: Optional[bool] = Field(False, description="TODO doc")
    remove_invisible: Optional[bool] = Field(False, description="TODO doc")
    compact: Optional[CompactArgs] = Field(
        CompactArgs(), description="If set, enables compact task"
    )


class ShadowArgs(DictModel):
    margin: Optional[float] = Field(0.6, description="Bake margin for shadow")
    format: Optional[TextureFormats] = Field(
        TextureFormats.jpg, description="File format of shadow texture"
    )
    resolution: Optional[conint(ge=1024, le=8 * 1024, multiple_of=1024)] = Field(
        1024, description="Resolution of shadow texture"
    )
    validation: Optional[bool] = Field(
        False, description="Check if shadow map is not completely black"
    )




class MaterialsArgs(DictModel):
    normalize_uv: Optional[bool] = Field(False, description="TODO doc")
    validate_uvs: Optional[bool] = Field(False, description="TODO doc")
    shadow: Optional[ShadowArgs] = Field(
        None, description="If set, creates a shadow plane and bakes shadow map on it"
    )


class AutbakeExportArgs(DictModel):
    # geometry_format: Optional[ExportFormats] = Field(ExportFormats.babylon, description='Format of returned object')
    geometry_as_babylon: Optional[bool] = Field(
        True, description="Format of returned object"
    )
    edit_babylon: Optional[bool] = Field(True, description="TODO")
    default_texture_format: Optional[TextureFormats] = Field(
        TextureFormats.jpg, description="Default file format of exported textures"
    )
    default_texture_resolution: Optional[
        conint(ge=1024, le=8 * 1024, multiple_of=1024)
    ] = Field(1024, description="Default resolution of exported textures")


class RooomReadyArgs(DictModel):
    callback_url: Optional[str] = Field(None)
    preset: Presets = Field(
        Presets.normal.value, description="Name of preconfigured pipeline"
    )
    extras: Optional[ExtraOptions] = Field(None)
    commands: List[str] = Field(
        [],
        description="Settings/Command additionally used to the ones selected by preset",
    )
    imports: Optional[ImportArgs] = Field(None, description="Configure import")
    geometry: Optional[GeometryArgs] = Field(
        GeometryArgs(), description="Tasks to be performed related to meshes"
    )
    materials: Optional[MaterialsArgs] = Field(
        MaterialsArgs(), description="Tasks to be performed related to materials"
    )
    export: Optional[AutbakeExportArgs] = Field(
        AutbakeExportArgs(), description="Configure export")

class WebReadyArgs(DictModel):
    callback_url: Optional[str] = Field(None)
    preset: Presets = Field(
        Presets.normal.value, description="Name of preconfigured pipeline"
    )
    extras: Optional[ExtraOptions] = Field(None)
    commands: List[str] = Field(
        [],
        description="Settings/Command additionally used to the ones selected by preset",
    )
    imports: Optional[ImportArgs] = Field(None, description="Configure import")
    geometry: Optional[GeometryArgs] = Field(
        GeometryArgs(), description="Tasks to be performed related to meshes"
    )
    materials: Optional[MaterialsArgs] = Field(
        MaterialsArgs(), description="Tasks to be performed related to materials"
    )
    export: Optional[AutbakeExportArgs] = Field(
        AutbakeExportArgs(), description="Defines what to export and how"
    )


class PolycountArgs(DictModel):
    callback_url: Optional[str] = Field(None)
    extras: Optional[ExtraOptions] = Field(None)


class PreflightArgs(DictModel):
    callback_url: Optional[str] = Field(None)
    extras: Optional[ExtraOptions] = Field(None)

