from app.api_services.srv_types import ObjectStates
from app.api_services.abstract import AbstractTask


class FutureTask(AbstractTask):
    def __init__(self, id, fn, **kwargs):
        super(FutureTask, self).__init__()
        self.id = id
        self.status = ObjectStates.RUNNING
        self.fn = fn
        self.values = kwargs

    def execute(self):
        return self.fn(**self.values)
