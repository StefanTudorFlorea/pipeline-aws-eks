from glob import glob
from os import path
from app.api_services import dispatcher
from app.api_services.abstract import AbstractTask
from app.api_services.srv_types import ObjectStates

import requests
import json
import zipfile

from app.api_services.utilities import split_id
from app.core.log import Log


class SendTask(AbstractTask):
    def __init__(self, id, from_path, to_url, zipped):
        super(SendTask, self).__init__()
        self.id = id
        self.status = ObjectStates.SENDING
        self.url = to_url
        self.export_folder = from_path
        self.zipped = zipped
        self.logger = Log()

    def execute(self):
        rows = dispatcher.Dispatcher.get_instance().storage.get(self.id)
        self.duration = rows.iloc[0].CalcTime
        server_data = rows.iloc[0].Ext
        backend_id, mode = split_id(self.id)
        data = {
            "objectId": backend_id,
            "duration": self.duration,
            "server_data": server_data,
        }
        if self.zipped:
            zipped_files = set()
            with (zipfile.ZipFile(f"{self.export_folder}/{self.id}.zip", "w")) as zip:
                for filter in [
                    "*.babylon",
                    "*.glb",
                    "Model*.json",
                    "*.jpg",
                    "info.json",
                    "*.png",
                    "*.jpeg",
                    "preview.*",
                ]:
                    for fn in glob(str(self.export_folder / filter)):
                        name, ext = path.splitext(fn)
                        if filter == "*.jpeg":
                            ext = ".jpg"
                        if "_shadow_pass" in name:
                            continue
                        arcname = path.basename(name) + ext
                        if arcname in zipped_files:
                            continue
                        self.logger.debug(
                            f"Adding {fn} to archive as {arcname}",
                            loggernames=["main", "backend"],
                        )
                        zip.write(fn, arcname=arcname)
                        zipped_files.add(arcname)
            files = {"file": open(f"{self.export_folder}/{self.id}.zip", "rb")}
        else:
            data["polycount"] = json.load(
                open(self.export_folder / f"polycount.json", "r")
            )["polycount"]
            files = None

        self.logger.info(f"sending to {self.url}", loggernames=["main", "backend"])
        for retries in range(1, 4):
            try:
                response = requests.post(self.url, data=data, files=files)
                self.logger.debug(response, loggernames=["main", "backend"])
                break
            except Exception as send_err:
                self.logger.error(
                    f"{retries}/3: Error sending results: {send_err}",
                    loggernames=["main", "backend"],
                )
                if retries == 3:
                    raise send_err
        self.logger.info(
            f"Result sent - Calculation time was {self.duration}s",
            loggernames=["main", "backend"],
        )
        return ObjectStates.SENT
