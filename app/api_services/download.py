from app.core.utilities.keywords import supported_geometries
from app.api_services.abstract import AbstractTask
from app.api_services.srv_types import ObjectStates
from app.api_services.utilities import download_file
import os
import shutil
from app.core.log import Log


class DownloadTask(AbstractTask):
    def __init__(self, id, from_url, temp_path, extract_path, check_mode=None):
        super(DownloadTask, self).__init__()
        self.id = id
        self.status = ObjectStates.DOWNLOADING
        self.url = from_url
        self.import_archive = temp_path
        self.import_folder = extract_path
        self.checks = check_mode
        self.logger = Log()

    def poll(self):
        return not os.path.exists(self.import_archive)

    def execute(self):
        self.logger.info(
            f"Downloading from {self.url} to {self.import_archive}",
            loggernames=["main", "backend"],
        )
        if isinstance(self.url, str):
            download_file(self.url, self.import_archive)
        else:
            with open(self.import_archive, "wb") as f:
                shutil.copyfileobj(self.url, f)
        self.logger.info(
            f"Extracting to {self.import_folder}", loggernames=["main", "backend"]
        )
        shutil.unpack_archive(self.import_archive, self.import_folder)
        self.check_result()
        self.logger.info(
            f"Successfully retrieved files", loggernames=["main", "backend"]
        )
        return ObjectStates.DOWNLOADED

    def check_result(self):
        if self.checks is None:
            return
        found_types = dict()
        for entry in os.scandir(self.import_folder):
            if not entry.is_file():
                continue
            ext = os.path.splitext(entry.name)[1].lower()
            if ext not in found_types:
                found_types[ext] = 1
            else:
                found_types[ext] += 1

        if self.checks == "single_geometry":
            total = sum(found_types.get(ext, 0) for ext in supported_geometries)
            if total == 1:
                return
            raise RuntimeError("zip file must contain exactly one geometry file")

        if self.checks == "images_jpg":
            total = sum(found_types.get(ext, 0) for ext in [".jpg", ".jpeg"])
            if total > 0:
                return
            raise RuntimeError("zip does not contain jpg files")

        raise Exception(f"Invalid download constraint: {self.checks}")
