import argparse
import requests
import uvicorn
from fastapi import FastAPI
from fastapi.templating import Jinja2Templates
import socket
import time
import threading

app = FastAPI()
# app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

TMS = "http://94.130.51.105:10085"


@app.on_event("startup")
def startup():
    threading.Thread(target=register_and_heatbeat).start()


def register_and_heatbeat():
    time.sleep(2)
    hostname = socket.gethostname()
    IPAddr = socket.gethostbyname(hostname)
    ip = str(IPAddr)
    while True:
        ms_data = {"name": "webready", "ip": ip, "port": "10090"}
        r = requests.post(TMS + "/register", data=ms_data)
        print(r)
        time.sleep(30)


@app.get("/ms_status")
def ms_status():
    print("status requested...")
    status = {"health": "ready"}
    return status


@app.get("/status")
def status():
    status = {"health": "ready"}
    return status


@app.get("/queues")
def queues():
    return {"active": "0"}


def main():
    parser = argparse.ArgumentParser(description="Run server.")
    parser.add_argument(
        "port", type=str, help="define the port on which the server runs", default=None
    )
    arguments, unknown = parser.parse_known_args()
    uvicorn.run("tms_test:app", host="0.0.0.0", port=int(arguments.port))


if __name__ == "__main__":
    main()
