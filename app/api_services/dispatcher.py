import json
import threading
import socket
import time

import requests

from app.api_services.download import DownloadTask
from app.services import WebreadySrv, PolycountSrv, PreviewSrv, RooomReadySrv
from app.api_services.utilities import module
from app.api_services.srv_types import WebReadyArgs, RooomReadyArgs, ObjectStates, presets
from app.api_services.backends import PDStorage
from concurrent.futures import ThreadPoolExecutor

from app.core.log import Log

logger = Log()


class Dispatcher:
    instance = None
    debug = False
    root = module
    initialized = False

    def __init__(self):
        self.ms_data = None
        self.ms_server = None
        self.ms_stop_event = threading.Event()

    @classmethod
    def get_instance(cls, debug=None, root=None):
        if Dispatcher.instance is None:
            Dispatcher.instance = cls()
            if debug:
                Dispatcher.instance.debug = debug
            if root:
                Dispatcher.instance.root = root
            Dispatcher.instance.__initialize()
        return Dispatcher.instance

    def __initialize(self):
        if Dispatcher.initialized:
            return
        self.storage = PDStorage()
        self.stop_requested = False
        self.executor = ThreadPoolExecutor()
        self.active = dict()
        self.start_time = None
        Dispatcher.initialized = True

    def setup_TMS(self, TMS_server, TMS_client_port, docker_proxy_port):
        if self.debug:
            logger.warning("TMS cannot be used if server in debug mode")
            return
        if docker_proxy_port is None:
            ip = str(socket.gethostbyname(socket.gethostname()))
        else:
            # linux 172.17.0.1
            # ip = str(socket.gethostbyname("gateway.docker.internal"))
            for i in range(1, 6):
                try:
                    r = requests.get("https://api.ipify.org/?format=json")
                    ip = r.json()["ip"]
                    break
                except Exception as tms_con_err:
                    logger.warning(f"({i}/5) Cannot reach TMS")
                    time.sleep(3)
                    if i == 5:
                        logger.warning(f"Disabling TMS")
                        ip = str(socket.gethostbyname(socket.gethostname()))
                        return

        self.ms_data = {
            "name": "webready",
            "ip": ip,
            "port": TMS_client_port if docker_proxy_port is None else docker_proxy_port,
        }
        print(
            f"Registered 'webready' {ip}:{TMS_client_port if docker_proxy_port is None else docker_proxy_port} on TMS {TMS_server}"
        )
        self.ms_server = TMS_server

    def run_TMS(self):
        if self.ms_data is None:
            logger.info("TMS disabled")
            return
        self.ms_stop_event.clear()
        threading.Thread(target=self.ping_TMS).start()

    def ping_TMS(self):
        headers = {"content-type": "application/json"}
        while not self.ms_stop_event.wait(30):
            if self.ms_data is None:
                logger.error("TMS not enabled")
                break
            try:
                js = json.dumps(self.ms_data)
                r = requests.post(self.ms_server + '/register', data=js, headers=headers, timeout=3.0)
                if r.status_code != 200:
                    print(r)
            except Exception as e:
                logger.warning(f"TMS error {e}")

    def get_ms_status(self):
        if self.ms_data is None:
            logger.info("TMS not enabled")
            return None
        if hasattr(self, "ms_wait_for_tms"):
            self.ms_wait_for_tms.set()
            return {"health": "bad"}
        return {"health": "ready"}

    def clear_db(self):
        self.storage.clear()

    def enqueue(
        self, mode, id, domain, token, callback_url, extras, cmds=None, zip=None
    ):
        id = f"{id}_{mode}"
        rows = self.storage.get(id)
        if len(rows) == 0:
            logger.info(f"Enqueued {mode} for {id}")
            self.storage.new(
                object_id=id, token=token, domain=domain, mode=mode, extra=extras
            )
            rows = self.storage.get(id)
        else:
            assert len(rows) == 1
            if (
                rows.iloc[0].Status.value >= ObjectStates.WAITING.value
                and rows.iloc[0].Status.value < ObjectStates.SENT.value
            ):
                logger.info(f"Resuming {mode} for {id} at {rows.iloc[0].Status}")
            else:
                self.storage.update(id, ObjectStates.WAITING, 0)

        args = {
            "debug": self.debug,
            "id": id,
            "token": token,
            "domain": domain,
            "callback_url": callback_url,
            "status": rows.iloc[0].Status,
            "upload": zip,
            "root_dir": self.root,
            "cb_task_start": self.task_start,
            "cb_task_err": self.task_err,
            "cb_task_done": self.task_done,
        }
        if mode == "autobake":
            args["cmds"] = cmds
            srv = WebreadySrv(**args)
        elif mode == "polycount":
            srv = PolycountSrv(**args)
        elif mode == "preview":
            if (args["callback_url"] is None) or (args["callback_url"] == ""):
                args[
                    "callback_url"
                ] = f"https://{domain}/resourceBackend/receiveWebreadyPreview"
            srv = PreviewSrv(**args)
        elif mode == "rooomready":
            args["cmds"] = cmds
            srv = RooomReadySrv(**args)
        else:
            raise f"Unknown service: {mode}"
        if self.start_time is None:
            self.start_time = time.localtime()
        self.executor.submit(srv.run)

    def waiting_items(self, all_states=False):
        result = {"autobake_queue": [], "polycount_queue": [], "preview_queue": [], "rooomready_queue": []}
        r = self.storage.get_unprocessed()
        for row in r.to_dict(orient="records"):
            if (
                all_states
                or ObjectStates(row["Status"]).value == ObjectStates.WAITING.value
            ):
                result[f"{row['Mode']}_queue"].append(
                    f"{row['ObjectId']}: {ObjectStates(row['Status']).name}"
                )
        return result

    def task_start(self, task, status):
        self.storage.update(task.id, status, all_modes=isinstance(task, DownloadTask))
        self.active[task.id] = ObjectStates(status).name

    def task_err(self, task, err):
        if err is RuntimeError:
            self.storage.update(task.id, ObjectStates.SRV_ERROR)
        else:
            self.storage.update(task.id, ObjectStates.ERROR)
        if self.debug:
            raise err

    def task_done(self, task, result):
        new_status, time = result
        self.storage.update(task.id, new_status, time)
        self.active.pop(task.id, None)

    def shutdown(self):
        self.ms_stop_event.set()
        if self.ms_data is not None:
            self.ms_wait_for_tms = threading.Event()
            self.ms_wait_for_tms.wait(1)
        self.executor.shutdown(wait=True, cancel_futures=True)
