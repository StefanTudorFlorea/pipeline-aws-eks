from app.api_services.srv_types import ObjectStates
from app.api_services.abstract import AbstractTask
from app.api_services.utilities import download_file, module, blender_path
from app.api_services.errors import ProcessError
from app.api_services import dispatcher

from app.core.log import Log
import subprocess
import time

class BlenderTask(AbstractTask):
    def __init__(
        self,
        srv_name,
        id,
        input_folder,
        export_folder,
        blender_executable=blender_path,
        blendfile=module.parent / "blender_scenes/webready_bake_2.93.1_2.2.blend",
        pyfile=module.parent / "core/blender.py",
        pipeline_args=[],
        log_args={},
    ):
        super(BlenderTask, self).__init__()
        self.id = id
        self.status = ObjectStates.RUNNING
        self.logger = Log()
        self.input_folder = input_folder
        self.output_folder = export_folder
        self.blender_executable = blender_executable
        self.blendfile = blendfile
        self.pyfile = pyfile
        self.pipeline_args = pipeline_args
        self.logger.info("Blender task created", loggernames="backend")
        self.errors = []
        self.task_count = 1
        self.task_current = 0

        """
        for handler in self.logger['backend'].handlers:
            if isinstance(handler,logging.handlers.HTTPHandler) and 'url' in log_args:
                handler.host = ''
                handler.url = log_args['url']
            if isinstance(handler, logging.FileHandler) and 'file' in log_args:
                #handler.baseFilename = str(logs / f'{id}_log.txt')
                handler.baseFilename = log_args['file']

        self.logger.info("Configuring service", loggernames="backend")
        self.logger.warning("Test error", loggernames="backend")
        """

    def update_progress(self):
        # TODO: direct database access critical; should be avoided
        data = dispatcher.Dispatcher.get_instance().storage.data
        idx = data["ObjectId"] == self.id
        data.loc[idx, "Progress"] = float(self.task_current) / self.task_count

    def con_read(self, result, timeout=0):
        start = time.time()
        log_count = 0
        while True:
            time.sleep(0.01)
            log_count += 1
            line = result.stdout.readline()
            if isinstance(line, bytes):
                line = line.decode("utf8")
            if line == "":
                break
            else:
                if line.startswith("$"):
                    self.errors.append(line)
                elif line.startswith("@--max_task@"):
                    self.task_count = int(line.split("@")[-1])
                    self.update_progress()
                elif line.startswith("@--current_task@"):
                    self.task_current = int(line.split("@")[-1])
                    self.update_progress()

                self.logger.blender(line.rstrip(), loggernames=["main", "backend"])

            if timeout > 0 and timeout < (time.time() - start):
                result.terminate()
                raise Exception(
                    f"Blender process exceeded timeout of {timeout}. Terminating process."
                )
        self.logger.info("console read stopped", loggernames=["main", "backend"])

    def execute(self):
        self.logger.info("Running Blender", loggernames=["main", "backend"])
        json_path = self.output_folder / f"Model_info.json"
        call = [
            str(self.blender_executable),
            "-b",
            str(self.blendfile),
            "--python",
            str(self.pyfile),
            "--",
            "--cli",
            "--import",
            str(self.input_folder),
            # "--log",
            # self.logfile,
            "--export",
            str(self.output_folder),
            "--write_info",
            str(json_path),
        ]
        # if self.debug:
        #    call += ["--debug"]
        call += self.pipeline_args

        self.logger.info("-- Blender start --", loggernames=["main", "backend"])
        self.logger.info(f"call: {call}")
        result = subprocess.Popen(
            call, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False
        )
        self.con_read(result)
        self.logger.info("-- Blender end --", loggernames=["main", "backend"])
        """
        for handler in self.logger['backend'].handlers:
            if isinstance(handler, logging.FileHandler):
                handler.close()
        """
        if len(self.errors) > 0:
            self.logger.error("-- Errors during execution --", loggernames="backend")
            raise ProcessError("".join(self.errors))
        return ObjectStates.RAN
