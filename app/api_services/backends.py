from app.api_services.srv_types import ObjectStates
import time
import pandas as pd

from app.api_services.utilities import split_id


class PDStorage:
    def __init__(self, path=None):
        self.data = pd.DataFrame(
            columns=[
                "ObjectId",
                "Status",
                "Token",
                "Domain",
                "Time",
                "CalcTime",
                "Progress",
                "Mode",
                "Ext",
            ]
        )
        self.data.set_index(["ObjectId"])

    def get(self, object_id, mode=None):
        q = "ObjectId == @object_id"
        if mode:
            q += " and Mode == @mode"
        return self.data.query(q)

    def new(self, object_id, token, domain, mode="autobake", extra=None):
        self.data = self.data.append(
            {
                "ObjectId": object_id,
                "Status": ObjectStates.WAITING,
                "Token": token,
                "Domain": domain,
                "Time": time.localtime(),
                "CalcTime": 0,
                "Progress": 0,
                "Mode": mode,
                "Ext": "{}" if extra is None else extra,
            },
            ignore_index=True,
        )

    def clear(self):
        self.data = self.data[0:0]

    def update(self, object_id, new_status, time=0, all_modes=False):
        if all_modes:
            base_id, mode = split_id(object_id)
            idx = self.data["ObjectId"].str.startswith(base_id)
        else:
            idx = self.data["ObjectId"] == object_id
        self.data.loc[idx, "Status"] = new_status
        if time == -1:
            self.data.loc[idx, "Time"] = time.localtime()
        else:
            self.data.loc[idx, "CalcTime"] += time

    def _find_by_status(self, status, **kwargs):
        if isinstance(status, str):
            q = "Status == @status"
        else:
            q = "Status in @status"
        for k, v in kwargs.items():
            q += f" and {k} == {v}"
        rows = self.data.query(q)
        return None if len(rows) == 0 else rows.iloc[0]

    def get_unprocessed(self):
        processed = [
            ObjectStates.ERROR.name,
            ObjectStates.SRV_ERROR.name,
            ObjectStates.SENT.name,
        ]
        return self.data.query("Status not in @processed")

    def next_for_download(self):
        return self._find_by_status(ObjectStates.WAITING.name)

    def next_done(self):
        return self._find_by_status(ObjectStates.SENT.name)
