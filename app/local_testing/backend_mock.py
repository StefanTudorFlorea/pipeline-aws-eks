from fastapi import (
    FastAPI,
    Request,
    Response,
    File,
    Form,
    Body,
    UploadFile,
    HTTPException,
)
from fastapi.responses import HTMLResponse, FileResponse
import socket
import uvicorn
import argparse
import logging
from typing import Optional, List, Any
from pydantic import BaseModel, Field, conint
from pydantic.types import Tuple
import json

logger = logging.getLogger("Mock Backend")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler("errors.log", mode="w")
logger.addHandler(handler)
# handler = logging.StreamHandler()
# logger.addHandler(handler)
count = 0
app = FastAPI()

# get files: resourceBackend/autobakeFile/{id}?token={token}

# send files: /resourceBackend/receiveAutobake


class ResultArgs(BaseModel):
    objectId: str
    duration: float
    error: Optional[str]
    message: Optional[str]
    polycount: Optional[int]
    # file: UploadFile()


class ErrorArgs(BaseModel):
    objectId: str
    error: str = None
    message: str = None
    duration: float = 0.0


@app.get("/resourceBackend/autobakeFile/{id}", status_code=200)
def get_upload(id: str, token: str, response: Response):
    zip_upload = "app/unit_testing/test_data/backend_errors/61a1c28a6e4929169002db6c_bowman_model.zip"
    return FileResponse(zip_upload)


@app.post("/receiveAutobake")
def get_error(
    objectId: str = Form(...),
    error: Optional[str] = Form(...),
    message: Optional[str] = Form(...),
    duration: Optional[float] = Form(...),
    server_data: Optional[str] = Form(...),
):
    print(f"Received autobake error for {objectId}: {error} server_data: {server_data}")
    global count
    logger.info(f"{count}. Received autobake error for {objectId}")
    logger.info(error.replace("\n", ""))
    count += 1


def main():
    parser = argparse.ArgumentParser(description="Run backend mock server.")
    parser.add_argument(
        "--port",
        type=int,
        required=False,
        help="define the port on which the server runs",
        default=11199,
    )
    arguments, unknown = parser.parse_known_args()

    ip = str(socket.gethostbyname(socket.gethostname()))

    logger.warning(f"Starting {ip}:{arguments.port}...")
    uvicorn.run(
        "backend_mock:app", host="0.0.0.0", port=arguments.port, access_log=False
    )
    logger.info("Bye")


if __name__ == "__main__":
    main()
