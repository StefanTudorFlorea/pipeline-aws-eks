from time import sleep
from functools import partial
from pathlib import Path

import argparse
import functools
import requests
import glob
import os
import json

DEFAULT_JSON = json.dumps(
    {
        "callback_url": "http://127.0.0.1:11199/receiveAutobake",
        "preset": "normal",
        "extras": {
            "objectId": "61cae8469b8b0a10cf124e52",
            "domain": "http://127.0.0.1:11199/resourceBackend/receiveAutobake",
            "authtoken": "OtyGl0qbGJoSCGkXfBuZ1oJmhQ0nJCdRux_O5kIUvQ2PoKk6iaDnFpVJgvnfpnInkB",
            "server_data": '{"-silent": "true"}',
        },
    }
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run server and test all zips from a given folder"
    )
    parser.add_argument(
        "path",
        type=str,
        help="Directory containing zip files",
        default=Path("../unit_testing/test_data/backend_errors").resolve(),
    )
    parser.add_argument(
        "--host",
        type=str,
        required=False,
        help="Server to use for testing",
        default="http://localhost:10100",
    )
    parser.add_argument(
        "--debug", action="store_true", help="Run server in debug mode", default=False
    )
    arguments, unknown = parser.parse_known_args()

    files = glob.glob(arguments.path + "/*.zip")
    for i, fn in enumerate(files):

        r = requests.put(
            url=arguments.host + "/polycount",
            data={"json_args": DEFAULT_JSON},
            files={"zipped": open(fn, "rb")},
        )
        r = requests.put(
            url=arguments.host + "/preview",
            data={"json_args": DEFAULT_JSON},
            files={"zipped": open(fn, "rb")},
        )
        r = requests.put(
            url=arguments.host + "/webready",
            data={"json_args": DEFAULT_JSON},
            files={"zipped": open(fn, "rb")},
        )
        print(f"Processing {fn} ({i+1}/{len(files)}): {r}")
        sleep(1)
