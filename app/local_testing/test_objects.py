import sys
import os
import pathlib
from main import start_blender
from concurrent.futures import ThreadPoolExecutor

module = pathlib.Path(__file__).parent.resolve()


def autobake_dir(dir):
    print(f"Running {dir}")
    settings_file = None
    for f in os.listdir(dir):
        if os.path.isfile(f):
            name, ext = os.path.splitext(f)
            if name.lower() in ["setting", "settings"]:
                settings_file = f
                break
    if settings_file:
        args = ["--settings", os.path.join(dir, settings_file)]
    else:
        args = []
    args += [
        "-cli",
        "--import",
        str(dir),
        "-export_directory",
        os.path.join(dir, "autobake"),
    ]
    start_blender(args)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        root_dir = sys.argv[1]
    else:
        root_dir = str(module)
    dirs = [os.path.join(module, d) for d in os.listdir(root_dir) if os.path.isdir(d)]
    print(f"{len(dirs)} : {dirs}")

    executor = ThreadPoolExecutor()
    for d in dirs:
        executor.submit(autobake_dir(d))
