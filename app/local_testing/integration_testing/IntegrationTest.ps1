#Make sure PowerShell 7 is used!!!
#Make sure installe.ps1 was excecuded and the blender folders are not empty
#Make sure submodule testObjects is included

# blender python requirements
# * numpy
# * scikit-image
# * opencv-python

$settingsList = New-Object Collections.Generic.List[String]
# add all the single setting files here
$settingsList.Add("Settings_01")
$settingsList.Add("Settings_02")
$settingsList.Add("Settings_03")
$settingsList.Add("Settings_04")
$settingsList.Add("Settings_05")
$settingsList.Add("Settings_06")
$settingsList.Add("Settings_07")
$settingsList.Add("Settings_08")
$settingsList.Add("Settings_09")
$settingsList.Add("Settings_10")
$settingsList.Add("Settings_11")
$settingsList.Add("Settings_12")


$testObjectList = New-Object Collections.Generic.List[String]
# add all test object folders here
$testObjectList.Add("Cubes/fbx-Packed")
$testObjectList.Add("Cubes/fbx-Unpacked")
$testObjectList.Add("Cubes/glTF-Binary")
$testObjectList.Add("Cubes/glTF")
$testObjectList.Add("Cubes/glTF-Unpacked")

$testObjectList.Add("Suzanne/fbx-Packed")
$testObjectList.Add("Suzanne/fbx-Unpacked")
$testObjectList.Add("Suzanne/glTF-Binary")
$testObjectList.Add("Suzanne/glTF")
$testObjectList.Add("Suzanne/glTF-Unpacked")




#relative path when pipeline is started with ./main.py
$relativePathToTestObjects = "./lib/TestObjects"
$relativePathToIntegrationTest = "./app/local_Testing/integration_testing"

$relativePathToBlenderFolder = "./blender_dir/Blender/"

#relative path when starting from ./IntegrationTest.ps1
$mainScript = "../../../main.py"

# ./IntegrationTest.ps1 directory
$rootDir = $PWD

# #The reference list should hold the reference objects parallel to the testObjectList
# # testObjectList[0] => Suzanne == referenceObjectList[0] => Reference_Suzanne ==> in core they are the same assets
# $referenceObjectList = New-Object Collections.Generic.List[String]
# $referenceObjectList.Add("Reference_Susanne")
# $referenceObjectList.Add("Reference_Cubes")


foreach ($setting in $settingsList) {

    $settingDir = Join-Path -Path $relativePathToIntegrationTest/settings -ChildPath $setting".txt"
    echo "Settings Dir": $settingDir

    foreach ($object in $testObjectList) {
        $outputFolder = $setting + "_"+ $object

        $referenceObjectDir = Join-Path -Path $relativePathToTestObjects -ChildPath "test_references/"$outputFolder
        #cut the input type from the path to get the reference path

        $importDir = Join-Path -Path $relativePathToTestObjects -ChildPath $object
        $exportDir = Join-Path -Path $relativePathToIntegrationTest -ChildPath "export/"$outputFolder

        # change current dir to git root dir
        # call the pipeline
        if ($IsMacOS -or $isLinux){
            $blenderPyPath = Join-Path -Path $relativePathToBlenderFolder -ChildPath Contents/Resources/2.93/python/bin/python

            #change to main.py path
            $resolvedMain = Resolve-Path -Path $mainScript
            $resolvedMainPath = Split-Path -Path $resolvedMain

            cd $resolvedMainPath

            # start the pipeline
            & $blenderPyPath ./main.py --settings $settingDir -cli --import $importDir --export $exportDir

             # start the testing with the usual python
             & python ./app/local_testing/integration_testing/testing/testing.py --referenceDir $referenceObjectDir y --compareDir $exportDir
        }
        elseif ($IsWindows) {
            $blenderPyPath = Join-Path -Path $relativePathToBlenderFolder -ChildPath 2.93/python/bin/python.exe

            #change to main.py path
            $resolvedMain = Resolve-Path -Path $mainScript
            $resolvedMainPath = Split-Path -Path $resolvedMain

            cd $resolvedMainPath

            # start the rooomReady pipeline
            & $blenderPyPath ./main.py --settings $settingDir -cli --import $importDir --export $exportDir

            # start the testing with the usual python
            & python ./app/local_testing/integration_testing/testing/testing.py --referenceDir $referenceObjectDir y --compareDir $exportDir
        }
        else {
            echo "Could not detect OS"
        }
        cd $rootDir


        #when pipeline finished, call test
    }
}