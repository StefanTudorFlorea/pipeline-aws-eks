# Readme for integration_testing
This testing pipeline takes different provided objects and runs them with different settings.
The output is compared against stored reference data, to check if they match

## Requirements
### General
* Powershell 7.0
* roomReady pipeline is set up
* submodule testObjects is included in this Repo

### Python
Make sure the following python packages are installed:
* numpy
* scikit-image
* opencv-python

## How to
### Running the test
Open a Powershell in this folder and run the IntegrationTest.ps1
The outputs and evaluations are stored inside the export folder
The result of the comparisson is stord in a result.log.
See this [Confluence](https://rooomdesk.atlassian.net/wiki/spaces/WRDY/pages/2149613588/Integration+Testing) for detailed information.

### Editing the test
In case the test needs to be increased with new test cases do the following

#### Adding new settings

* Run the rooomReady pipeline with the new settings and all objects that are stored in the "./lib/testObjects" folder
* Manually, check if the output result is correct, otherwise corret the rooomReady pipeline
* Save the result in the **rooomReady-test-object** repository in a folder build up from this template:
``` text
    ./test_references
        -> Settings_<number>_<objectname>
            -> <filetype>
```
* Push the changes
* Update the rooomReady submodule under "./lib/testObjects"
* Create a new settings file and name it *Settings_\<number>.txt*.
* Save the file in the *settings* folder
* Open the *IntegrationTest.ps1* file and add the new setting name to the list of settings
``` batch
$settingsList.Add("Settings_<number>")
```
* Save the file and push all changes

#### Adding new test objects
* Create a new object fitting the specification for the rooomReady pipeline
* Save the new object in the **rooomReady-test-object** repository
    * as testObject
* Manually, run the rooomReady pipeline with all the settings, check if the results are correct.
* Store the exported files in the **rooomReady-test-object** repository in the *./test_references* folder
``` text
    ./test_references
        -> Settings_<number>_<objectname>
            -> <filetype>
```
* Push all the changes

* In the *rooomReady* repository, sync the submodule lib/testObjects folder.
* In the ./app/local_testing/integration_testing folder add the new test object to the *IntegrationTest.ps1* file
``` bash
$testObjectList.Add("<object>/<file_type>")
```
* Run the test for checking
* Push the changes