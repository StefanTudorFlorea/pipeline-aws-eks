
# from tkinter import Image
from skimage.metrics import structural_similarity as ssim
import numpy

import log

def mean_square_error(image1,image2) -> float:
    """ the 'Mean Squared Error' (mse) between the two images is the
        sum of the squared difference between the two images;
        NOTE: the two images must have the same dimension

    Args:
        image1 (Image): image 1 for comparisson
        image2 (Image): image 2 for comparisson

    Returns:
        numbers: mean squared error of both images.
            0.0 -> 100% similar, bigger 0.0 -> not similar
    """
    mse = None
    try:
        mse = numpy.sum((image1.astype("float") - image2.astype("float")) ** 2)
        mse /= float(image1.shape[0] * image1.shape[1])
    except Exception as e:
        print(e)
        log.write_message(
            "\t\tmse: Images could not be compared!",
            f"\t\t\t{e}\n\n"
            )

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return mse

def structural_similarity_index(image1,image2) -> float:
    """ the Structural Simailarity Index' (ssim) between two images is the
        perceived change in the structural information of the image
        NOTE: the two images must have the same dimension

    Args:
        image1 (Image): image 1 for comparisson
        image2 (Image): image 2 for comparisson

    Returns:
        float: structural simailarity index of both images.
            1.0 -> 100% similar, smaller 1.0 -> not similar
    """
    measure = None
    try:
        measure = ssim(image1, image2)
    except Exception as e:
        print(e)
        log.write_message(
            "\t\tssim: Images could not be compared!",
            f"\t\t\t{e}\n\n"
            )
    return measure