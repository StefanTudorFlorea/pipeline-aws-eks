import filecmp
import difflib

def compare_binary_files(file1, file2):
    filecmp.clear_cache()
    return filecmp.cmp(file1,file2)

def compare_txt_files(file1, file2):
    file1_r = open(file1, "r").readlines()
    file2_r = open(file2, "r").readlines()

    is_equal = True
    for line in difflib.unified_diff(
        file1_r,
        file2_r,
        fromfile='reference file',
        tofile='compare file',
        lineterm=''
    ):
        log.write_message(f"\t\t\t{line}")
        print(line)
        is_equal = False

    return is_equal

