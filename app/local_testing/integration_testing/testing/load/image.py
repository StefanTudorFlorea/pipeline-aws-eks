from pathlib import Path
# from tkinter import Image
import cv2
import glob

def load_image(filepath: Path):
    """ Loads an image from the given path

    Args:
        filepath (Path): The filepath of the image

    Returns:
        Image: A grayscale version of the loaded image
    """
    image = cv2.imread(filepath)
    imageGrayScale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    return imageGrayScale

def get_images_from_directory(directory: Path):
    """ gets the absolute image paths from the input directory and
        returns a list with all found images. (jpg, jpeg, png)

    Args:
        directory (Path): directory to check for images

    Returns:
        List: the list of found images
    """
    file_types = ("/*.jpg", "/*.jpeg", "/*.png")
    file_dictionary = {}
    for type in file_types:
        files = glob.glob(directory + type)

        for file in files:
            print(str(file))
            if "baseColor" in str(file):
                file_dictionary["baseColor"] = file
            elif "emission" in str(file):
                file_dictionary["emission"] = file
            elif "shadow" in str(file):
                if "shadowmap" in str(file):
                    file_dictionary["shadowmap"] = file
                else:
                    file_dictionary["shadow"] = file
            elif "reflectivity" in str(file):
                file_dictionary["reflectivity"] = file
            elif "normal" in str(file):
                file_dictionary["normal"] = file

    return file_dictionary

