def read_line_arguments(arguments):
    arguments_dict = dict ()
    for index, arg in enumerate(arguments):
        if arg.startswith("--"):
            arguments_dict[arg] = arguments[index + 1]

    return arguments_dict