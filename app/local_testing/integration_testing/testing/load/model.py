from pathlib import Path
import glob

def get_models_from_directory(directory:Path):
    file_types = (
        "/*.obj", "/*.mtl",
        "/*.glb",
        "/*.babylon", "/*.babylon.manifest",
        )
    file_dictionary = {}
    for types in file_types:
        files = glob.glob(directory+types)

        for file in files:
            print(str(file))
            if "obj" in str(Path(file).suffix):
                file_dictionary["obj"] = file
            elif "mtl" in str(Path(file).suffix):
                file_dictionary["mtl"] = file
            elif "glb" in str(Path(file).suffix):
                file_dictionary["glb"] = file
            elif "babylon" in str(Path(file).suffix):
                file_dictionary["babylon"] = file
            elif "manifest" in str(Path(file).suffix):
                file_dictionary["manifest"] = file

    return file_dictionary
