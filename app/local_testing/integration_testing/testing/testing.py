# from tkinter import Image
import pathlib
from compare import image, model
from load.image import (
    load_image,
    get_images_from_directory,
    )
from load.lineArguments import (
    read_line_arguments,
)

from load.model import get_models_from_directory


import sys
import os
import logging
import log

def compare_images(original_image_path, compare_image_path):
    original_image = load_image(original_image_path)
    compare_image = load_image(compare_image_path)

    mse = image.mean_square_error(original_image,compare_image)
    ssim = image.structural_similarity_index(original_image,compare_image)

    log.write_result((mse,ssim), mse = mse,ssim = ssim)
    print("This is the mse:", mse)
    print("This is the ssim:", ssim)
    if mse != 0 and ssim != 1:
        print("The two images are not similar!")
        print()
    else:
        print("The two images are similar!")
        print()

def compare_models(original_model_path, compare_model_path, func):
    result = func(original_model_path, compare_model_path)
    log.write_result(result)

    if result:
        print("The two files are similar!")
        print()
    else:
        print("The two files are not similar!")
        print()

if __name__ == "__main__":
    # read given line arguments to get directories to compare
    line_arguments = read_line_arguments(sys.argv)

    reference_dir = line_arguments["--referenceDir"]

    compare_dir = line_arguments["--compareDir"]

    #set up a logging file
    logging.basicConfig(
        filename=os.path.join(compare_dir, "result.log"),
        level=logging.INFO,
        filemode="w",
        format='%(message)s'
        )
    log.logging_start(compare_dir)

    # from each directory, get the stored files
    images_reference_dict = get_images_from_directory(reference_dir)
    images_compare_dict = get_images_from_directory(compare_dir)
    # Do the same for the models
    model_reference_dict = get_models_from_directory(reference_dir)
    model_compare_dict = get_models_from_directory(compare_dir)

    print("refDir", reference_dir)
    print("compareDir", compare_dir)

    if os.path.exists(reference_dir):
        for imageType in images_reference_dict:
            log.write_file_type(imageType)
            print()
            print(imageType)
            if images_compare_dict.get(imageType):
                compare_images(
                    images_reference_dict[imageType],
                    images_compare_dict[imageType]
                    )

        for fileType in model_reference_dict:
            log.write_file_type(fileType)
            print()
            print(fileType)
            if model_compare_dict.get(fileType):
                if fileType == "obj" or fileType == "mtl" or fileType == "manifest":
                    compare_models(
                        model_reference_dict[fileType],
                        model_compare_dict[fileType],
                        model.compare_txt_files)
                else:
                    compare_models(
                        model_reference_dict[fileType],
                        model_compare_dict[fileType],
                        model.compare_binary_files)
    else:
        print("Could not find reference directory", reference_dir)