import logging
from pathlib import PurePath
import os

def logging_start(directory):
    split_directory = directory.split(os.sep)
    logging.info(f"========== Testing results for: {split_directory[-2]}{os.sep}{split_directory[-1]} ==========")


def write_file_type(filetype:str):
    logging.info("")
    logging.info(f"\tChecking {filetype.capitalize()}:")

def write_result(result, **arg):

    logging.info(f"\t\tThe two files are {check_result(result)}similar!")
    if not result:
        logging.info(f"\t\t\tCompare the files to see the difference!")
    for key, value in arg.items():
        logging.info(f"\t\t\t\tThe {key} is: {value} ")

def check_result(result):
    if type(result) == bool:
        if not result:
            return 'not '
        else:
            return ''
    else:
        if result[0] != 0 and result[1] != 1:
            return 'not '
        else:
            return ''
def write_message(*msg):
    for m in msg:
        logging.info(m)


