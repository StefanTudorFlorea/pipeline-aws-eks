Please append to all files
- the blender version the file got last saved by
- an increment to the version number each time the file is saved with:
-- the first digit being a new feature
-- the second digit being smaller fixes

Please make sure that all .blend files are saved with the "compressed" option and
no unnecessiary Objects or images are packed into the .blend files