import os
import time
from pathlib import Path
import platform
import subprocess
import json

platform_os = platform.system().lower()
# change to False if blender and webready are not in AppData
win_user_install = False

module = Path(__file__).parent.parent.parent.resolve()
blend_file = (
    module / "app" / "blender_scenes" / "webready_bake_2.93.1_2.2.blend"
)
python_file = module / "app" / "core" / "blender.py"
base_export_directory = (
    Path(os.path.dirname(os.path.abspath(__file__))) / "pipeline_cases"
)
# our main import data
base_import_directory = Path(os.path.dirname(os.path.abspath(__file__))) / "pipeline_cases" / "rooomready" / "glTF-Binary"
pipeline_test_export_directory = (
    Path(os.path.dirname(os.path.abspath(__file__))) / "pipeline_tasks"
)

# adjust path according to the platform
if platform_os == "linux" or platform_os == "linux2":
    blender_executable = "blender"
    python_path = "/bin/2.93/python/bin/python3.9"
elif platform_os == "darwin":
    # for mac user.
    blender_folder = module / "blender_dir" / "Blender"
    blender_executable = blender_folder / "Contents/MacOS/Blender"
    python_folder = blender_folder / "Contents/Resources/2.93/python/bin"
    python_executable = python_folder.joinpath("python3.9")
else:
    blender_folder = module / "blender_dir" / "Blender"
    blender_executable = blender_folder / "blender.exe"
    python_folder = blender_folder.joinpath("2.93/python")
    python_executable = python_folder.joinpath("bin/python.exe")


def load_babylon(path):
    """load babylon file for checking the output

    Args:
        path (str): file path to the babylon folder

    Returns:
        json (json): json object
    """
    file = open(
        path,
    )
    return json.load(file)


def con_read(result, timeout=0):
    """check the communication output

    Args:
        result (_type_): _description_
        timeout (int, optional): _description_. Defaults to 0.
    """
    start = time.time()
    log_count = 0
    while True:
        time.sleep(0.01)
        log_count += 1
        line = result.stdout.readline()
        if isinstance(line, bytes):
            line = line.decode("utf8")
        if line == "":
            break
        else:
            print(line.rstrip())


def start_blender(arguments, setting_file):
    """start blender and testing the object

    Args:
        arguments (str): arguments to pass to the pipeline
        setting_file (str): filepath to the setting file
    """
    call = f"{blender_executable} {blend_file} --background --python {python_file} -- -setting {setting_file} -cli {arguments} "
    process = subprocess.run(
        args=call, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    # con_read(process)
    # print(f'Blender finished: {process.returncode}')


def remove_files_in_folder(path):
    """remove files to save repository space

    Args:
        path (str): the dir to be removed.
    """
    for root, dirs, files in os.walk(path):
        for file in files:
            os.remove(os.path.join(root, file))


def unit_test_statistics(dict):
    """report statistics

    Args:
        dict (dict): python dictionary

    Returns:
        all_pass: boolean
    """
    all_pass = False
    total_checks = len(dict.keys())
    success = 0
    for key, val in dict.items():
        if not val:
            success += 1
    success_percent = round((success / total_checks) * 100, 2)
    print(f"{success_percent}% pass the check")
    if success != total_checks:
        for key, val in dict.items():
            if val:
                print(f"**{key} Failed**: {val}")
    else:
        all_pass = True
    return all_pass
