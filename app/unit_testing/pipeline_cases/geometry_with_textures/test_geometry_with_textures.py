# -*- coding: utf-8 -*-

import unittest
import os, re
from sys import path, argv
from subprocess import Popen
import subprocess

# from testing.testing_utilities import remove_files_in_folder
# import testing.testing_utilities
import os, re
from pathlib import Path
from os import getenv
from app.unit_testing import testing_utilities


class TestGeometryTextures(unittest.TestCase):
    """This class tests only the export of a geometry and not the generation of a valid geometry"""

    def print_results(self, test_name, value, check):
        if value == check:
            print(f"SUCCESS: \t {test_name}")
        else:
            print(f"FAILED: \t {test_name}")

    def unit_test(self):
        export_directory = (
            Path(testing_utilities.base_export_directory)
            / "geometry_with_textures/output/"
        )
        import_directory = (
            Path(testing_utilities.base_import_directory) / "textured_material/"
        )
        settings_path = (
            Path(testing_utilities.base_export_directory) / "geometry_with_textures/"
        )

        arguments_dict = {
            "--import": import_directory,
            "--export_directory": export_directory,
        }

        arguments = ""
        for key, value in arguments_dict.items():
            arguments += str(key) + " " + str(value) + " "
        testing_utilities.start_blender(arguments, settings_path)

        testValue = True
        checkValue = False

        # Check for Diffuse
        for file in os.listdir(export_directory):
            if "obj" in file:
                checkValue = True

        self.print_results("Export OBJ", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        checkValue = False

        # Check for Diffuse
        babylon_path = None
        for file in os.listdir(export_directory):
            if file.endswith(".babylon"):
                checkValue = True
                babylon_path = Path(export_directory) / file

        self.print_results("Export Babylon", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        # Check Babylon File Content
        if babylon_path:
            data = testing_utilities.load_babylon(babylon_path)
            materials = data["materials"]
            meshes = data["meshes"]

            check_material = False
            check_material_ms = False
            check_model = False
            check_model_ms = False
            for material in materials:
                if material["name"] == "Model.0":
                    check_material = True
                if material["name"] == "Model.MS":
                    check_material_ms = True
            for mesh in meshes:
                if mesh["name"] == "Model":
                    check_model = True
                if mesh["name"] == "Model_shadow":
                    check_model_ms = True

        self.print_results("Check Babylon Material 0", check_material, True)
        self.assertEqual(check_material, True)

        self.print_results("Check Babylon Material MS", check_material_ms, True)
        self.assertEqual(check_material_ms, True)

        self.print_results("Check Babylon Model", check_model, True)
        self.assertEqual(check_model, True)

        self.print_results("Check Babylon Model MS", check_model_ms, True)
        self.assertEqual(check_model_ms, True)

        checkValue = False

        # Check for Diffuse
        for file in os.listdir(export_directory):
            if "obj" in file:
                checkValue = True

        self.print_results("Export GLB", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        checkValue = False

        for file in os.listdir(export_directory):
            if "shadow" in file:
                checkValue = True

        self.print_results("Export Shadowmap", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        checkValue = False

        # Check for Diffuse
        for file in os.listdir(export_directory):
            if "diffuse" in file:
                checkValue = True

        self.print_results("Export diffuse texture", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        checkValue = False

        # Check for Metallic
        for file in os.listdir(export_directory):
            if "metallic" in file:
                checkValue = True

        self.print_results("Export metallic texture", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        # Check for Reflectivity
        for file in os.listdir(export_directory):
            if "reflectivity" in file:
                checkValue = True

        self.print_results("Export reflectivity texture", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        # Check for Roughness
        for file in os.listdir(export_directory):
            if "roughness" in file:
                checkValue = True

        self.print_results("Export roughness texture", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        testing_utilities.remove_files_in_folder(export_directory)
