# -*- coding: utf-8 -*-

import unittest
import os, re
from sys import path, argv
from subprocess import Popen
import subprocess

# from testing.testing_utilities import remove_files_in_folder
# import testing.testing_utilities
import os, re
from pathlib import Path
from os import getenv

from app.unit_testing import testing_utilities

module = Path(os.path.dirname(os.path.abspath(__file__))).parent.parent.parent
blendfile = module / "blendfiles" / "webready_bake_2.93.1_2.1.blend"
pythonfile = module / "blender.py"


def remove_files_in_folder(path):
    for root, dirs, files in os.walk(path):
        for file in files:
            os.remove(os.path.join(root, file))


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


def start_blender(arguments):
    process = subprocess.run(
        args=f"{testing_utilities.blender_executable} {blendfile} --background --python {pythonfile} -- -cli {arguments}",
        stdout=subprocess.DEVNULL,
    )


class TestGeometryTripleMaskTextures(unittest.TestCase):
    """This class tests only the export of a geometry and not the generation of a valid geometry"""

    def print_results(self, test_name, value, check):
        if value == check:
            print(f"SUCCESS: \t {test_name}")
        else:
            print(f"FAILED: \t {test_name}")

    def test_tmt_export(self):
        export_directory = os.path.dirname(os.path.abspath(__file__)) + "\\output\\"
        import_directory = (
            module / "test\\objects\\textured_material_and_triple_mask_texture"
        )
        settings_path = (
            Path(testing_utilities.base_export_directory)
            / "geometry_with_triple_material_set/"
        )

        arguments_dict = {
            "--import": import_directory,
            "--export_directory": export_directory,
        }

        arguments = ""
        for key, value in arguments_dict.items():
            arguments += str(key) + " " + str(value) + " "
        start_blender(arguments, settings_path)

        testValue = True
        checkValue = False

        for file in os.listdir(export_directory):
            if file.endswith(".glb"):
                checkValue = True

        self.assertEqual(testValue, checkValue)

        self.print_results("Export TMT GLB", testValue, checkValue)

        for file in os.listdir(export_directory):
            if file.endswith(".obj"):
                checkValue = True

        self.assertEqual(testValue, checkValue)

        self.print_results("Export TMT OBJ", testValue, checkValue)

        for file in os.listdir(export_directory):
            if file.endswith(".babylon"):
                checkValue = True

        self.assertEqual(testValue, checkValue)

        self.print_results("Export TMT BABYLON", testValue, checkValue)

        # Check for Diffuse
        for file in os.listdir(export_directory):
            if "diffuse" in file:
                checkValue = True

        self.assertEqual(testValue, checkValue)

        self.print_results("Export TMT diffuse texture", testValue, checkValue)

        checkValue = False

        # # Check for Metallic
        # for file in os.listdir(export_directory):
        #     if "metallic" in file:
        #         checkValue = True

        # self.assertEqual(testValue, checkValue)

        # self.print_results("Export metallic texture", testValue, checkValue)

        # # Check for Reflectivity
        # for file in os.listdir(export_directory):
        #     if "reflectivity" in file:
        #         checkValue = True

        # self.assertEqual(testValue, checkValue)

        # self.print_results("Export TMT reflectivity texture", testValue, checkValue)

        # # Check for Roughness
        # for file in os.listdir(export_directory):
        #     if "roughness" in file:
        #         checkValue = True

        # self.assertEqual(testValue, checkValue)

        # self.print_results("Export TMT roughness texture", testValue, checkValue)

        # remove_files_in_folder(export_directory)
