# -*- coding: utf-8 -*-

from cmath import exp
import unittest
import os, re
import json
from sys import path, argv
from subprocess import Popen
import subprocess

# from testing.testing_utilities import remove_files_in_folder
# import testing.testing_utilities
import os, re
from pathlib import Path
from os import getenv

from app.unit_testing import testing_utilities

module = Path(os.path.dirname(os.path.abspath(__file__))).parent.parent.parent


def remove_files_in_folder(path):
    for root, dirs, files in os.walk(path):
        for file in files:
            os.remove(os.path.join(root, file))


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


class TestPreflightNoMaterials(unittest.TestCase):
    """This class tests only the export of a geometry and not the generation of a valid geometry"""

    def print_results(self, test_name, value, check):
        if value == check:
            print(f"SUCCESS: \t {test_name}")
        else:
            print(f"FAILED: \t {test_name}")

    def unit_test(self):
        export_directory = (
            Path(testing_utilities.base_export_directory)
            / "preflight_no_materials/output/"
        )
        import_directory = Path(testing_utilities.base_import_directory) / "template/"
        settings_path = (
            Path(testing_utilities.base_export_directory) / "preflight_no_materials/"
        )

        arguments_dict = {
            "--import": import_directory,
            "--export_directory": export_directory,
            "--preview": True,
        }

        if not os.path.exists(export_directory):
            os.makedirs(export_directory)

        arguments = ""
        for key, value in arguments_dict.items():
            arguments += str(key) + " " + str(value) + " "
        testing_utilities.start_blender(arguments, settings_path)

        testValue = True
        checkValue = False

        # Check for PNG File
        for file in os.listdir(export_directory):
            if file.endswith(".png"):
                checkValue = True

        self.print_results("Export Preview PNG", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        checkValue = False

        # Check for GIF File
        for file in os.listdir(export_directory):
            if file.endswith(".gif"):
                checkValue = True

        self.print_results("Export Preview GIF", testValue, checkValue)
        self.assertEqual(testValue, checkValue)

        testing_utilities.remove_files_in_folder(export_directory)
