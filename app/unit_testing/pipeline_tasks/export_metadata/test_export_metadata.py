import unittest
import json
import os
from pathlib import Path

from app.unit_testing import testing_utilities
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
module = Path(os.path.dirname(ROOT_DIR)).parent.parent.parent

class TestExportMetaData(unittest.TestCase):
    """This class tests creation and export of metadata created by misc.mesh_to_dict(model)

    However, it does not check the values created by this function, it is just a check if export and reload
    are the same. It simply compares two strings and will likely fail for different encoding or pretty-print.
    """
    def print_results(self, test_name, value, check):
        if value == check:
            print(f"SUCCESS: \t {test_name}")
        else:
            print(f"FAILED: \t {test_name}")

    def unit_test(self):
        export_directory = Path(testing_utilities.pipeline_test_export_directory) / "export_metadata/output/"
        export_directory = Path(testing_utilities.pipeline_test_export_directory) / "export_metadata/output/"
        import_directory = Path(testing_utilities.base_import_directory) / "template/"
        settings_path = Path(testing_utilities.pipeline_test_export_directory) / "export_metadata/"

        arguments_dict = {
            "--import": import_directory,
            "--export_directory": export_directory,
            "--unit_testing_export_path": export_directory,
            "--unit_testing": True,
            # you could expect...
            # "--export_meta_data": True,
            # "--export_meta_data_path": export_directory/"Model_info.json"
            # ...but it is
            "--metadata": export_directory/"Model_info.json"
        }

        if not os.path.exists(export_directory):
            os.makedirs(export_directory)

        arguments = ""
        for key, value in arguments_dict.items():
            arguments += str(key) + " " + str(value) + " "
        testing_utilities.start_blender(arguments, settings_path)

        jsonfilePath = ''
        for file in os.listdir(export_directory):
            if file.lower() == "unit_test.json":
                checkValue = True
                jsonfilePath = str(export_directory) + "/" + file

        data = json.load(open(jsonfilePath))

        if data['ExportMetaDataTask']:
            self.print_results("Check ExportMetaDataTask", bool(data['ExportMetaDataTask']), True)
            self.assertEqual(bool(data['ExportMetaDataTask']), True)

        self.assertIn("exported", data, "No metadata was exported")
        self.assertIn("reloaded", data, "Exported metadata could not be read")
        self.print_results("Metadata", data['reloaded'], data['exported'])
        self.assertEqual(data['exported'],data['reloaded'],"Exported and loaded metadata do not match")

        # testing_utilities.remove_files_in_folder(export_directory)