# -*- coding: utf-8 -*-
import os
from pathlib import Path
import unittest
import json

from app.unit_testing import testing_utilities

module = Path(os.path.dirname(os.path.abspath(__file__))).parent.parent.parent


class TestExportGeometry(unittest.TestCase):
    """This class tests only the export of a geometry and not the generation of a valid geometry"""

    def unit_test(self):
        """passing arguments to start blender"""
        export_directory = (
            Path(testing_utilities.pipeline_test_export_directory)
            / "export_geometry"
            / "output"
        )
        import_directory = Path(testing_utilities.base_import_directory)
        settings_path = (
            Path(testing_utilities.pipeline_test_export_directory).parent
            / "pipeline_cases"
            / "rooomready"
            / "setting.txt"
        )

        arguments_dict = {
            "--import": import_directory,
            "--export_directory": export_directory,
            "--unit_testing_export_path": Path(
                testing_utilities.pipeline_test_export_directory
            ).as_posix(),
            "--unit_testing": True,
            "--unit_testing_stage": str(self.__class__.__name__),
        }

        expected_data = json.load(
            open(
                Path(os.path.abspath(__file__)).parent
                / "unit_test_reference.json"
            )
        )
        # print(f"expected_data is {expected_data}")
        if not os.path.exists(export_directory):
            os.makedirs(export_directory)

        arguments = ""
        for key, value in arguments_dict.items():
            arguments += str(key) + " " + str(value) + " "

        testing_utilities.start_blender(arguments, settings_path)

        jsonfilePath = Path(export_directory) / "unit_test.json"
        try:
            tested_data = json.load(open(jsonfilePath))
        except Exception as e:
            print(e)
        else:
            check_items = {}
            for key in tested_data:
                try:
                    if isinstance(tested_data[key], (str, bool, int, float)):
                        check_items[key] = self.assertEqual(
                            tested_data[key], expected_data[key]
                        )
                    elif isinstance(tested_data[key], list):
                        check_items[key] = self.assertCountEqual(
                            tested_data[key], expected_data[key]
                        )
                except Exception as e:
                    check_items[key] = e

            all_pass = testing_utilities.unit_test_statistics(check_items)
            if all_pass:
                print("Removing the test export folder...")
                testing_utilities.remove_files_in_folder(export_directory)
            else:
                print("Please check the folder and see what went wrong")
