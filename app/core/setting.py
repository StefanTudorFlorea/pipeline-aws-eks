import sys
import json
import yaml
from pathlib import Path, PurePath, WindowsPath
import re

from app.core.abstract.singleton_meta import SingletonMeta

from app.core.log import Log

log = Log()

# @time_decorator
def is_number(item:str) -> bool:
    """Checks is the given argument is an integer or a float

    Args:
        item (string): the value to check

    Returns:
        bool: Returns True or False
    """

    # find either numbers linke (-)3333333.33333 or (-)333333
    regex =r"(-?[0-9]+[.][0-9]+)|(-?[0-9]+){1}"

    result = re.search(regex, item)

    if result and result.span()[1] == len(item):
        return True
    return False



def parse_list(data):
    arguments_dict = dict()
    for index, item in enumerate(data):
        if item.startswith("--"):
            if index != len(data) - 1 and not data[index + 1].startswith("--"):
                arguments_dict[item] = data[index + 1]
            else:
                arguments_dict[item] = True
    return arguments_dict


class Setting(object, metaclass=SingletonMeta):
    def __init__(self):

        self.export_setting_container = {}

        self.path = ""

        self.file_paths = []

        # Pipeline type
        # default: autobake
        self.export_polycount = False
        self.re_bake = False
        self.preview = False
        self.rooomready = False

        # Import
        self.import_path = ""
        self.import_no_material = False
        self.import_bake_resolution = "Test"

        # Export
        self.export_path = "Model.babylon"
        self.export_geometry_as_obj = False
        self.export_geometry_as_glb = False
        self.export_geometry_as_babylon = False
        self.export_textures = False
        self.export_directory = ""
        self.export_meta_data = False
        self.export_meta_data_path = ""
        self.export_folder = ""
        self.export_type_babylon = True
        self.export_log = True
        self.export_log_type = "txt"
        self.export_settings = True
        self.combine_metallic_roughness =True # it is always true for now.
        # Addons
        self.install_addons = False

        #   Misc
        self.misc_name = ""
        self.misc_recursive = False
        self.misc_run = False
        self.misc_batch = False
        self.misc_data_model = "basic"
        self.misc_keep_position = False
        self.misc_render_image = False
        self.misc_image_size = 512
        self.misc_imposter_image_size = 512
        self.misc_imposter_size = 4
        self.misc_imposter_samples = 4
        self.misc_imposter_rotation_steps = 16
        self.misc_imposter = False
        self.misc_turntable = False
        self.misc_turntable_rotation_steps = 32
        self.misc_turntable_name = "preview"
        self.misc_gif = True
        self.misc_edit_babylon = False
        self.primary_material_only = False
        self.write_uv = False
        self.validate_uvs = False
        self.validate_reflectivity = False

        #   Log
        self.debug_log = True

        #   Resolution
        self.baking_samples = 16

        #   Cage
        self.max_cage_offset = 0.1
        self.min_cage_offset = 0.0

        #   Misc
        ##  Tilesize
        self.misc_tilesize = 16
        self.misc_tilesize_cpu = 16
        self.misc_tilesize_gpu = 256

        ##  Device
        self.misc_device_gpu = True

        self.bake_margin = 32
        self.misc_samples = 1

        # Texture Data Type
        self.texture_format = "jpg"
        self.normal_format = "png"
        self.bake_format="jpeg"
        #self.texture_default_format = "jpeg"
        self.reflectivity_format="jpeg"

        # GPU & Texture
        # self.gpu = True
        self.texture_fallback_resolution = 1024
        self.texture_shadow_resolution = 256 # or 1024
        self.texture_resolution = 1024
        self.texture_default_resolution = 1024
        self.texture_max_size = 8192
        self.texture_bit_depth = "8"

        self.samples = 16
        self.super_sampling = 1

        self.bake_scale_factor = 1.0
        self.bake_shadow_margin = 0.6
        self.bake_margin = 5
        self.island_margin = 10
        self.border_padding = 10

        self.parent_geometries = False
        self.filter_meshes = False

        # Geometry
        self.geometry_resize = False
        self.geometry_resize_value = 1

        self.geometry_rotate_x = False
        self.geometry_rotate_x_value = 0

        self.geometry_rotate_y = False
        self.geometry_rotate_y_value = 0

        self.geometry_rotate_z = False
        self.geometry_rotate_z_value = 0

        self.geometry_recenter = False

        self.geometry_polygon_max = 120000
        self.geometry_polygon_target = 120000

        self.geometry_auto_scale = False
        self.geometry_scale = 0

        self.geometry_flatten = False
        self.geometry_flatten_value = 5
        self.geometry_filter_meshes = False
        self.geometry_remove_doubles = False
        self.geometry_remove_distance = 0.0001

        self.geometry_parent_additional_geometries_to_main_mesh = True
        self.geometry_parent_shadowplane_to_mesh = True
        self.geometry_edge_split = False
        self.geometry_edge_split_angle = 80.0
        self.geometry_recalc_normals = False
        self.geometry_smooth = False
        self.geometry_remove_invisible = False

        #   Real Estate
        self.re_compute_uv_scale = True
        self.re_unwrap = True
        self.re_resize_uv = True
        self.re_image_size = 2048

        #   Materials
        self.unwrap = False
        self.bake = True
        self.normalize_uv_space = False
        self.merge = True

        #   BAKING
        self.bake_shadow = True
        self.bake_shadowmap = True
        self.bake_textures = True
        self.bake_user_defined_plane_shadow = False # User can define parts of the mesh that can be handle differently, specifically, the shadowmap baking.
        self.bake_user_defined_glass_shadow = False
        #  Join
        self.join_materials = True
        self.join_geometry = True

        # Scene
        self.scene_name = "Scene"

        # Log
        self.log_error = False
        self.log_info = False
        self.log_warning = False

        self.decimate = False

        self.write_info = False
        self.render_image = False

        self.cpu = True

        # validation
        self.validate_shadow_map = True

        # Sanity check
        self.sanity_check_success = True

        # unit test
        self.unit_testing_export_path = ""
        self.unit_testing = False
        self.unit_testing_stage = ""

    def default_json(t):
        return f"{t}"

    def export_commands_json(self, file_path):
        dictionary = {"settings": [], "operations": []}
        obj_dic = self.__dict__
        for property, value in obj_dic.items():
            if not isinstance(value, dict):
                try:
                    if isinstance(value, bool):
                        command = {
                            "command": "--" + property,
                            "type": type(value).__name__,
                            "default": value,
                        }
                        dictionary["operations"].append(command)
                    else:
                        val_type = type(value).__name__
                        if val_type == "str":
                            val_type = "string"
                        command = {
                            "command": "--" + property,
                            "type": type(value).__name__,
                            "default": value,
                        }
                        dictionary["settings"].append(command)
                except Exception as e:
                    print(e)
        with open(Path(file_path) / "setting.json", "w") as outfile:
            json.dump(dictionary, outfile, indent=4)

    def export_json(self, file_path):
        dictionary = self.export_setting_container
        for key in dictionary:
            if isinstance(dictionary[key], list):
                for item in dictionary[key]:
                    if (
                        isinstance(item, Path)
                        or isinstance(item, PurePath)
                        or isinstance(item, WindowsPath)
                    ):
                        item = str(item)
            if (
                isinstance(dictionary[key], Path)
                or isinstance(dictionary[key], PurePath)
                or isinstance(dictionary[key], WindowsPath)
            ):
                dictionary[key] = str(dictionary[key])
            dictionary[key] = str(dictionary[key])

        with open(Path(file_path) / "setting.json", "w") as outfile:
            json.dump(dictionary, outfile, indent=4)

    def export_yaml(self, file_path):

        with open(Path(file_path) / "setting.yaml", "w") as outfile:
            yaml.dump(self.export_setting_container, outfile)

        # dictionary = self.__dict__
        # dictionary.pop("import_path", None)
        # dictionary.pop("export_path", None)
        # dictionary.pop("file_paths", None)
        # dictionary.pop("path", None)
        # dictionary.pop("install_addons", None)
        # with open(Path(file_path) / "setting.yaml", "w") as outfile:
        #     yaml.dump(dictionary, outfile)

    def export_txt(self, file_path):
        with open(Path(file_path) / "setting.txt", "w") as outfile:
            for key, value in self.export_setting_container.items():
                outfile.write(f"{key} = {value}\n")
            outfile.close

        # dictionary = self.__dict__
        # dictionary.pop("import_path", None)
        # dictionary.pop("export_path", None)
        # dictionary.pop("file_paths", None)
        # dictionary.pop("path", None)
        # dictionary.pop("install_addons", None)
        # with open(Path(file_path) / "setting.txt", "w") as outfile:
        #     for key, value in dictionary.items():
        #         outfile.write(f"{key} = {value}\n")
        #     outfile.close

    def import_json_from_file(self, file_path):
        with open(file_path, "r") as read_file:
            try:
                data = json.load(read_file)
                self.import_dict(data)
                log.info("Import Data from JSON File", file_path)
            except json.JSONDecodeError as exc:
                log.error(exc)

    def import_yaml_from_file(self, file_path):
        with open(file_path, "r") as read_file:
            try:
                data = yaml.safe_load(read_file)
                self.import_dict(data)
            except yaml.YAMLError as exc:
                log.error(exc)

    def import_txt_from_file(self, file_path):
        with open(file_path) as read_file:
            content = read_file.readlines()
        content = [x.strip("\n") for x in content]
        data_dict = {}
        for line in content:
            line = line.split("=")
            try:
                line[1] = line[1].strip()
                line[0] = line[0].strip()
                data_dict[line[0]] = line[1]
            except:
                pass
        self.import_dict(data_dict)
        log.info("Import Data from Text File", data_dict)

    def check_boolean(self, value):
        boolean_positive = ["True", "true", "Yes", "yes"]
        boolean_negative = ["False", "false", "No", "no"]
        if value in boolean_positive:
            return True
        elif value in boolean_negative:
            return False
        return True

    def import_dict(self, dict):
        try:
            for index, item in dict.items():
                index = str(index).lower()

                while len(index) > 0 and index[0] == "-":
                    index = index[1:]

                if index == "filepath" or index == "file_path":
                    if Path(item).is_dir():
                        self.path = Path(item).absolute().as_posix()
                    else:
                        log.error("Given File Path is not a Directory")
                        log.error(item)

                # Pipeline type
                if index in [
                    "polycount",
                    "poly_count",
                    "export_polycount",
                    "exportpolycount",
                ]:
                    self.append_export_container("polycount", self.check_boolean(item))
                    self.export_polycount = self.check_boolean(item)

                if index == "re_bake" or index == "real_estate":
                    self.append_export_container("re_bake", self.check_boolean(item))
                    self.re_bake = self.check_boolean(item)

                if index == "preview":
                    self.append_export_container("preview", self.check_boolean(item))
                    self.preview = self.check_boolean(item)
                if index == "rooomready":
                    self.append_export_container("rooomready", self.check_boolean(item))
                    self.rooomready = self.check_boolean(item)
                # Import
                if index in ["import_no_material", "importnomaterial", "no_material"]:
                    self.append_export_container(
                        "import_no_material", self.check_boolean(item)
                    )
                    self.import_no_material = self.check_boolean(item)

                if index in [
                    "import_bake_resolution",
                    "bake_resolution",
                    "import_bake_res",
                    "bake-res",
                ]:
                    item = str(item)
                    if item in ["Custom", "Test", "1k", "2k", "3k", "4k", "5k", "6k"]:
                        res = item
                    else:
                        res = "Test"
                    self.append_export_container("import_bake_resolution", str(res))
                    self.import_bake_resolution = res
                if index=="bake_shadow_margin":
                    margin_float =float(item)
                    self.append_export_container("bake_shadow_margin", margin_float)
                    self.bake_shadow_margin = margin_float
                # Export
                if index in [
                    "export_geometry_as_obj",
                    "exportgeometryasobj",
                    "export_obj",
                    "exportobj",
                ]:
                    self.append_export_container(
                        "export_geometry_as_obj", self.check_boolean(item)
                    )
                    self.export_geometry_as_obj = self.check_boolean(item)

                if index in [
                    "export_geometry_as_glb",
                    "exportgeometryasglb",
                    "export_glb",
                    "exportglb",
                ]:
                    self.append_export_container(
                        "export_geometry_as_glb", self.check_boolean(item)
                    )
                    self.export_geometry_as_glb = self.check_boolean(item)

                if index in [
                    "export_geometry_as_babylon",
                    "exportgeometryasbabylon",
                    "export_babylon",
                    "exportbabylon",
                ]:
                    self.append_export_container(
                        "export_geometry_as_babylon", self.check_boolean(item)
                    )
                    self.export_geometry_as_babylon = self.check_boolean(item)

                if index in [
                    "export_textures",
                    "exporttextures",
                    "export_texture",
                    "exporttextures",
                ]:
                    self.append_export_container(
                        "export_textures", self.check_boolean(item)
                    )
                    self.export_textures = self.check_boolean(item)

                if index == "export_directory" or index == "export":
                    self.export_directory = Path(item)

                if index in ["meta_data", "metadata", "write_info", "writeinfo"]:
                    if item:
                        self.export_meta_data_path = item
                        self.export_meta_data = True
                    else:
                        self.export_meta_data = self.check_boolean(item)

                if index in ["validate_shadow_map"]:
                    self.append_export_container(
                        "validate_shadow_map", self.check_boolean(item)
                    )
                    self.validate_shadow_map = self.check_boolean(item)

                # Misc
                if (
                    index == "import"
                    or index == "i"
                    or index == "import_path"
                    or index == "import_paths"
                ):
                    self.append_export_container("import", str(item))
                    self.import_path = str(item)

                if index == "name":
                    self.append_export_container("name", str(item))
                    self.misc_name = str(item)

                if index == "recursive":
                    self.append_export_container("recursive", self.check_boolean(item))
                    item = str(item)
                    self.misc_recursive = self.check_boolean(item)

                if index == "run":
                    self.append_export_container("run", self.check_boolean(item))
                    self.misc_run = self.check_boolean(item)

                if index == "batch":
                    self.append_export_container("batch", self.check_boolean(item))
                    self.misc_batch = self.check_boolean(item)

                if index == "keep_position" or index == "keepposition":
                    self.append_export_container(
                        "keep_position", self.check_boolean(item)
                    )
                    self.keep_position = self.check_boolean(item)

                if index == "render_image" or index == "renderimage":
                    self.append_export_container(
                        "render_image", self.check_boolean(item)
                    )
                    self.render_image = self.check_boolean(item)

                if index == "material_only" or index == "materialonly":
                    self.append_export_container(
                        "material_only", self.check_boolean(item)
                    )
                    self.primary_material_only = self.check_boolean(item)

                if index == "write_uv" or index == "writeuv":
                    self.append_export_container("write_uv", self.check_boolean(item))
                    self.write_uv = self.check_boolean(item)

                if index == "validate_uv" or index == "validateuv":
                    self.append_export_container(
                        "validate_uv", self.check_boolean(item)
                    )
                    self.validate_uvs = self.check_boolean(item)

                if index == "validate_reflectivity" or index == "validatereflectivity":
                    self.append_export_container(
                        "validate_reflectivity", self.check_boolean(item)
                    )
                    self.validate_reflectivity = self.check_boolean(item)

                if index == "imposter_samples" or index == "impostersamples":
                    self.append_export_container("imposter_samples", str(item))
                    self.misc_imposter = True
                    self.misc_imposter_samples = int(item)

                if index == "imposter_image_size" or index == "imposterimagesize":
                    self.append_export_container("imposter_image_size", str(item))
                    self.misc_imposter = True
                    self.misc_imposter_image_size = int(item)

                if index == "turntable_rotation_steps":
                    self.append_export_container("turntable_rotation_steps", str(item))
                    self.misc_turntable = True
                    self.misc_turntable_rotation_steps = int(item)

                if index == "turntable_name":
                    self.append_export_container("turntable_name", str(item))
                    self.misc_turntable = True
                    self.misc_turntable_name = str(item)

                if index == "gif" or index == "misc_gif":
                    self.append_export_container("gif", self.check_boolean(item))
                    self.misc_gif = self.check_boolean(item)

                if index == "imposter" or index == "misc_imposter":
                    self.append_export_container("imposter", self.check_boolean(item))
                    self.misc_imposter = self.check_boolean(item)

                if index == "turntable" or index == "misc_turntable":
                    self.append_export_container("turntable", self.check_boolean(item))
                    self.misc_turntable = self.check_boolean(item)

                if index in ["misc_edit_babylon", "edit_babylon"]:
                    self.append_export_container(
                        "misc_edit_babylon", self.check_boolean(item)
                    )
                    self.misc_edit_babylon = self.check_boolean(item)

                # Log
                if index == "debug_log" or index == "debuglog":
                    self.append_export_container("debug_log", self.check_boolean(item))
                    self.debug_log = self.check_boolean(item)

                # Resolution
                if index == "baking_samples" or index == "bakingsamples":
                    self.append_export_container("misc_tilesize", int(item))
                    self.baking_samples = int(item)

                # Tilesize
                if index == "tile_size" or index == "tilesize":
                    self.append_export_container("misc_tilesize", int(item))
                    self.misc_tilesize = int(item)

                if index == "tile_size_gpu" or index == "tilesizegpu":
                    self.append_export_container("misc_tilesize_gpu", int(item))
                    self.misc_tilesize_gpu = int(item)

                if index == "tile_size_cpu" or index == "tilesizecpu":
                    self.append_export_container("misc_tilesize_cpu", int(item))
                    self.misc_tilesize_cpu = int(item)

                # Device
                if index == "device_gpu" or index == "devicegpu":
                    self.append_export_container("device_gpu", self.check_boolean(item))
                    self.device_gpu = self.check_boolean(item)

                if index == "bake_margin" or index == "bakemargin":
                    self.append_export_container("bake_margin", int(item))
                    self.bake_margin = int(item)

                if index == "samples":
                    self.append_export_container("misc_samples", int(item))
                    self.misc_samples = int(item)

                # Texture Data Type
                if index == "texture_format" or index == "textureformat":
                    self.append_export_container("texture_format", str(item))
                    self.texture_format = "jpg" if str(item) == "jpeg" else str(item)

                if index == "shadow_format" or index == "shadowformat":
                    self.append_export_container("shadow_format", str(item))
                    self.shadow_format = "jpg" if str(item) == "jpeg" else str(item)

                if index == "normal_format" or index == "normalformat":
                    self.append_export_container("normal_format", str(item))
                    self.normal_format = "jpg" if str(item) == "jpeg" else str(item)

                if index == "default_format" or index == "defaultformat":
                    self.append_export_container("texture_default_format", str(item))
                    self.texture_default_format = (
                        "jpg" if str(item) == "jpeg" else str(item)
                    )

                # GPU & Texture
                if (
                    index == "fallback_resolution"
                    or index == "fallbackresolution"
                    or index == "texture_fallback_resolution"
                ):
                    self.append_export_container(
                        "texture_fallback_resolution", int(item)
                    )
                    self.texture_fallback_resolution = int(item)

                if (
                    index == "shadow_resolution"
                    or index == "shadowresolution"
                    or index == "texture_shadow_resolution"
                ):
                    self.append_export_container("texture_shadow_resolution", int(item))
                    self.texture_shadow_resolution = int(item)

                if index == "resolution" or index == "texture_resolution":
                    self.append_export_container("texture_resolution", int(item))
                    self.texture_resolution = int(item)

                if (
                    index == "default_resolution"
                    or index == "defaultresolution"
                    or index == "texture_default_resolution"
                ):
                    self.append_export_container(
                        "texture_default_resolution", int(item)
                    )
                    self.texture_default_resolution = int(item)

                if (
                    index == "max_size"
                    or index == "maxsize"
                    or index == "texture_max_size"
                ):
                    self.append_export_container("texture_max_size", int(item))
                    self.texture_max_size = int(item)

                if (
                    index == "bit_depth"
                    or index == "bitdepth"
                    or index == "texture_bit_depth"
                ):
                    self.append_export_container("texture_bit_depth", str(item))
                    self.texture_bit_depth = str(item)

                if index == "samples":
                    self.append_export_container("samples", int(item))
                    self.samples = int(item)

                if index == "super_sampling" or index == "supersampling":
                    self.append_export_container("super_sampling", int(item))
                    self.super_sampling = int(item)

                if index == "bake_scale_factor" or index == "bakescalefactor":
                    self.append_export_container("bake_scale_factor", float(item))
                    self.bake_scale_factor = float(item)

                if index == "bake_margin" or index == "bakemargin":
                    self.append_export_container("bake_margin", int(item))
                    self.bake_margin = int(item)

                if index == "island_margin" or index == "islandmargin":
                    self.append_export_container("island_margin", int(item))
                    self.island_margin = int(item)

                if index == "border_padding" or index == "borderpadding":
                    self.append_export_container("border_padding", int(item))
                    self.border_padding = int(item)

                if (
                    index == "parent_additional_geometries_to_main_mesh"
                    or index == "parentadditionalgeometriestomainmesh"
                ):
                    self.append_export_container(
                        "parent_additional_geometries_to_main_mesh",
                        self.check_boolean(item),
                    )
                    self.parent_additional_geometries_to_main_mesh = self.check_boolean(
                        item
                    )

                if index == "filter_meshes" or index == "filtermeshes":
                    self.append_export_container(
                        "filter_meshes", self.check_boolean(item)
                    )
                    self.filter_meshes = self.check_boolean(item)

                # Geometry
                if index == "resize":
                    self.append_export_container(
                        "geometry_resize", self.check_boolean(item)
                    )
                    self.geometry_resize = self.check_boolean(item)
                    if str(item).isnumeric():
                        self.geometry_resize_value = item

                if index == "rotate_x" or index == "rotatex":
                    self.append_export_container(
                        "geometry_rotate_x", self.check_boolean(item)
                    )
                    self.geometry_rotate_x = self.check_boolean(item)
                    if is_number(str(item)):
                        self.geometry_rotate_x_value = item

                if index == "rotate_y" or index == "rotatey":
                    self.append_export_container(
                        "geometry_rotate_y", self.check_boolean(item)
                    )
                    self.geometry_rotate_y = self.check_boolean(item)
                    if is_number(str(item)):
                        self.geometry_rotate_y_value = item

                if index == "rotate_z" or index == "rotatez":
                    self.append_export_container(
                        "geometry_rotate_z", self.check_boolean(item)
                    )
                    self.geometry_rotate_z = self.check_boolean(item)
                    if is_number(str(item)):
                        log.info("z is numeric")
                        self.geometry_rotate_z_value = item
                    else:
                        log.info("z is not numeric")

                if index == "recenter" or index == "geometry_recenter":
                    self.append_export_container(
                        "geometry_recenter", self.check_boolean(item)
                    )
                    self.geometry_recenter = self.check_boolean(item)

                if index == "polygon_max" or index == "polygonmax":
                    self.append_export_container(
                        "geometry_polygon_max", self.check_boolean(item)
                    )
                    self.geometry_polygon_max = int(item)

                if index == "polygon_target" or index == "polygontarget":
                    self.append_export_container(
                        "geometry_polygon_target", self.check_boolean(item)
                    )
                    self.geometry_polygon_target = int(item)

                if index == "auto_scale" or index == "autoscale":
                    self.append_export_container(
                        "geometry_auto_scale", self.check_boolean(item)
                    )
                    self.geometry_auto_scale = self.check_boolean(item)

                if index == "scale":
                    self.append_export_container(
                        "geometry_scale", self.check_boolean(item)
                    )
                    self.geometry_scale = int(item)

                if index == "flatten":
                    self.append_export_container(
                        "geometry_flatten", self.check_boolean(item)
                    )
                    self.geometry_flatten = self.check_boolean(item)
                    if str(item).isnumeric():
                        self.geometry_flatten_value = item

                if index == "remove_doubles" or index == "removedoubles":
                    self.append_export_container(
                        "geometry_remove_doubles", self.check_boolean(item)
                    )
                    self.geometry_remove_doubles = self.check_boolean(item)

                if index == "parent_to_mesh" or index == "parenttomesh":
                    self.append_export_container(
                        "geometry_parent_to_mesh", self.check_boolean(item)
                    )
                    self.geometry_parent_to_mesh = self.check_boolean(item)

                if (
                    index == "edge_split"
                    or index == "edgesplit"
                    or index == "geometry_edge_split"
                ):
                    self.append_export_container(
                        "geometry_edge_split", self.check_boolean(item)
                    )
                    self.geometry_edge_split = self.check_boolean(item)

                if index == "recalc_normals" or index == "recalcnormals":
                    self.append_export_container(
                        "geometry_recalc_normals", self.check_boolean(item)
                    )
                    self.geometry_recalc_normals = self.check_boolean(item)

                if index == "smooth":
                    self.append_export_container(
                        "geometry_smooth", self.check_boolean(item)
                    )
                    self.geometry_smooth = self.check_boolean(item)

                if index == "remove_invisible" or index == "removeinvisible":
                    self.append_export_container(
                        "geometry_remove_invisible", self.check_boolean(item)
                    )
                    self.geometry_remove_invisible = self.check_boolean(item)

                # Materials
                if index == "unwrap":
                    self.append_export_container("unwrap", self.check_boolean(item))
                    self.unwrap = self.check_boolean(item)

                if index == "bake":
                    self.append_export_container("bake", self.check_boolean(item))
                    self.bake = self.check_boolean(item)

                if (
                    index == "normalize_uv_space"
                    or index == "normalizeuvspace"
                    or index == "normalize_uv"
                    or index == "normalizeuv"
                ):
                    self.append_export_container(
                        "normalize_uv_space", self.check_boolean(item)
                    )
                    self.normalize_uv_space = self.check_boolean(item)

                # Bake
                if (
                    index == "bake_shadowmap"
                    or index == "bakeshadowmap"
                    or index == "bake_shadow"
                    or index == "bakeshadow"
                ):
                    self.append_export_container(
                        "bake_shadow", self.check_boolean(item)
                    )
                    self.bake_shadow = self.check_boolean(item)

                if (
                    index == "bake_textures"
                    or index == "baketextures"
                    or index == "bake_texture"
                    or index == "baketexture"
                ):
                    self.append_export_container(
                        "bake_textures", self.check_boolean(item)
                    )
                    self.bake_textures = self.check_boolean(item)

                # Join
                if (
                    index == "join_materials"
                    or index == "joinmaterials"
                    or index == "join_material"
                    or index == "joinmaterial"
                ):
                    self.append_export_container(
                        "join_materials", self.check_boolean(item)
                    )
                    self.join_materials = self.check_boolean(item)

                if index == "join_geometry" or index == "joingeometry":
                    self.append_export_container(
                        "join_geometry", self.check_boolean(item)
                    )
                    self.join_geometry = self.check_boolean(item)

                if index == "decimate":
                    self.append_export_container("decimate", self.check_boolean(item))
                    self.decimate = self.check_boolean(item)

                # Real Estate
                if index == "re_compute_uv" or index == "real_estate_compute_uv":
                    self.append_export_container(
                        "re_compute_uv_scale", self.check_boolean(item)
                    )
                    self.re_compute_uv_scale = self.check_boolean(item)

                if index == "re_unwrap" or index == "real_estate_unwrap":
                    self.append_export_container("re_unwrap", self.check_boolean(item))
                    self.re_unwrap = self.check_boolean(item)

                if index == "re_resize_uv" or index == "real_estate_resize_uv":
                    self.append_export_container(
                        "re_resize_uv", self.check_boolean(item)
                    )
                    self.re_resize_uv = self.check_boolean(item)

                if index == "re_image_size" or index == "real_estate_image_size":
                    self.append_export_container("re_image_size", int(item))
                    self.re_image_size = int(item)

                if index == "unit_testing_export_path":
                    self.append_export_container("unit_testing_export_path", str(item))
                    self.unit_testing_export_path = str(item)
                if index == "unit_testing":
                    self.append_export_container(
                        "unit_testing", self.check_boolean(item)
                    )
                    self.unit_testing = self.check_boolean(item)
                if index == "unit_testing_stage":
                    self.unit_testing_stage = item
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = Path(exc_tb.tb_frame.f_code.co_filename).name
            # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        self.post_evaluation()

    def all_members(self):
        try:
            # Try getting all relevant classes in method-resolution order
            mro = list(self.__mro__)
        except AttributeError:
            # If a class has no _ _mro_ _, then it's a classic class
            def getmro(aClass, recurse):
                mro = [aClass]
                for base in aClass.__bases__:
                    mro.extend(recurse(base, recurse))
                return mro

            mro = getmro(self, getmro)
        mro.reverse()
        members = {}
        for someClass in mro:
            members.update(vars(someClass))
        return members

    def append_export_container(self, var_name, new_value):
        self.export_setting_container[var_name] = new_value
        # old_value = self.all_members()[var_name]
        # if old_value != new_value:
        #     self.export_setting_container.append(f"{var_name} = {new_value}")

    def post_evaluation(self):
        if self.misc_turntable or self.export_polycount:
            # self.export_geometry_as_babylon = False
            self.export_geometry_as_glb = False
            self.export_geometry_as_obj = False
            self.export_textures = False
            self.export_log = False

    def to_dict(self):
        temp_dict = self.__dict__
        return temp_dict
