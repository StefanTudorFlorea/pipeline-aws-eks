from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List

from app.core.utilities import export

from app.core.log import Log

export

from app.core.setting import Setting

setting = Setting()

log = Log()


class AbstractTask(ABC):
    """
    The Task Interface declares a set of visiting methods that correspond to
    model classes. The signature of a visiting method allows the task to
    identify the exact class of the model that it's dealing with.
    """

    def __init__(self):
        self.priority = 0
        log.info("INITIALIZE %s" % str(self.__class__.__name__))

    def poll(self) -> bool:
        """
        Poll checks the current state of the model and context for compatibility
        with it's behaviour. If the requirements are not met the task can not be
        performed.
        """
        return True

    def cleanup(self):
        """
        Restores scene and context to their state before the task's execution
        """
        pass

    @abstractmethod
    def execute(self) -> None:
        """[Execute a collection of actions bundled in a task]"""
        pass

    @abstractmethod
    def unit_test(self):
        """
        Execute the task and then check if the changes are correct and export the unit test results as a json
        """
        pass
