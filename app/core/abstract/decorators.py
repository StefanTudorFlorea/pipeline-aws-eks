import time
import functools
from app.core.log import Log

log = Log()


def time_decorator(func):
    def wrapper_function(*args, **kwargs):
        time_start = time.perf_counter()

        result = func(*args, **kwargs)

        log.time(func_name=func.__name__, elapsed_time=time.perf_counter() - time_start)
        return result

    return wrapper_function
