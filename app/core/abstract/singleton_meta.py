class SingletonMeta(type):
    """
    The Singleton class can be implemented in different ways in Python. Some
    possible methods include: base class, decorator, metaclass. We will use the
    metaclass because it is best suited for this purpose.
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

    # @property
    # def __bases__(cls):
    #     print('Getting __bases__ via Meta.__bases__')
    #     return cls._find_super('__bases__').__get__(cls)

    # @__bases__.setter
    # def __bases__(cls, value):
    #     print('Setting __bases__ via Meta.__bases__')
    #     return cls._find_super('__bases__').__set__(cls, value)
