from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List


class AbstractModel(ABC):
    """The Abstract meta class for a model

    Args:
        ABC (abstract): None
    """

    @abstractmethod
    def accept(self, task) -> None:
        pass
