"""Loggings

Args:
    configuration: log configure via files

"""
import os
from pathlib import Path
import string
import sys
import queue
import socket
import subprocess
import threading
import time
import logging
import logging.config
from app.core.abstract.log_meta import LogMeta

sys.setrecursionlimit(10000)

module = Path(__file__).parent.resolve()


class Log(metaclass=LogMeta):
    def __init__(self):
        log_conf_path = "logging.conf"
        log_export_path = "app/core/log/"
        if not os.path.exists(log_export_path):
            os.makedirs(log_export_path,exist_ok=True)
        logging.addLevelName(15, "TIME")
        logging.addLevelName(5, "BLENDER")
        logging.config.fileConfig(
            log_conf_path, defaults={"logfilename": log_export_path}
        )

        self.loggers = {
            "main": logging.getLogger("main"),
            "developer": logging.getLogger("developer"),
            "backend": logging.getLogger("backend"),
        }

    def __getitem__(self, loggernames):
        if isinstance(loggernames, str):
            return self.loggers[loggernames]
        return self.loggers["main"]

    def print_pretty(self, args, indent=0):
        """
        Taken from previous logging

        For convenience passed arguments will be displayed as list. Call to python's log library require
        format string if arguments as used. We do not use format string, instead we traverse structures
        as dicts and lists recursively and build a string with list of traversed items.

        :return: String containing given arguments or traversed arguments from given structure
        """
        tab = " " * indent * 2
        msgs = []
        if isinstance(args, list):
            msgs.append(tab + "Items:")
            for item in args:
                for msg in self.print_pretty(item, indent + 1):
                    msgs.append(tab + msg)
        elif type(args) == dict:
            msgs.append(tab + "Key, Values:")
            for k, v in args.items():
                for msg in self.print_pretty(v, indent + 1):
                    msgs.append(f"{tab}{k}={msg}")
        elif isinstance(args, str):
            for msg in args.split("\n"):
                msgs.append(tab + msg)
        else:
            msgs.append(tab + str(args))
        return msgs

    def _log(self, level, msg, *args, loggernames="main"):
        if isinstance(loggernames, str) or loggernames is None:
            names = [loggernames or "main"]
        else:
            names = loggernames
        if len(args) > 0:
            msgs = self.print_pretty(args)
        else:
            msgs = [msg]

        for name in names:
            for msg in msgs:
                self.loggers[name].log(level, msg)

    def blender(self, msg, *args, loggernames="main"):
        """debug message
        Forward logging from code running inside blender
        Args:
            msg (string): report debug message
            loggernames: str or iterable; loggers to use
        """
        self._log(5, msg, *args, loggernames=loggernames)

    # define, debug, info, warning, error, critical
    def debug(self, msg, *args, loggernames="main"):
        """debug message
        A general use case
        Args:
            msg (string): report debug message
            loggernames: str or iterable; loggers to use
        """
        self._log(logging.DEBUG, msg, *args, loggernames=loggernames)

    def info(self, msg, *args, loggernames="main"):
        """info message
        Report current tasks.

        Args:
            msg (string): report info message.
        """
        self._log(logging.INFO, msg, *args, loggernames=loggernames)

    def warning(self, msg, *args, loggernames="main"):
        """warning message
        Capture the warnings, the pipeline can still run even there is a warning.

        Args:
            msg (string): report warning message
        """
        self._log(logging.WARNING, msg, *args, loggernames=loggernames)

    def error(self, msg, *args, debug=False, send=False, loggernames="main"):
        """error message
        Capture the errors, the pipeline cannot run with errors.

        Args:
            msg (string): report error message
        """
        self._log(logging.ERROR, msg, *args, loggernames=loggernames)
        if send:
            print("@--error@True")
            sys.stdout.flush()
            if isinstance(msg, list):
                msgs = ""
                for item in msg:
                    msgs += "{} ".format(item)
                msg = msgs
            print("@--error_msg@" + str(msg))
            sys.stdout.flush()

    def critical(self, msg, *args, debug=False, loggernames="main"):
        """critical message
        Capture the critical issues, the task abort immediately
        Args:
            msg (string): report critical message
            debug (bool, optional): use at debug stage or not. Defaults to False.
        """
        if debug:
            self._log(logging.DEBUG, msg, *args, loggernames=loggernames)
        self._log(logging.CRITICAL, msg, *args, loggernames=loggernames)

    def time(
        self, func_name, elapsed_time, parameter="", debug=False, loggernames="main"
    ):
        """duration time.

        Args:
            msg (string): the duration of the process
        """
        self._log(
            15,
            f"{func_name} executed in {elapsed_time}s Parameter: '{parameter}'",
            loggernames=loggernames,
        )
        # self.logger.info(msg)


# class Log(metaclass=LogMeta):
#     def __init__(self):
#         self.columns = ["date", "type", "msg", "parameter"]
#         self.log_frame = pd.DataFrame(columns=self.columns)
#         self.log_frame.style.set_properties(**{'text-align': 'left'})
#         self.print_error = True
#         self.logfile = module.joinpath("logs/log.txt")
#         # self.logfile.write_text("INIT")

#     def get_formatted_time(self):
#         """Returns the current time formatted

#         Returns:
#             date: Current date time to the seconds
#         """
#         dt = datetime.datetime.now()

#         formatted_time = dt.strftime("%Y-%m-%d %H:%M:%S")
#         return formatted_time

#     def clear(self):
#         """Clears the current Dataframe used for logging"""
#         del self.log_frame
#         self.log_frame = pd.DataFrame(columns=self.columns)
#         self.log_frame.style.set_properties(**{'text-align': 'left'})

#     def print(self, index):
#         """Prints a Log Dataframe row with a formatted string by Index

#         Args:
#             index (int): Index of Log Dataframe

#         Returns:
#             string: [description]
#         """
#         try:
#             return print(
#                 "{}\t{}:\t{}\t{}".format(
#                     str(self.log_frame[index, "date"]),
#                     str(self.log_frame[index, "type"]),
#                     str(self.log_frame[index, "msg"]),
#                     str(self.log_frame[index, "parameter"]),
#                 )
#             )
#         except BaseException as e:
#             return print(
#                 "{}\t{}:\t{}".format(str(self.get_formatted_time()), "ERROR", str(e))
#             )

#     def print_all(self):
#         """Prints a complete Log Dataframe row with a formatted string

#         Returns:
#             string: [description]
#         """
#         print(self.log_frame.to_string(justify="justify"))

#     def print_all_pretty(self):
#         """Prints a complete Log Dataframe rows with a formatted string in a pretty way for the console output"""
#         frame = self.log_frame
#         if self.print_error:
#             frame = frame[(frame["type"] == "ERROR")]
#         for index, row in frame.iterrows():
#             try:
#                 sys.stderr.write("{}\t{}\n".format(str(row["date"]), str(row["type"])))
#                 msgs = ""
#                 if isinstance(row["msg"], list):
#                     for item in row["msg"]:
#                         msgs += "{} ".format(item)
#                 elif isinstance(row["msg"], tuple):
#                     msgs = "".join(row["msg"])
#                 else:
#                     msgs = str(row["msg"]).split("\n")
#                 for msg in msgs:
#                     sys.stderr.write("$ " + msg + "\n")
#                 # output += str(msgs)
#                 # print("\n")

#             except Exception as e:
#                 sys.stderr.write("")

#     def pretty_print(self):
#         table = ""
#         for row in self.log_frame.itertuples():
#             table.add_row(row[1:])
#         return str(table)

#     def write_log(self, msg, tpe, parameter=""):
#         """
#         Append a log entry to the log frame.
#         """
#         time = self.get_formatted_time()
#         entry = {
#             "date": time,
#             "type": tpe,
#             "msg": f"{msg}",
#             "parameter": f"{parameter}",
#         }
#         self.log_frame = self.log_frame.append(entry, ignore_index=True)
#         # self.export(self.logfile)

#     def info(self, msg, parameter="", debug=False):
#         """Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         if debug:
#             self.debug(msg)
#         self.write_log(msg, "INFO", parameter)

#     def time(self, msg, parameter="", debug=False):
#         """Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         self.write_log(msg, "TIME", parameter)

#     def debug(self, msg, parameter=""):
#         """Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         self.write_log(msg, "DEBUG", parameter)

#     def warning(self, msg, parameter="", debug=False):
#         """Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         if debug:
#             self.debug(msg)
#         self.write_log(msg, "WARNING", parameter)

#     def error(self, msg, parameter="", debug=False, send=False):
#         """
#         Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         if debug:
#             self.debug(msg)
#         self.write_log(msg, "ERROR", parameter)

#         if send:
#             print("@--error@True")
#             sys.stdout.flush()

#             if isinstance(msg, list):
#                 msgs = ""
#                 for item in msg:
#                     msgs += "{} ".format(item)
#                 msg = msgs
#             print("@--error_msg@" + str(msg))
#             sys.stdout.flush()

#     def critical(self, msg, parameter="", debug=False):
#         """Logs that are not warnings or errors.

#         Args:
#             msg (string): Dumps Log Message for informations in the log class
#         """
#         if debug:
#             self.debug(msg)
#         self.write_log(msg, "CRITICAL", parameter)

#     def export(self, filepath):
#         """
#         Export a Log Dataframe to the data format encoded in the suffix

#         Args:
#             filepath (string): Filepath to export file
#         """
#         filepath = Path(filepath)

#         export_functions = {
#             ".xlsx": self.log_frame.to_excel,
#             ".csv": self.log_frame.to_csv,
#             ".json": self.log_frame.to_json,
#             ".txt": self.log_frame.to_string,
#         }

#         if filepath.suffix not in export_functions.keys():
#             raise Exception("Export format not supported")

#         export_function = export_functions[filepath.suffix]
#         export_function(filepath.open(mode="w"))

#     def export_excel(self, folder):
#         """Export a Log Dataframe to .xlsx (excel) data format

#         Args:
#             folder (string): folder containing export file
#         """
#         if not os.path.isdir(folder):
#             raise Exception("Must be a folder")
#         self.export(Path(folder) / "log.xlsx")

#     def export_csv(self, folder):
#         """Export a Log Dataframe to .csv data format

#         Args:
#             folder (string): folder containing export file
#         """
#         if not os.path.isdir(folder):
#             raise Exception("Must be a folder")
#         self.export(Path(folder) / "log.csv")

#     def export_json(self, folder):
#         """Export a Log Dataframe to .json data format

#         Args:
#             folder (string): folder containing export file
#         """
#         if not os.path.isdir(folder):
#             raise Exception("Must be a folder")
#         self.export(Path(folder) / "log.json")

#     def export_txt(self, folder):
#         """Export a Log Dataframe to txt data format

#         Args:
#             folder (string): folder containing export file
#         """
#         if not os.path.isdir(folder):
#             raise Exception("Must be a folder")
#         self.export(Path(folder) / "log.txt")
