import sys
import json
from app.core.abstract.model_meta import ModelMeta


class Model(metaclass=ModelMeta):
    """rooomReady self defined model"""

    def __init__(self):
        # Name for the exported file
        self.name = "Model"
        self.file_type = ""

        # imported 3D models from one file which can contain multiple models
        # Only holds geometries from the keywords
        self.imported_objects = []
        # stores the images from the ImportDataTask
        self.unassigned_textures = set()

        # Merged geometries into one object
        self.object = None

        # extract maximum texture resolution in baking.py
        self.textures = set()

        self.diffuse = []
        self.normal = []
        self.emission = []
        self.metallic = []
        self.roughness = []
        self.reflectivity = []
        self.shadow = []
        self.shadowmap = []

        # temporary, will be removed later
        # Nothing is processed here
        self.material_replacements = {
            "0": set(),
            "1": set(),
            "2": set(),
            "3": set(),
            "MS": set(),  # shadow
            "MG": set(),  # glass
            "MP": set(),  # plane
            "ML": set(),  # label
        }

        # plane where the shadow is baked on
        self.shadow_plane = None

        # plane where the ao is baked on
        self.shadowmap_plane = None

        # texture with the baked shadow, results of shadow baking with the shadow plane
        self.baked_shadow = None

        # TODO: Group textures to materials and assign them to the best fitting material (e.g. by name: Material_0_diffuse.jpg -> D.materials["Material_0"])
        self.materials = set()

        # Planes from the backend system
        self.planes = []
        self.glass = []
        self.labels = []

        # special case used by 3D developers
        # RGB Channel decoding for e.g. primary, secondary, tertiary, etc
        self.mask = None

        # when there are multiple objects in the same directory they are distinguished by their name
        self.filter_by_name = False

        # backend info export UV scaling
        self.scaleX = self.scaleY = None

    def get_backend_compliant_name(self, suffix=None):
        if not suffix:
            return self.name
        return f"{self.name}.{suffix}"

    def get_material_meta_data(self, material):
        if not material:
            return {}
        if "images" not in material.keys():
            return {"name": material.name}
        return {
            "name": material.name,
            "images": [
                f"{image.name} ({image['category']})" if image else {}
                for image in material["images"]
            ],
            "textures": [
                node.image.name
                for node in material.node_tree.nodes
                if node.type == "TEX_IMAGE"
            ],
        }

    def append(self, attr, item):
        """append elements to the model attribute

        Args:
            attr (str): an attribute in the model
            item : string or bpy object
        """
        if hasattr(self, attr):
            self.__dict__[attr].append(item)

    def get_meta_data(self):
        data_string = {
            "name": str(self.name),
            "materials": str(len(self.materials)),
            # [self.get_material_meta_data(material) for material in self.object.data.materials],
            "vertices": str(len(self.object.data.vertices.values()))
            if self.object
            else "0",
            "triangles": str(
                len(
                    [
                        p
                        for p in self.object.data.polygons
                        if len(p.vertices) == 3
                    ]
                )
            )
            if self.object
            else "0",
            "quads": str(
                len(
                    [
                        p
                        for p in self.object.data.polygons
                        if len(p.vertices) == 4
                    ]
                )
                if self.object
                else "0"
            ),
            "polycount": str(len(self.object.data.polygons))
            if self.object
            else "0",
            # "normals": [{
            #    "x":normal.x,
            #    "y":normal.y,
            #    "z":normal.z} for normal in [polygon.normal @ self.object.matrix_world for polygon in self.object.data.polygons]],
            "dimensions_x": str(self.object.dimensions.x)
            if self.object
            else "0",
            "dimensions_y": str(self.object.dimensions.y)
            if self.object
            else "0",
            "dimensions_z": str(self.object.dimensions.z)
            if self.object
            else "0",
            "diagonal": str(self.object.dimensions.length)
            if self.object
            else "0",
            # "has_uvs": bool(self.object.data.uv_layers),
            # "uv_layers": [{"name": layer.name} for layer in self.object.data.uv_layers],
            # "planes": [mesh_to_dict(plane) for plane in self.planes],
            # "glass": [mesh_to_dict(glass) for glass in self.glass],
            # "labels": [mesh_to_dict(label) for label in self.labels],
            # "mask": str(self.mask)
        }

        return data_string

    def print_encode_data(self, data_dict):
        encoded_data_dict = ""
        for key, value in data_dict.items():
            encoded_data_dict = "@--{}@{} ".format(
                str(key), str(" " if value is None else value)
            )
            print(encoded_data_dict)
            sys.stdout.flush()
            # websocket_client.local_message(encoded_data_dict)

    # def write_temp_json(self, dict):
    #     if dict:
    #         with open("./temp_log.json", "r+") as f:
    #             json.dumps(dict, f)
