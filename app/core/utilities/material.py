import numbers
import bpy
import bmesh
import math
import io
import re
from pathlib import Path
from contextlib import redirect_stdout
from app.core.utilities.misc import deselect_everything
from app.core.abstract.decorators import time_decorator
from app.core.utilities.texture import Texture
from app.core.utilities.geometry import world_min_max, create_ground_plane, parent
import app.core.utilities.keywords as keywords
from utilities.export import export_image_to_filepath as export_image
from app.core.setting import Setting
setting = Setting()

from app.core.log import Log
log = Log()

from app.core.model import Model
model = Model()


def bake_shadow_or_shadowmap(model, image_type="shadow", bake_shadowmap=True, file_extension=".jpg"):
    bpy.data.scenes["Scene"].render.bake.margin = 32  # 16
    bpy.data.scenes["Scene"].render.bake.use_clear = True
    bpy.context.scene.render.engine = "CYCLES"
    bpy.context.scene.cycles.samples = 48
    bpy.context.scene.view_settings.view_transform = "Standard"

    if bake_shadowmap:
        model.object.select_set(True)
        bpy.context.view_layer.objects.active = model.object
    else:
        model.shadow_plane.select_set(True)
        bpy.context.view_layer.objects.active = model.shadow_plane

    output_filename = f"{setting.export_directory}/{model.object.name}_{image_type}{file_extension}"
    image_name = f"{model.object.name}_{image_type}"

    bake_image = bpy.data.images[image_name]

    bpy.ops.object.bake(
        type="EMIT",
        use_selected_to_active=False,
        target="IMAGE_TEXTURES",
        save_mode="EXTERNAL",
    )

    bake_image.filepath_raw=output_filename#str(Path(output_filename).absolute().resolve())
    # model_render_image = bpy.data.images[image_name]

    if bake_shadowmap:
        bake_image["category"] = "shadowmap"
        log.info(f"BAKE SHADOWMAP image is {bake_image}")
    else:
        bake_image["category"] = "shadow"
        log.info(f"BAKE SHADOW image is {bake_image}")
    bake_image.pack()
    return bake_image

def setup_shadowplan(type:str, margin:numbers)-> object:
    """Creates a shadowplan with min and max boundings of the imported object + some margin

    Args:
        type (str): Name of shadow to bake. Either 'shadowmap' or 'shadow'
        margin (numbers): Extra margin that is added to the dimension of the plane

    Returns:
        object: The created shadowplane
    """
    all_objects = [model.object]
    all_objects.extend([sub_object for sub_object in (model.glass + model.planes) if sub_object])

    minimum_corner, maximum_corner = world_min_max(all_objects, reset_z=True)
    log.info(f"minimum_corner is {minimum_corner}, maximum_corner is {maximum_corner}")
    shadow_plane = None
    shadow_plane = create_ground_plane(minimum_corner, maximum_corner, margin=margin, name=f"{type}_plane")

    shadow_plane.name = model.object.name+f"_{type}"
    parent(shadow_plane, model.object)
    shadow_plane["category"] = type

    log.info(f"{type}_plane is: {shadow_plane.name}")

    return shadow_plane

def attach_imgage_to_node(material_name, image_name, width= None , height = None, node_name="ShaderNodeTexImage" ):
    """create an image and attach to a node

    Args:
        material_name (str): the material name
        image_name (str): the desired name
        node_name (str, optional): a node to add. Defaults to "ShaderNodeTexImage".

    Returns:
        node : bpy node
    """
    bpy.data.materials[material_name].use_nodes = True
    node_tree = bpy.data.materials[material_name].node_tree
    node = node_tree.nodes.new(node_name)#['Image Texture']
    node.select = True
    node_tree.nodes.active = node
    img = bpy.data.images.new(
        image_name, #f"{model.object.name}_shadowmap"
        (width if width else setting.texture_shadow_resolution),
        (height if height else setting.texture_shadow_resolution)
    )
    node.image = img
    return node

def load_imgage_to_shader_node(material_name, image_filepath, node_name="ShaderNodeTexImage"):
    """attach a loaded image to a material's bsdf node

    Args:
        material_name (str): the material name
        image (str): filepath
        node_name (str, optional): a node to add. Defaults to "ShaderNodeTexImage".

    Returns:
        node.image or e
    """
    material = bpy.data.materials[material_name]
    if not material:
        return False
    material.use_nodes = True
    node_tree = material.node_tree
    node = node_tree.nodes.new(node_name)
    node.select = True
    node.image = bpy.data.images.load(image_filepath)
    bsdf = node_tree.nodes["Principled BSDF"]
    try:
        material.node_tree.links.new(node.outputs["Color"], bsdf.inputs["Base Color"])
        return node.image
    except Exception as e:
        log.info(f"error is {e}")
        raise e


def replace_material_with_rooom_material(blender_objects, replace_material):
    """replace object's material with rooom's material

    Args:
        blender_objects (list): a list of objects to be replaced their material
        replace_material (bpy material): rooom's material

    Returns:
        dict: a dictionary of objects that the materials are replaced
    """
    material_dictionary = {}
    for object in blender_objects:
        if object.data.materials:
            material_slots = object.material_slots
            for idx, m in enumerate(material_slots):
                material = m.material
                material_dictionary[object] = {"slot_index": idx, "slot": m, "material": material}
                object.material_slots[idx].material = replace_material
                #object.material_slots[idx].material = shadowmap_shader
                log.info(f"object.data.material_slots {object.material_slots}")
        else:
            continue
    return material_dictionary

def restore_material(mat_dict):
    """restore material back to where it was

    Args:
        mat_dict (dict): key is object, value is an array contains material info
    """
    for obj, original_material in mat_dict.items():
        try:
            find_obj=bpy.context.scene.objects.get(obj.name, None)
        except Exception as e:
            log.error(f"object {obj} not found. {e}") # and break, or?
            return None
        else:
            slot_index = original_material["slot_index"]
            material = original_material["material"]
            obj.material_slots[slot_index].material = material
            log.info(f"material {material.name} is restored")
            return True

def delete_an_object(blender_object):
    """delete an object

    Args:
        blender_object (bpy object): The object to be deleted

    Returns:
        check_object_if_deleted: True if the object is deleted. False if the object is not deleted.
    """
    target_object_name = blender_object.name
    bpy.ops.object.select_all(action='DESELECT') # deselect everything for the safety.
    bpy.data.objects[blender_object.name].select_set(True)
    bpy.ops.object.delete()
    if not bpy.context.scene.objects.get(target_object_name, None):
        return True
    else:
        return False

@time_decorator
def unwrap(blender_objects, island_margin=0.1, layer_name="UVMap"):
    """unwrap one or more blender object(s)

    Args:
        blender_objects ([type]): [description]
        island_margin (float, optional): [description]. Defaults to 0.1.
        layer_name (str, optional): [description]. Defaults to "UVMap".
    """
    """ Unwraps geometries in one uv layer."""
    blender_objects = (
        blender_objects if isinstance(blender_objects, list) else [blender_objects]
    )
    for mesh in blender_objects:
        mesh.data.uv_layers.active = mesh.data.uv_layers.new(name=layer_name)
    bpy.context.view_layer.objects.active = blender_objects[0]
    for mesh in blender_objects:
        mesh.select_set(True)
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.context.view_layer.objects.active = blender_objects[0]
    for mesh in blender_objects:
        mesh.select_set(True)
    bpy.ops.mesh.select_all(action="SELECT")
    bpy.ops.uv.select_all(action="SELECT")
    bpy.ops.uv.smart_project(island_margin=island_margin)
    bpy.ops.object.mode_set(mode="OBJECT")


@time_decorator
def scaleUVs(blender_object, resize) -> list:
    """_summary_

    Args:
        blender_object (_type_): _description_
        resize (_type_): _description_

    Returns:
        list: _description_
    """
    bpy.context.view_layer.objects.active = blender_object
    bm = bmesh.new()
    bm.from_mesh(blender_object.data)
    nm = blender_object.name
    uvl = bm.loops.layers.uv.active
    bmesh.ops.triangulate(
        bm, faces=bm.faces[:], quad_method="BEAUTY", ngon_method="BEAUTY"
    )
    uv = 0

    # compute uv bounding box
    uv_min, uv_max = [[+math.inf, +math.inf], [-math.inf, -math.inf]]
    for face in bm.faces:
        for loop in face.loops:
            uv = loop[uvl].uv
            if uv[0] < uv_min[0]:
                uv_min[0] = uv[0]
            if uv[1] < uv_min[1]:
                uv_min[1] = uv[1]
            if uv[0] > uv_max[0]:
                uv_max[0] = uv[0]
            if uv[1] > uv_max[1]:
                uv_max[1] = uv[1]
    scale = [1.0 / (uv_max[0] - uv_min[0]), 1.0 / (uv_max[1] - uv_min[1])]
    # print('uvmin: {}\nuvmax: {}\nscale: {}'.format(uv_min, uv_max, scale))
    # transform uvs
    if resize:
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.uv.select_all(action="SELECT")
        # wdw = bpy.context.area.ui_type
        # bpy.context.area.ui_type = 'UV'
        # bpy.ops.uv.cursor_set(location=(0, 0))
        # bpy.context.space_data.cursor_location[0] = 0
        # bpy.context.space_data.cursor_location[1] = 0
        bpy.ops.transform.translate(
            value=(-uv_min[0], -uv_min[1], 0),
            constraint_axis=(False, False, False),
            orient_matrix_type="GLOBAL",
            mirror=0,
            use_proportional_edit=0,
            proportional_edit_falloff="LINEAR",
            proportional_size=1.0,
        )
        # bpy.context.space_data.pivot_point = 'CURSOR'
        bpy.ops.transform.resize(
            value=(scale[0], scale[1], 1.0),
            constraint_axis=(False, False, False),
            orient_matrix_type="GLOBAL",
            mirror=0,
            use_proportional_edit=0,
            proportional_edit_falloff="LINEAR",
            proportional_size=1.0,
        )
        # bpy.context.area.ui_type = wdw
        bpy.ops.object.mode_set(mode="OBJECT")
    # update mesh and compute uv density
    bm = bmesh.new()
    bm.from_mesh(blender_object.data)
    uvl = bm.loops.layers.uv.active
    bmesh.ops.triangulate(
        bm, faces=bm.faces[:], quad_method="BEAUTY", ngon_method="BEAUTY"
    )
    uv_density = 0
    for face in bm.faces:
        v = []
        for loop in face.loops:
            v.append(loop[uvl].uv)
        # calculate area with trapezoid formula
        uv_density += 0.5 * (
            v[0][0] * (v[1][1] - v[2][1])
            + v[1][0] * (v[2][1] - v[0][1])
            + v[2][0] * (v[0][1] - v[1][1])
        )
    # return calculated data
    surf_area = sum(f.calc_area() for f in bm.faces)
    sc = math.sqrt(abs(surf_area * (1 / uv_density)))
    # print('surface: {}\nuv density: {}\nscale: {}'.format(surf_area, uv_density, sc))
    return [scale[1] * sc, scale[0] * sc, uv_density]


@time_decorator
def real_estate_unwrap(blender_object):
    """_summary_

    Args:
        blender_object (_type_): _description_
    """
    bpy.context.view_layer.objects.active = blender_object
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_all(action="SELECT")
    with redirect_stdout(io.StringIO()):
        bpy.ops.uv.smart_project(
            angle_limit=66.0, island_margin=0.03, scale_to_bounds=False
        )
    bpy.ops.object.mode_set(mode="OBJECT")


@time_decorator
def pack(
    blender_objects,
    island_margin=10,
    border_padding=10,
    texture_resolution=1024,
    passes=2,
):
    """Packs the passed blender_objects uvs.
    Important is that the blender_objects have been unwrapped before.
    Requires uvpackmaster addon.

    Args:
        blender_objects ([type]): [description]
        island_margin (int, optional): [description]. Defaults to 10.
        border_padding (int, optional): [description]. Defaults to 10.
        texture_resolution (int, optional): [description]. Defaults to 1024.

    """
    bpy.context.scene.uvp2_props.precision = 500
    bpy.context.scene.uvp2_props.pixel_margin = island_margin
    bpy.context.scene.uvp2_props.pixel_padding = border_padding
    bpy.context.scene.uvp2_props.pixel_margin_tex_size = texture_resolution

    bpy.context.view_layer.objects.active = blender_objects[0]
    blender_objects[0].select_set(True)
    bpy.ops.object.mode_set(mode="EDIT")
    deselect_everything()
    for mesh in blender_objects:
        mesh.select_set(True)

    bpy.ops.mesh.select_all(action="SELECT")
    bpy.ops.uv.select_all(action="SELECT")

    return_value = bpy.ops.uvpackmaster2.uv_pack()
    if "FINISHED" not in return_value:
        print(f"The return value of uvpackmaster2 is not FINISHED but {return_value}")
        print("Packing uv map for all model geometries using blender operator")
        bpy.ops.uv.pack_islands(rotate=True, margin=0.001)
    else:
        for p in range(passes):
            bpy.ops.uv.select_all(action="SELECT")
            bpy.ops.uvpackmaster2.uv_pack()
    bpy.ops.object.mode_set(mode="OBJECT")


@time_decorator
def DiffuseImageMaterial(name="DiffuseImageMaterial", image=None, link_image=False):
    """create a Diffuse texture node

    Args:
        name (str, optional): [description]. Defaults to "DiffuseImageMaterial".
        image ([type], optional): [description]. Defaults to None.
        link_image (bool, optional): [description]. Defaults to False.

    Returns:
        [type]: [description]
    """
    material = bpy.data.materials.get(name)
    if not material:
        material = bpy.data.materials.new(name)
    material.use_nodes = True
    material.node_tree.links.clear()
    nodes = material.node_tree.nodes
    nodes.clear()
    output = nodes.new(type="ShaderNodeOutputMaterial")
    diffuse = nodes.new(type="ShaderNodeBsdfDiffuse")
    diffuse.inputs["Color"].default_value = [0, 0, 0, 1]
    material.node_tree.links.new(diffuse.outputs["BSDF"], output.inputs["Surface"])
    image_node = nodes.new("ShaderNodeTexImage")
    image_node.image = image
    nodes.active = image_node
    if link_image:
        material.node_tree.links.new(
            image_node.outputs["Color"], diffuse.inputs["Color"]
        )
    return material

@time_decorator
def texture_already_exists(filepath):
    """check if the texture exists.

    Args:
        filepath ([type]): [description]

    Returns:
        [type]: [description]
    """
    return str(filepath) in [image.filepath for image in bpy.data.images]


@time_decorator
def try_creating_material_from_image_list(images):
    """Assigns categories to each image if possible and returns the categorized images as a material.
        If no image is categorized, no material is created and thus the function returns None.

    Args:
        images ([dict]): [description]

    Returns:
        [type]: [description]
    """
    for image in images:
        Texture.categorize(image)
    categorized_images = [image for image in images if image["category"]]
    if not categorized_images:
        return None
    else:
        # TexturedMaterial, TexturedMaterial.001, TexturedMaterial.002,..
        material = bpy.data.materials.new("TexturedMaterial")
        material["images"] = categorized_images
        material.use_nodes = True
        nodes = material.node_tree.nodes
        bsdf_node = nodes["Principled BSDF"]
        category_to_socket = {
            "diffuse": "Base Color",
            "normal": "Normal",
            "metallic": "Metallic",
            "roughness": "Roughness",
        }
        for image in categorized_images:
            if image["category"] not in category_to_socket.keys():
                continue
            socket_name = category_to_socket[image["category"]]
            image_node = nodes.new("ShaderNodeTexImage")
            image_node.image = image
            print("image_node.image", image_node.image)
            if bsdf_node.inputs[socket_name].is_linked:
                log.warning("socket already linked")
            material.node_tree.links.new(
                image_node.outputs["Color"], bsdf_node.inputs[socket_name]
            )
        return material


@time_decorator
def get_indices_of_used_materials(mesh):
    """[summary]

    Args:
        mesh ([type]): [description]

    Returns:
        [type]: [description]
    """
    return list({face.material_index for face in mesh.data.polygons})


@time_decorator
def get_indices_of_unused_materials(mesh):
    """_summary_

    Args:
        mesh (_type_): _description_

    Returns:
        _type_: _description_
    """
    used_materials = set(
        mesh.data.materials[index] for index in get_indices_of_used_materials(mesh)
    )
    return set(range(0, len(mesh.data.materials))) - used_materials


@time_decorator
def apply_preview_material(blender_object):
    """[summary]

    Args:
        model ([type]): [description]
    """
    preview_material = bpy.data.materials["previewMaterial"]
    preview_material.use_nodes = True

    if "images" in blender_object.data.materials[0]:
        for image in blender_object.data.materials[0]["images"]:
            try:
                if image["category"] == "diffuse":
                    print("Add Diffuse Texture")
                    preview_node = preview_material.node_tree.nodes.get(
                        "diffuse texture"
                    )
                    preview_node.image = image
                elif image["category"] == "roughness":
                    print("Add Reflectivity Texture")
                    preview_node = preview_material.node_tree.nodes.get(
                        "reflectivity texture"
                    )
                    preview_node.image = image
                elif image["category"] == "normal":
                    print("Add Normal Texture")
                    preview_node = preview_material.node_tree.nodes.get(
                        "normal texture"
                    )
                    preview_node.image = image
            except Exception as e:
                log.error(e)

    keep = set(blender_object.data.materials) - set(bpy.data.materials)
    blender_object.data.materials.clear()
    for mat in keep:
        blender_object.data.materials.append(mat)

    blender_object.data.materials.append(preview_material)
    blender_object.active_material = preview_material


@time_decorator
def remove_all_uv_layers(element):
    """[summary]

    Args:
        element ([type]): [description]
    """
    for key in element.data.uv_layers.keys():
        layer = element.data.uv_layers.get(key)
        element.data.uv_layers.remove(layer)


@time_decorator
def remove_unused_uv_layers(element):
    """[summary]

    Args:
        element ([type]): [description]
    """
    for key in element.data.uv_layers.keys():
        layer = element.data.uv_layers.get(key)
        if not layer.active_render:
            element.data.uv_layers.remove(layer)


@time_decorator
def normalize_uv_layer(model, export_directory, write_info=False) -> list:
    """[summary]

    Args:
        model ([type]): [description]
        export_directory ([type]): [description]
        write_info (bool, optional): [description]. Defaults to False.

    Returns:
        list: [description]
    """
    """ determines the boundaries of the current uv map and scales it to fit between 0 and 1 """
    # bpy.context.scene.objects.active = o

    o = model.object
    bpy.context.view_layer.objects.active = o
    bm = bmesh.new()
    bm.from_mesh(o.data)
    nm = o.name
    uvl = bm.loops.layers.uv.active
    bmesh.ops.triangulate(
        bm, faces=bm.faces[:], quad_method="BEAUTY", ngon_method="BEAUTY"
    )
    uv = 0

    # compute uv bounding box
    uv_min, uv_max = [[+math.inf, +math.inf], [-math.inf, -math.inf]]
    for face in bm.faces:
        for loop in face.loops:
            uv = loop[uvl].uv
            if uv[0] < uv_min[0]:
                uv_min[0] = uv[0]
            if uv[1] < uv_min[1]:
                uv_min[1] = uv[1]
            if uv[0] > uv_max[0]:
                uv_max[0] = uv[0]
            if uv[1] > uv_max[1]:
                uv_max[1] = uv[1]
    scale = [1.0 / (uv_max[0] - uv_min[0]), 1.0 / (uv_max[1] - uv_min[1])]

    # transform uvs
    # translate
    for face in bm.faces:
        for loop in face.loops:
            uv = loop[uvl].uv
            uv[0] += -uv_min[0]
            uv[1] += -uv_min[1]
    # resize
    for face in bm.faces:
        for loop in face.loops:
            uv = loop[uvl].uv
            uv[0] *= scale[0]
            uv[1] *= scale[1]

    # update mesh and compute uv density
    bm = bmesh.new()
    bm.from_mesh(o.data)
    uvl = bm.loops.layers.uv.active
    bmesh.ops.triangulate(
        bm, faces=bm.faces[:], quad_method="BEAUTY", ngon_method="BEAUTY"
    )
    uv_area = 0
    for face in bm.faces:
        v = []
        for loop in face.loops:
            v.append(loop[uvl].uv)
        # calculate area with trapezoid formula
        uv_area += 0.5 * (
            v[0][0] * (v[1][1] - v[2][1])
            + v[1][0] * (v[2][1] - v[0][1])
            + v[2][0] * (v[0][1] - v[1][1])
        )

    # return calculated data
    surf_area = sum(f.calc_area() for f in bm.faces)
    texture_scale = 0
    uv_density = 0
    if uv_area != 0:
        texture_scale = surf_area * (1 / uv_area)
        uv_density = math.sqrt(abs(texture_scale))

    model.scaleX = scale[0] * uv_density
    model.scaleY = scale[1] * uv_density

    if write_info:
        print("Writing info file about uv-scaling")
        data = f"scaleX {model.scaleX}\nscaleY {model.scaleY}\nuv_area {uv_area}\nsurf_area {surf_area}\ntexture_scale {texture_scale}\nuv_density {uv_density}\n"
        if uv_area < 0.75:
            data += "note: possible bad uv-layout\n"
        (export_directory / f"{model.name}_scale_data.txt").write_text(data)


@time_decorator
def get_channel_values(image: bpy.types.Image, channel: int) -> set:
    """[summary]

    Args:
        image (bpy.types.Image): [description]
        channel (int): [description]

    Raises:
        Exception: [description]

    Returns:
        set: [description]
    """
    channels = int(image.channels)
    pixels = list(image.pixels)
    if channel > channels:
        raise Exception("Less channels than the desired one.")
    return set(pixels[channel::channels])


@time_decorator
def channel_is_used(image: bpy.types.Image, channel: int) -> bool:
    """Check if there are other values apart from 0.0

    Args:
        image (bpy.types.Image): [description]
        channel (int): [description]

    Returns:
        bool: [description]
    """
    return len(get_channel_values(image, channel)) > 1


@time_decorator
def get_descendant_nodes_of_input_socket(socket):
    """travelling through nodes and find the images.

    Args:
        socket (bpy.data.socket): diffuse, normal, metalic, roughness

    Returns:
        list [list]: all the traversed nodes.
    """
    traversed_nodes = set()
    node_stack = set(link.from_node for link in socket.links)
    while node_stack:
        node = node_stack.pop()
        traversed_nodes.add(node)
        links = []
        for input in node.inputs:
            links.extend(input.links)
        connected_nodes = [link.from_node for link in links]
        node_stack |= set(connected_nodes)
    return list(traversed_nodes)


@time_decorator
def connect_image_node_to_socket(image, node, channel):
    """[summary]

    Args:
        image ([type]): [description]
        node ([type]): [description]
        channel ([type]): [description]

    Returns:
        [type]: [description]
    """
    image_node = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeImage")
    image_node.image = image
    bpy.context.scene.node_tree.links.new(image_node.outputs["Image"], node.inputs[channel])
    return image_node


@time_decorator
def create_image_from_separate_channels(
    filepath, red=None, green=None, blue=None, resolution=None
):
    """[summary]

    Args:
        filepath ([type]): [description]
        red ([type], optional): [description]. Defaults to None.
        green ([type], optional): [description]. Defaults to None.
        blue ([type], optional): [description]. Defaults to None.
        resolution ([type], optional): [description]. Defaults to None.

    Returns:
        [type]: [description]
    """
    """ creates a rgb texture from up to three single-colored textures by writing the value of each texture in a separate channel"""
    bpy.context.scene.render.use_compositing = True
    bpy.context.scene.use_nodes = True
    # https://docs.blender.org/api/current/bpy.types.CompositorNode.html
    combine_node = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeCombRGBA")
    composite_node = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeComposite")
    bpy.context.scene.node_tree.links.new(
        combine_node.outputs["Image"], composite_node.inputs["Image"]
    )

    image_nodes = []
    if red:
        image_nodes.append(connect_image_node_to_socket(red, combine_node, "R"))
    if green:
        image_nodes.append(connect_image_node_to_socket(green, combine_node, "G"))
    if blue:
        image_nodes.append(connect_image_node_to_socket(blue, combine_node, "B"))

    resolution_x = resolution_y = 0
    for node in image_nodes:
        resolution_x = max(node.image.size[0], resolution_x)
        resolution_y = max(node.image.size[1], resolution_y)
    bpy.context.scene.render.resolution_x = resolution if resolution else resolution_x
    bpy.context.scene.render.resolution_y = resolution if resolution else resolution_y
    bpy.context.scene.render.filepath = filepath

    bpy.ops.render.render(write_still=True)
    image = bpy.data.images.load(filepath)
    image.pack()
    # TODO ADD THIS TO TASK
    # remove(filepath)
    bpy.context.scene.node_tree.nodes.remove(composite_node)
    bpy.context.scene.node_tree.nodes.remove(combine_node)
    for node in image_nodes:
        bpy.context.scene.node_tree.nodes.remove(node)
    return image


@time_decorator
def create_uv_layers(blender_objects):
    """[summary]

    Args:
        blender_objects ([type]): [description]
    """
    for mesh in blender_objects:
        mesh.data.uv_layers.active = mesh.data.uv_layers.new(name="joined_layer")


# TODO Transform to Task CHECK


@time_decorator
def join_materials(model, resolution=1024, samples=1, tilesize=16):
    """[summary]

    Args:
        model ([type]): [description]
        resolution (int, optional): [description]. Defaults to 1024.
        samples (int, optional): [description]. Defaults to 1.
        tilesize (int, optional): [description]. Defaults to 16.
    """
    """ TODO: support uvpackmaster """

    joined_material = bpy.data.materials.new(model.name + ".0")
    joined_material.use_nodes = True
    image_node = joined_material.node_tree.nodes.new("ShaderNodeTexImage")
    shader_node = joined_material.node_tree.nodes["Principled BSDF"]
    joined_material.node_tree.links.new(
        image_node.outputs["Color"], shader_node.inputs["Base Color"]
    )
    bake_nodes = []
    baked_diffuse = bpy.data.images.new(
        name="baked_diffuse", width=resolution, height=resolution
    )
    # baked_diffuse.file_format = texture.file_format
    baked_diffuse["category"] = "diffuse"
    baked_roughness = bpy.data.images.new(
        name="baked_roughness", width=resolution, height=resolution
    )
    baked_roughness["category"] = "roughness"
    baked_normal = bpy.data.images.new(
        name="baked_normal", width=resolution, height=resolution
    )
    baked_normal["category"] = "normal"
    joined_material["images"] = [baked_diffuse, baked_normal, baked_roughness]
    image_node.image = baked_diffuse
    for material in model.materials:
        node = material.node_tree.nodes.new("ShaderNodeTexImage")
        node.label = "BakeImageNode"
        for node in material.node_tree.nodes:
            node.select = False
        node.select = True
        material.node_tree.nodes.active = node
        bake_nodes.append(node)

    bpy.context.scene.render.bake.margin = 1
    bpy.context.scene.render.tile_x = bpy.context.scene.render.tile_y = tilesize
    bpy.context.scene.render.engine = "CYCLES"
    bpy.context.scene.cycles.samples = samples

    for geometry in model.imported_objects:
        geometry.select_set(False)

    for geometry in model.imported_objects:
        if not geometry.data.materials:
            print(f"Skipping object {geometry} because it has no materials")
            continue
        print(f"Baking material from geometry {geometry}")
        geometry.select_set(True)
        bpy.context.view_layer.objects.active = geometry
        bpy.context.scene.render.bake.use_selected_to_active = False
        for node in bake_nodes:
            node.image = baked_diffuse
        print("Baking diffuse")
        bpy.ops.object.bake(type="DIFFUSE", pass_filter=set({"COLOR"}), use_clear=False)
        for node in bake_nodes:
            node.image = baked_normal
        print("Baking normal")
        bpy.ops.object.bake(type="NORMAL", use_clear=False)
        for node in bake_nodes:
            node.image = baked_roughness
        print("Baking roughness")
        bpy.ops.object.bake(type="ROUGHNESS", use_clear=False)
        geometry.select_set(False)

    for geometry in model.imported_objects:
        geometry.data.materials.clear()
        geometry.data.materials.append(joined_material)
        geometry.data.uv_layers["joined_layer"].active_render = True

    joined_material["images"] = [baked_diffuse, baked_normal, baked_roughness]
    model.object.data.materials.clear()
    model.object.data.materials.append(joined_material)
    model.materials.clear()
    model.materials.add(joined_material)


def check_principled_shader(input_node):
    """Traverse the node tree in a given node.

    Args:
        input_node (material node): a node under the principled shader

    Returns:
        list: a list of images
    """
    images = set()
    socket_names_and_categories = [
        (("Base Color"), ("diffuse")),
        (("Normal"), ("normal")),
        (("Metallic"), ("metallic")),
        (("Roughness"), ("roughness")),
        (("Emission"), ("emission"))
    ]
    image_set = set()
    for socket_name, category in socket_names_and_categories:
        for node in get_descendant_nodes_of_input_socket(
                input_node["Principled BSDF"].inputs[socket_name]
            ):
                if node.type == "TEX_IMAGE" and node.image:
                    image_set.add(node.image)
                for image in image_set:
                    if image in images:
                        if image["category"] == "metallic" and category == "roughness":
                            if check_image_is_reflective(input_node, image):
                                log.debug("SUCCEED")
                                # if separate rgb uses the image as input, than this is a reflectivity map for sure
                                image["category"] = "reflectivity"
                        continue
                    log.info(f"Found image {image} for category {category}")
                    image["category"] = category
                    images.add(image)

    return list(images)

def check_shader_with_material_output(input_node): # what is this?
        return None

def check_image_texture(model, node):
    """
        check texture in a node if it is categorized.
    """
    image = node.image
    if image is None:
        return None
    if not hasattr(image, "category"):
        Texture.categorize(image)
    if image["category"] is not None and image in model.unassigned_textures:
        log.info(f'Applying category {image["category"]} to {image}')
        model.unassigned_textures.remove(image)
    return image

def assign_image_files_to_category(image, category):
    """ log the images to the corresponding category in the model.py
    Args:
        material (bpy.data): the material object
        image (bpy.data): the image object
    Return:
        boolean: if the task is done or not
    """
    found_image = False
    for filepath in setting.file_paths:
        if Path(filepath).name == image.name:
            # remove from unassigned_textures, and log the original name (including path) in the corresponding category
            model.unassigned_textures.remove(image)
            if category == "diffuse":
                category = "baseColor"
            model.append(category, image)
            found_image = True
    return found_image

def find_sub_objects_from_material_name(mesh, check_meterial):
    """find the sub objects and its material in a given mesh.
    If a sub object is found, the sub object will be moved to its suitable place in our model.

    Args:
        mesh (bpy object): A bpy object
        check_meterial (str, optional): check if the sub object belongs to plane, glass, label or shadow. Defaults to "plane".

    Returns:
        boolean: found or not found
    """
    regex_dict = {
        "plane": re.compile("[\.\-\_](m?p)$"),
        "glass": re.compile("[\.\-\_](m?g)$"),
    }
    #regex_dict = re.compile("[\.\-\_](m?(\d+)|ms|m?g|m?p|ml)$")
    found = False
    for material in mesh.data.materials:
        log.info(f"Material name is {material.name}")
        if any(tag in material.name.lower() for tag in getattr(keywords, check_meterial)):
            found = True
        elif regex_dict[check_meterial].search(material.name.lower()):
            found = True
    return found

def check_image_is_reflective(input_node, image) -> bool:
    """checks if the given image is a reflective image that was splitted with a 'separate rgb' material node

    Args:
        input_node (node): a node under the principled shader
        image (image): this is the image that could be related to the 'separete rgb' node

    Returns:
        bool: returns ture in case the image is related to the 'separete rgb', and false if not
    """
    # this gives a set of links that go into the node
    # in case of 'separate rgb' it should be only the link pluged into the image -> so 'image texture'
    if input_node.get("Separate RGB", None):
        node_stack = set(link.from_node for link in input_node["Separate RGB"].inputs['Image'].links)
        while node_stack:
            #this is the image texture
            node = node_stack.pop()
            #check if the 'image texture' is part of the linked node
            if node.type == "TEX_IMAGE" and node.image.name == image.name:
                log.debug(f"is {node.image.name} == {image.name} ?")
                return True

    return False

