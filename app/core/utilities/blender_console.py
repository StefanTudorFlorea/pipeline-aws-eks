import os
from app.core.utilities.ascii import get_logo
import sys

# from asciimatics.screen import Screen


class BlenderConsole(object):
    def __init__(self):
        self.name = ""
        self.shadow_name = ""
        self.complete = False
        self.error = False
        self.error_msg = ""
        self.no_pipeline = False
        self.install_addons = False

        self.current_task = 0
        self.current_task_temp = 0
        self.current_task_name = ""
        self.max_task = 0

        self.filepath = None
        self.export_directory = ""
        self.materials = 0

        self.vertices = 0
        self.triangles = 0
        self.quads = 0
        self.polycount = 0

        self.dimension_x = ""
        self.dimension_y = ""
        self.dimension_z = ""

        self.has_uvs = ""
        self.mask = ""

        self.pinball_length = 40
        self.pinball_index = 0
        self.pinball_up = True
        self.pinball_str = ""
        self.pinball_char = "###"

    def get_meta_data(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        meta_data = "========== AUTOBAKE PIPELINE ==========\n"
        meta_data += "Name:\t\t{}\n".format(self.name)
        meta_data += "File Path:\t{}\n".format(self.filepath)
        meta_data += "Export Dir:\t{}\n".format(self.export_directory)
        meta_data += "Materials:\t{}\n".format(self.materials)
        meta_data += "Current Task:\t{}\n".format(self.current_task_name)
        meta_data += "Task Queue:\t{} of {}\n".format(self.current_task, self.max_task)
        meta_data += "\n"
        meta_data += "========== GEOMETRY ==========\n"
        meta_data += "Vertices:\t{}\n".format(self.vertices)
        meta_data += "Polygons:\t{}\n".format(self.polycount)
        meta_data += "Triangles:\t{}\n".format(self.triangles)
        meta_data += "\n"
        meta_data += "========== DIMENSIONS ==========\n"
        meta_data += "Dimension x:\t{}\n".format(self.dimension_x)
        meta_data += "Dimension y:\t{}\n".format(self.dimension_y)
        meta_data += "Dimension z:\t{}\n".format(self.dimension_z)
        return meta_data

    def encode_output(self, response):
        """_summary_

        Args:
            response (_type_): _description_

        Returns:
            _type_: _description_
        """
        response = response.split("@")
        dict_response = {}
        for index, item in enumerate(response):
            if item.startswith("--"):
                if index != len(response) - 1 and not response[index + 1].startswith(
                    "--"
                ):
                    dict_response[item[2:]] = response[index + 1]
        return dict_response

    def print_response(self, response):
        """_summary_

        Args:
            response (_type_): _description_
        """
        dict_response = self.encode_output(response)
        self.update_interface(dict_response)
        self.print()

    def clear_console(self):
        """_summary_"""
        if os.name == "nt":
            os.system("cls")

    def communicate(self, process):
        """_summary_

        Args:
            process (_type_): _description_
        """
        self.clear_console()
        self.print()
        for line in iter(process.stdout.readline, b""):
            if len(line) > 0 and line[0] == "@":
                # Updating Interface
                self.clear_console()
                self.print_response(line)

                if "END_PIPELINE" in line:
                    break

            if len(line) > 0 and line == "$":
                print(line[1::])
        self.print()

    def print(self):
        """_summary_"""
        try:
            if self.install_addons:
                print(get_logo(), end="\r", flush=True)  # , end='')
                print("========== BLENDER ADDONS INSTALLED ==========")
                if self.complete and not self.error:
                    print("========== AUTOBAKE COMPLETE ==========")
            else:
                print(get_logo())

                if self.filepath is not None:
                    print(self.get_meta_data())
                    if self.complete and not self.error:
                        print("========== AUTOBAKE COMPLETE ==========")

                    if self.complete and self.error:
                        print("========== ERRORS ==========")
                        print(self.error_msg)
                        if self.no_pipeline:
                            print("PIPELINE GOT NOTHING TO IMPORT")
                        print("========== AUTOBAKE COMPLETE WITH ERRORS ==========")
                        print("Please reach out to one of the following devs:\n")
                        print("Manuel Hartmann")
                        print("\trocket.chat " + "\U0001F680" + "\tmanuel.hartmann")
                        print("\tmail " + "\U0001F4E7" + "\t\tm.hartmann@rooom.com\n")
                        print("Marcel Himmelreich")
                        print("\trocket.chat " + "\U0001F680" + "\tm.himmelreich")
                        print(
                            "\tmail " + "\U0001F4E7" + "\t\tm.himmelreich@rooom.com\n"
                        )
        except Exception as e:
            print(e)

    def print_pinball(self):
        """_summary_"""
        if self.pinball_up:
            self.pinball_index += 1
        else:
            self.pinball_index -= 1

        if self.pinball_index < 0:
            self.pinball_index = 0
            self.pinball_up = True
        elif self.pinball_index > self.pinball_length:
            self.pinball_index = self.pinball_length
            self.pinball_up = False

        self.pinball_str = ""
        for i in range(self.pinball_length):
            if i == self.pinball_index:
                self.pinball_str += self.pinball_char
            else:
                self.pinball_str += " "

        print("||{}||".format(self.pinball_str))

    def update_interface(self, dictionary):
        """_summary_

        Args:
            dictionary (_type_): _description_
        """
        for key, value in dictionary.items():
            key = key.lower()
            if key == "name":
                self.name = str(value).replace("\n", "")
            if key == "current_task_name":
                self.current_task_name = str(value).replace("\n", "")
            if key == "current_task":
                self.current_task = int(value)
            if key == "max_task":
                self.max_task = int(value)
            if key == "export_directory":
                self.export_directory = str(value).replace("\n", "")
            if key == "vertices":
                self.vertices = int(value)
            if key == "triangles":
                self.triangles = int(value)
            if key == "quads":
                self.quads = int(value)
            if key == "polycount":
                self.polycount = int(value)
            if key == "dimensions_x":
                self.dimension_x = float(value)
            if key == "dimensions_y":
                self.dimension_y = float(value)
            if key == "dimensions_z":
                self.dimension_z = float(value)
            if key == "has_uvs":
                self.has_uvs = str(value)
            if key == "mask":
                self.mask = str(value)
            if key == "materials":
                self.materials = int(value)
            if key == "complete":
                self.complete = bool(value)
            if key == "error":
                self.error = bool(value)
            if key == "error_msg":
                self.error_msg += str(value)
            if key == "file_path":
                self.filepath = str(value).replace("\n", "")
            if key == "no_pipeline":
                self.no_pipeline = True
                self.error = True
            if key == "install_addons":
                self.install_addons = True
