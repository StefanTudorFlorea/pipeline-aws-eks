import math
import glob
import operator

# checking
import os
from os.path import islink, join, isdir
from os import remove, path, listdir

import bpy
from bpy import context as C
from bpy import data as D
from bpy import ops as O
from bpy import types as T
from app.core.abstract.decorators import time_decorator
from app.core.utilities.animated_gif import save_transparent_gif
from pathlib import Path
import json

from app.core.model import Model

model = Model()

from app.core.log import Log

log = Log()


@time_decorator
def activate_cpu(tilesize=16):
    """Activates CPU Calculation for device
    Args:
        tilesize (int, optional): [description]. Defaults to 16.

    Returns:
        Object: CPU Device
    TODO: adjust render settings
    """

    preferences = C.preferences.addons["cycles"].preferences
    cuda_devices, opencl_devices = preferences.get_devices()

    if len(preferences.devices) == 0:
        return None

    cpu_device = None

    for device in preferences.devices:
        device.use = False  # reset
        if device.type == "CPU" and device in opencl_devices:
            cpu_device = device

    if not cpu_device:
        return None

    C.scene.cycles.device = "CPU"
    C.preferences.addons["cycles"].preferences.compute_device_type = "OPENCL"
    cpu_device.use = True

    for scene in D.scenes:
        scene.render.engine = "CYCLES"
        scene.cycles.device = "CPU"

    C.scene.render.tile_x = tilesize
    C.scene.render.tile_y = tilesize
    return cpu_device


@time_decorator
def activate_gpu(tilesize=256):
    """Activates GPU Calculation for Device

    Args:
        tilesize (int, optional): [description]. Defaults to 256.

    Returns:
        Object: GPU Device

    https://blender.stackexchange.com/questions/182983/compute-device-selection-for-a-shared-blender-version
    TODO: adjust render settings
    """
    preferences = C.preferences.addons["cycles"].preferences
    cuda_devices, opencl_devices = preferences.get_devices()

    if len(preferences.devices) == 0:
        return None

    gpu_device = None

    for device in preferences.devices:
        device.use = False  # reset
        if device.type == "CUDA" and device in cuda_devices:
            gpu_device = device

    if not gpu_device:
        return None

    C.scene.cycles.device = "GPU"
    C.preferences.addons["cycles"].preferences.compute_device_type = gpu_device.type
    gpu_device.use = True

    for scene in D.scenes:
        scene.render.engine = "CYCLES"
        scene.cycles.device = "GPU"

    C.scene.render.tile_x = tilesize
    C.scene.render.tile_y = tilesize
    return gpu_device


@time_decorator
def get_selection_copy():
    return bpy.context.selected_objects.copy()



@time_decorator
def set_scene(export_folder, image_size):
    """Set Settings for Blender Scene

    Args:
        export_folder (string): Export Filepath
        image_size (int): Resolution for Image
    """
    bpy.context.scene.render.filepath = str(export_folder) + "/preview.png"
    bpy.context.scene.render.image_settings.file_format = "PNG"
    bpy.context.scene.render.image_settings.color_mode = "RGBA"
    bpy.context.scene.render.film_transparent = True
    bpy.context.scene.render.resolution_x = image_size
    bpy.context.scene.render.resolution_y = image_size
    bpy.data.scenes["Scene"].render.resolution_x = image_size
    bpy.data.scenes["Scene"].render.resolution_y = image_size
    bpy.data.scenes["Resources"].render.resolution_x = image_size
    bpy.data.scenes["Resources"].render.resolution_y = image_size
    bpy.context.scene.use_nodes = True
    # bpy.context.scene.view_settings.exposure = 1.0


@time_decorator
def export_gif(file_path, mask="turntable*.png"):
    """Exports Gif from images of given Directory

    Args:
        file_path (string): Directory
    """
    # filepaths
    fp_in = str(Path(file_path) / mask)
    fp_out = str(Path(file_path) / "preview.gif")

    from PIL import Image

    # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
    log.info("Search Files for gif creation", fp_in)
    fnames = sorted(glob.glob(fp_in))
    img, *imgs = [Image.open(f) for f in fnames]
    save_transparent_gif(imgs, 200, fp_out)
    for file in fnames[1:]:
        log.debug(f"Removing {file}")
        os.unlink(file)
    log.info("Save Preview Gif: " + fp_out)


@time_decorator
def set_rotation_keyframes(blender_object, steps):
    """Set Rotation Keyframes for Turntable & Preflight

    Args:
        blender_object (string): Base blender_object Class
        steps (int): Image Count devided by 360 Degree
    """
    # Render Images with 360 Degree
    for rad in range(steps):
        print(list(bpy.data.objects))
        blender_object.rotation_euler[2] = math.radians(360 / steps * rad)
        blender_object.keyframe_insert(data_path="rotation_euler", frame=rad)


@time_decorator
def imposter(
    blender_objects, image_size, imposter_size, imposter_samples, export_directory
):
    """Imposter Rendering

    Args:
        model (Model): Base Model Class
        image_size (int): Image Size Resolution
        imposter_size (int): Image Count per row / column
        imposter_samples (int): Rendering Samples
        export_directory (string): Export Directory
    """
    for entry in blender_objects:
        entry.select_set(True)

    steps = imposter_size * imposter_size

    # try:
    # 	bpy.data.scenes['Scene'].collection.objects.link(bpy.data.objects['midpoint'])
    # except:
    # 	print("Cant link object to Scene")

    # filename, filetype = str(export_directory).split('.')

    file_path = Path(export_directory) / "imposter"

    # Configure Composites
    scene = bpy.data.scenes["Scene"]
    scene.render.use_compositing = True
    # bpy.data.scenes['Scene'].node_tree.nodes['normalization factor'].value = 300
    bpy.data.scenes["Scene"].node_tree.nodes["File Output"].base_path = file_path
    # prepare a scene
    scn = bpy.context.scene
    scn.frame_start = 0
    scn.frame_end = steps
    bpy.context.scene.frame_set(0)

    bpy.context.scene.cycles.samples = imposter_samples

    for rad in range(steps):
        bpy.ops.render.render(layer="View Layer", write_still=True)
        bpy.context.scene.frame_current += 1

    # bpy.data.scenes['Scene'].collection.objects.unlink(bpy.data.objects['midpoint'])
    # deselect_everything()

    textures = [
        "alpha",
        "base_color",
        "motion_backward",
        "motion_forward",
        "base_color_with_alpha",
    ]
    for texture in textures:
        # filepaths
        from PIL import Image
        import PIL

        if (
            texture == "base_color_with_alpha"
            or texture == "motion_backward"
            or texture == "motion_forward"
        ):

            fp_in = file_path + "\\" + texture + "*.png"

            fp_out = export_directory + "\\" + texture + ".png"

            imgs = [Image.open(f) for f in sorted(glob.glob(fp_in))]
            size = imposter_size

            result = Image.new("RGBA", (size * image_size, size * image_size))

        else:

            fp_in = file_path + "\\" + texture + "*.jpg"

            fp_out = export_directory + "\\" + texture + ".jpg"

            imgs = [Image.open(f) for f in sorted(glob.glob(fp_in))]
            size = imposter_size

            result = Image.new("RGB", (size * image_size, size * image_size))

        for index, img in enumerate(imgs):
            img.thumbnail((image_size, image_size), Image.ANTIALIAS)
            y = index // size * image_size
            x = index % size * image_size
            w, h = img.size

            result.paste(img, (x, y, x + w, y + h))
            # print('{}_{}'.format(x, y))

        result.save(fp_out, quality=100, subsampling=0)

    for file in sorted(glob.glob(export_directory + "imposter\*")):
        # os.unlink(file)
        Path(file).unlink()
    try:
        # os.rmdir(export_directory + 'imposter')
        Path(export_directory).joinpath("imposter").rmdir()
    except Exception as e:
        print(e)


@time_decorator
def turntable(blender_object, export_directory, model_name, file_name, gif=False):
    """Turntable Rendering for Preflight

    Args:
        blender_objects (blender_objects): Base blender_objects Class
        export_directory (string): Export Directory
        gif (bool, optional): [description]. Defaults to False.
    """

    blender_object.select_set(True)

    steps = 1
    filename = str(Path(export_directory) / file_name)
    if gif:
        steps = 16

    try:
        bpy.data.scenes["Scene"].collection.objects.link(bpy.data.objects["midpoint"])
    except:
        print("Cant link object to Scene")

    try:
        C.collection.objects.unlink(D.objects["Plane"])
    except:
        log.warning("Can't Unlink Plane")

    try:
        C.collection.objects.unlink(D.objects[model_name + "_shadow"])
    except:
        log.warning("Can't Unlink Plane")

    # Render Images with 360 Degree
    # bpy.context.scene.render.engine = 'BLENDER_EEVEE'
    for rad in range(steps + 1):
        # print(list(bpy.data.objects))
        bpy.data.objects["midpoint"].rotation_euler[2] = math.radians(360 / steps * rad)
        if math.radians(360 / steps * rad) == 0:
            file_path = str(export_directory) + f"/{file_name}.png"
        else:
            file_path = (
                str(export_directory)
                + f"/{file_name}{str(math.radians(360/steps * rad))}.png"
            )
        bpy.context.scene.render.filepath = file_path
        bpy.ops.render.render(write_still=True)
        log.info("Render Image to", file_path)

    bpy.data.scenes["Scene"].collection.objects.unlink(bpy.data.objects["midpoint"])


@time_decorator
def material_to_dict(material):
    """[summary]

    Args:
        material ([type]): [description]

    Returns:
        [type]: [description]
    """
    if not material:
        return {}
    if "images" not in material.keys():
        return {"name": material.name}
    return {
        "name": material.name,
        "images": [
            f"{image.name} ({image['category']})" if image else {}
            for image in material["images"]
        ],
        "textures": [
            node.image.name
            for node in material.node_tree.nodes
            if node.type == "TEX_IMAGE"
        ],
    }


@time_decorator
def unlink_all_objects_from_current_scene():
    """_summary_"""
    for element in D.objects:
        if element.name not in C.collection.objects:
            continue
        C.collection.objects.unlink(element)


@time_decorator
def link_model_to_current_scene(model):
    """_summary_

    Args:
        model (_type_): _description_
    """
    link_objects_to_scene(model)


@time_decorator
def link_all_objects_to_scene(objects, scene_name="Scene"):
    """

    Args:
        objects ([type]): [description]

    Raises:
        Exception: [description]

    Returns:
        [type]: [description]
    """
    C.window.scene = D.scenes[scene_name]
    for obj in objects:
        C.collection.objects.link(obj)


@time_decorator
def recursive_items(dictionary):
    """_summary_

    Args:
        dictionary (_type_): _description_

    Yields:
        _type_: _description_
    """
    for key, value in dictionary.items():
        if type(value) is dict:
            yield (key, value)
            yield from recursive_items(value)
        else:
            yield (key, value)


@time_decorator
def link_objects_to_scene(model, scene_name="Scene"):
    """
    Args:
        scene ([type]): [description]

    Returns:
        [type]: [description]
    """
    C.window.scene = D.scenes[scene_name]

    if not model.object.name in C.collection.objects:
        C.collection.objects.link(model.object)

    if model.shadow_plane and not model.shadow_plane.name in C.collection.objects:
        C.collection.objects.link(model.shadow_plane)

    for element in [*model.glass, *model.planes, *model.labels]:
        if element.name in C.collection.objects:
            continue
        C.collection.objects.link(element)


@time_decorator
def unlink_objects_to_scene(model, scene):
    """[summary]

    Args:
        model ([type]): [description]
        scene ([type]): [description]
    """
    C.window.scene = D.scenes[scene]
    if model.object.name in C.collection.objects:
        D.collection.objects.unlink(model.object)

    if model.shadow_plane.name in C.collection.objects:
        C.collection.objects.unlink(model.shadow_plane)


@time_decorator
def mesh_to_dict(model):
    """Returns a dict by given model and materials

    Args:
        model (Model): Base Model Class
        materials (list): List of Materials

    Returns:
        [type]: [description]
    """
    return {
        "name": model.object.name,
        # material_to_dict(material) for material in model.object.data.materials
        "materials": [
            material_to_dict(material) for material in model.object.data.materials
        ],
        "vertices": len(model.object.data.vertices.values()),
        "triangles": len(
            [p for p in model.object.data.polygons if len(p.vertices) == 3]
        ),
        "quads": len([p for p in model.object.data.polygons if len(p.vertices) == 4]),
        "polycount": len(model.object.data.polygons),
        # "normals": [{
        #    "x":normal.x,
        #    "y":normal.y,
        #    "z":normal.z} for normal in [polygon.normal @ model.object.matrix_world for polygon in model.object.data.polygons]],
        "dimensions": {
            "x": model.object.dimensions.x,
            "y": model.object.dimensions.y,
            "z": model.object.dimensions.z,
        },
        "diagonal": model.object.dimensions.length,
        "has_uvs": bool(model.object.data.uv_layers),
        "uv_layers": [{"name": layer.name} for layer in model.object.data.uv_layers],
        "aspect_ratio": aspect_ratio(),
        # "planes": [mesh_to_dict(plane) for plane in model.planes],
        # "glass": [mesh_to_dict(glass) for glass in model.glass],
        # "labels": [mesh_to_dict(label) for label in model.labels],
        "mask": str(model.mask),
    }


@time_decorator
def aspect_ratio():
    """_summary_

    Returns:
        _type_: _description_
    """
    planes = []
    for plane in model.planes:
        log.info(plane.data)
        # log.info(plane.data.vertices)
        C.view_layer.objects.active = plane
        aspect_ratio = float(calculate_aspect_ratio(plane.data))
        log.info(aspect_ratio)
        ratio = {}
        ratio["name"] = plane.name
        ratio["aspect"] = aspect_ratio
        planes.append(ratio)
    return planes


@time_decorator
def calculate_edge_length(mesh, edge):
    """_summary_

    Args:
        mesh (_type_): _description_
        edge (_type_): _description_

    Returns:
        _type_: _description_
    """
    v0 = mesh.vertices[edge.vertices[0]]
    v1 = mesh.vertices[edge.vertices[1]]
    x2 = (v0.co[0] - v1.co[0]) ** 2
    y2 = (v0.co[1] - v1.co[1]) ** 2
    z2 = (v0.co[2] - v1.co[2]) ** 2
    length = (x2 + y2 + z2) ** 0.5
    return length


@time_decorator
def calculate_aspect_ratio(element):
    """_summary_

    Args:
        element (_type_): _description_

    Returns:
        _type_: _description_
    """
    x = 1
    y = 1
    edges = {}
    edges_pos = {}
    for edge in element.edges:
        length = calculate_edge_length(element, edge)
        edges[edge.index] = length
        v0 = element.vertices[edge.vertices[0]]
        v1 = element.vertices[edge.vertices[1]]
        edges_pos[edge.index] = v0.co[2] + v1.co[2]
    max_index = max(edges_pos.items(), key=operator.itemgetter(1))[0]
    x = edges[max_index]
    edges.pop(max_index)
    min_index = min(edges_pos.items(), key=operator.itemgetter(1))[0]
    edges.pop(min_index)
    if len(edges.items()) > 0:
        y = list(edges.values())[0]
    if y <= 0:
        y = 1
    return x / y


@time_decorator
def calculate_ggt(x, y):
    """_summary_

    Args:
        x (_type_): _description_
        y (_type_): _description_

    Returns:
        _type_: _description_
    """
    z = x % y
    if z == 0:
        return y
    return calculate_ggt(y, z)


@time_decorator
def load_babylon(path):
    if os.path.exists(path):
        file = open(
            path,
        )
        return json.load(file)
    else:
        return None


@time_decorator
def save_babylon(bjs_file, path):
    """_summary_

    Args:
        bjs_file (_type_): _description_
        path (_type_): _description_

    Raises:
        Exception: _description_
    """
    if os.path.exists(path):
        with open(path, "w", encoding="utf-8") as f:
            json.dump(bjs_file, f, ensure_ascii=False, indent=4)
    else:
        raise Exception


@time_decorator
def overwrite_mtl(export_directory):
    """_summary_

    Args:
        export_directory (_type_): _description_
    """
    if os.path.isdir(export_directory):
        for file in os.listdir(export_directory):
            if file.endswith(".mtl"):
                new_file = ""
                temp_file = open(Path(export_directory) / file, "r+")
                log.info("Overwriting MTL ", Path(export_directory) / file)
                lines = temp_file.readlines()
                for line in lines:
                    if "newmtl" in line:
                        new_file += line.split()[0] + (" M" + line.split(" ")[1])
                    else:
                        new_file += line
                temp_file.close()
                temp_file = open(Path(export_directory) / file, "r+")
                temp_file.writelines(new_file)
                temp_file.close()


def find_file(dir, filename):
    """This is to confirm that a file exists in a certain directory

    Args:
        dir (str): typically the export directory path
        filename (str): search the file with the filename

    Returns:
        boolean: true is found, false is not found
    """
    dir_path = Path(dir)
    search_result = [file for file in dir_path.iterdir() if file.stem == filename]
    if len(search_result) !=0:
        return True
    return False

@time_decorator
def deselect_everything():
    """
    removes all references to objects that may be implicitly manipulated by an operator call
    """
    for obj in D.objects:
        obj.select_set(False)
    C.view_layer.objects.active = None
