# keywords
supported_geometries = [
    ".gltf",
    ".glb",
    ".fbx",
    ".obj",
]
supported_image_library = [".mtl"]
supported_textures = [".png", ".jpg", ".tif", ".exr", ".jpeg"]
# prevent know filetypes from geometry/texture parsing
# remove ".zip" for now
ignore_filetypes = [".log", ".DS_STORE", ".gitignore", ".bat"]
directory_blacklist = [
    "autobake",
    "AUTOBAKE",
    "pycache",
    ".git",
    "docs",
    "__pycache__",
    "!!!exclude!!!",
    "tmp",
]
directory_whitelist = []  # ["final","Final"]
settings = ["settings", "set", "stg", "_settings", "auto_settings"]
highpoly = ["high_poly", "highpoly", "highPoly", "high", "hp"]
lowpoly = ["low_poly", "lowpoly", "lowPoly", "low", "lp"]
# check geometry
plane = ["plane", "palne"]
label = ["label"]
glass = ["glas"]
shadow = ["shadow"]
geometry_categories = [*highpoly, *plane, *label, *glass, *shadow, *lowpoly]
# textures
mask = ["mask", "mk"]
normal = ["nor"]  # check filename contains nor ("normal" will be detected.)
# check filename contains dif (diffusion will be detected)
diffuse = ["dif", "basecolor", "albedo", "primary"]
reflectivity = ["ref", "metallicroughness", "mr"]  # "ref" check refl*
roughness = ["roughness"]  # check filename contains roughness
metallic = ["metal"]  # check filename contains metal*
emission = ["emission"]
displacement = ["displacement"]
# unknown textures
# primary, secondary, teritory, quaternary
mash_texture = ["pri", "sec", "ter", "quat"]
