from pathlib import Path
from json import dumps
import bpy
import app.core.utilities.keywords as keywords

class Texture(object):
    """Given images better definition for later exporting.

    Args:
        object (img): images

    Returns:
        class : better define how the images should be exported.
    """
    class Categories:
        diffuse = 0
        normal = 1
        reflectivity = 2
        roughness = 3
        metallic = 4
        emission = 5
        displacement = 6
        mask = 7

    class DefaultExportResolutions:
        diffuse = 4096
        normal = 4096
        reflectivity = 4096
        roughness = 4096
        metallic = 4096
        emission = 4096
        displacement = 4096
        mask = 4096

    class DefaultExportFormats:
        diffuse = ".jpg"
        normal = ".png"
        reflectivity = ".jpg"
        roughness = ".jpg"
        metallic = ".jpg"
        emission = ".jpg"
        displacement = ".jpg"
        mask = ".jpg"

    def __init__(self, image, filepath, category=None, bit_depth=None, resolution=None):
        self.filepath = filepath if isinstance(filepath, Path) else Path(filepath)
        self.image = image
        self.bit_depth = bit_depth if bit_depth else self.image.depth
        self.x_resolution = resolution if resolution else self.image.size[0]
        self.y_resolution = resolution if resolution else self.image.size[1]
        self.category = category

    def __str__(self):
        return dumps(
            {
                "filepath": str(self.filepath),
                "image": str(self.image),
                "bit_depth": self.bit_depth,
                "x_resolution": self.x_resolution,
                "y_resolution": self.y_resolution,
                "category": self.category,
            },
            indent=4,
        )

    def export(self):
        if self.filepath.suffix == ".jpg":
            bpy.context.scene.render.image_settings.file_format = "JPEG"
            bpy.context.scene.render.image_settings.color_mode = "RGB"
        elif self.filepath.suffix == ".png":
            bpy.context.scene.render.image_settings.file_format = "PNG"
            if self.image.channels == 4:
                bpy.context.scene.render.image_settings.color_mode = "RGBA"
            else:
                bpy.context.scene.render.image_settings.color_mode = "RGB"
        else:
            bpy.context.scene.render.image_settings.file_format = self.filepath.suffix

        # C.scene.render.image_settings.color_depth = str(self.bit_depth)
        factor = max(self.x_resolution, self.y_resolution) / max(self.image.size)
        self.image.scale(self.image.size[0] * factor, self.image.size[1] * factor)
        self.image.save_render(str(self.filepath))
        # self.image.reload() #reload in full size

    @classmethod
    def categorize(self, image):
        if any(tag in image.name.lower() for tag in keywords.diffuse):
            image["category"] = "diffuse"
        elif any(tag in image.name for tag in keywords.normal):
            image["category"] = "normal"
            image.colorspace_settings.name = "Non-Color"
        elif any(tag in image.name for tag in keywords.reflectivity):
            image["category"] = "reflectivity"
        elif any(tag in image.name for tag in keywords.roughness):
            image["category"] = "roughness"
        elif any(tag in image.name for tag in keywords.metallic):
            image["category"] = "metallic"
        elif any(tag in image.name for tag in keywords.emission):
            image["category"] = "emission"
        elif any(tag in image.name for tag in keywords.displacement):
            image["category"] = "displacement"
        else:
            image["category"] = None