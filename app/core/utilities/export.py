import bpy
from bpy import context as C
from bpy import data as D
from bpy import ops as O
from bpy import types as T

import json
from pathlib import Path
from shutil import rmtree
import os
from abstract.decorators import time_decorator

from app.core.setting import Setting
setting = Setting()

from app.core.model import Model
model = Model()

from app.core.log import Log
log = Log()

import app.core.utilities.keywords as keywords

@time_decorator
def export_geometry_as_obj(blender_object, name, export_directory):
    """Exports geometry by Category as Object to directory. If there is an object in the export, replace with the new one.

    Args:
        model (Model): Base Model
        name (string): name of the obj file.
        export_directory (string): the export location.
    """

    blender_object.select_set(True)
    try:
        export_file = Path(f"{export_directory}/{name}.obj")
        if export_file.is_file:
            export_file.unlink()

        # if os.path.isfile(str(export_directory / f"{name}.obj")):
        #     os.unlink(str(export_directory / f"{name}.obj"))
    except:
        pass
    bpy.ops.export_scene.obj(
        filepath=str(export_directory / f"{name}.obj"), use_selection=True
    )

    blender_object.select_set(False)


@time_decorator
def export_shadow_plane(model, export_directory):
    """[summary]

    Args:
        model ([type]): [description]
        export_directory ([type]): [description]
    """
    if model.shadow_plane:
        model.shadow_plane.select_set(True)

        if os.path.isfile(str(export_directory / f"{model.name}_shadow.obj")):
            os.unlink(str(export_directory / f"{model.name}_shadow.obj"))

        O.export_scene.obj(
            filepath=str(export_directory / f"{model.name}_shadow.obj"),
            use_selection=True,
        )
        model.shadow_plane.select_set(False)


@time_decorator
def export_text_file(text_file, export_directory, name):
    """_summary_

    Args:
        text_file (_type_): _description_
        export_directory (_type_): _description_
        name (_type_): _description_

    Raises:
        Exception: _description_
    """
    if os.path.isdir(export_directory):
        file = open(Path(export_directory) / f"{name}.txt", "w")
        file.write(text_file)
        file.close()
    else:
        raise Exception


@time_decorator
def export_meshes_as_obj(meshes, directory, name):
    """Convert mesh array to blender object and export it to given directory

    Args:
        array (array): [description]
        directory (string): [description]
        name (string): [description]
    """
    for index, entry in enumerate(meshes):
        entry.select_set(True)

        if os.path.isfile(str(directory / f"{name}{index}.obj")):
            os.unlink(str(directory / f"{name}{index}.obj"))

        O.export_scene.obj(
            filepath=str(directory / f"{name}{index}.obj"), use_selection=True
        )
        entry.select_set(False)


@time_decorator
def save_dict_as_json(dictionary, filepath):
    """Export dictionary to JSON file.

    Args:
        dictionary (dict): a python dictionary object
        filepath (str): a filepath for output
    """
    if Path(filepath).is_file():
        Path(filepath).unlink(missing_ok=True)
    elif Path(filepath).is_dir():
        filepath = Path(filepath) / "unit_test.json"
    with open(filepath, "w") as outfile:
        json.dump(dictionary, outfile)


@time_decorator
def export_image_to_filepath(image, export_name:str, filepath, category=""):
    """Save an image to a given file path

    Args:
        image (object): a bpy image object
        export_name (str): the prefix name added to the exported file. usually model.object.name
        filepath (str): an export directory
        category (str): the category that will be added as postfix to the exported image.
    """
    filepath = Path(filepath).absolute()
    fileformat = filepath.suffix
    if fileformat.lower() in [".jpg", ".jpeg"]:
        C.scene.render.image_settings.file_format = "JPEG"
    elif fileformat.lower()==".png":
        C.scene.render.image_settings.file_format = "PNG"
    else:
        raise Exception(f"Fileformat not yet supported: {fileformat}")
    if category != "":
        log.info(f"image packed file {image.packed_file}")
        if not image.packed_file:
            log.info(f"image has no packed file! {image}")
            bpy.data.images.load(image.filepath)
            image.pack()
        image_file_path = bpy.data.images[image.name].filepath
        new_image_name = f"{export_name}_{category}"
        bpy.data.images[image.name].name = new_image_name
        if Path(os.path.abspath(filepath)) != Path(os.path.abspath(image_file_path)):
            bpy.data.images[new_image_name].filepath = filepath.as_posix()
    else:
        bpy.data.images[image.name].name = f"{export_name}"
    log.info(f"bpy.data.images['new_image_name'] looks like {bpy.data.images[new_image_name]}")
    log.info(f"image object is {image}")
    image.save_render(str(filepath))

@time_decorator
def export_tmp_tex(img, suffix, ext="png"):
    """export temporary texture

    Args:
        img (img object): the image object
        suffix (str): the suffix of the filename. For example, XXX_shadow
        ext (str, optional): _description_. Defaults to 'png'.

    Returns:
        _type_: _description_
    """
    filepath = setting.export_directory/ f"{model.name}{suffix}.{ext}"
    export_image_to_filepath(img, filepath)
    return filepath

@time_decorator
def RGBMaterial(name="RGBMaterial", color=[0, 0, 0]):
    """Create a material with a name using R, G, B three channels.

    Args:
        name (str, optional): [description]. Defaults to "RGBMaterial".
        color (list, optional): [description]. Defaults to [0, 0, 0].

    Returns:
        material [bpy.data.material]: a new material
    """
    material = D.materials.new(name)
    material.use_nodes = True
    nodes = material.node_tree.nodes
    links = material.node_tree.links
    nodes.clear()
    links.clear()
    output = nodes.new(type="ShaderNodeOutputMaterial")
    diffuse = nodes.new(type="ShaderNodeBsdfDiffuse")
    diffuse.inputs["Color"].default_value = [*color, 1]
    material.node_tree.links.new(diffuse.outputs["BSDF"], output.inputs["Surface"])
    return material

def convert_obj_material(obj_name, material_name="M.0", export_color=[0.8, 0.8, 0.8]):
    """Scan the scene to find the object name. Find the "M.0" material in the entire scene.
    If not, M.0. Create one material from the RGBMaterial function.
    Args:
        obj_name (str): the object to convert
        material_name (str, optional): _description_. Defaults to "M.0".
        export_color (list, optional): _description_. Defaults to [0.8, 0.8, 0.8].

    Returns:
        material: bpy.data.material
    """
    for obj in bpy.data.scenes["Scene"].objects:
        if obj.name in obj_name:
            # replace the object material with the M.0, if not,
            export_material = bpy.data.materials.get(material_name)
            if export_material:
                export_material = bpy.data.materials[material_name]
                export_material.node_tree.nodes["Diffuse BSDF"].inputs["Color"].default_value = [*export_color, 1]
                log.info(f"The scene has a material called {export_material}.")
            else:
                log.info(f"create a material from the function RGBMaterial")
                export_material = RGBMaterial(name=material_name, color=export_color)
    return export_material

