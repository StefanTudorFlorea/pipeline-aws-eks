
from multiprocessing import context
import bpy
from bpy import data as D

from app.core.log import Log
from model import Model

log = Log()

model = Model()


def print_blender_data_attribute(name="objects"):
    """_summary_

    Args:
        name (str, optional): _description_. Defaults to "objects".
    """
    if not hasattr(D, name):
        log.waring(f'bpy.data has not attr "name"')
        return
    log.debug(f"{name}:")
    for i, element in enumerate(getattr(D, name)):
        log.debug(f"\t{i}. {element.name} ({type(element)})")


def print_scene_objects(scene="Scene"):
    """_summary_

    Args:
        scene (str, optional): _description_. Defaults to "Scene".
    """
    for i, element in enumerate(D.scenes[scene].objects):
        log.debug(f"\t{i}. {element.name}")


def print_scene_data(mode="OBJECTS"):
    """print the scene data according to the mode.

    Args:
        mode (str, optional): _description_. Defaults to "OBJECTS".
    """
    m = str(mode).upper()
    if m == "0" or m == "NONE" or m == "OBJECTS" or m == "OBJECT" or m == "OBJ":
        log.debug("Blender main scene objects")
        print_scene_objects()

    if m == "1" or m == "IMAGES" or m == "IMAGE" or m == "IMG":
        log.debug("Blender images")
        print_blender_data_attribute("images")

    if m == "2" or m == "SCENES" or m == "SCENE" or m == "SCN":
        log.debug("Blender scenes")
        print_blender_data_attribute("scenes")

    if m == "3" or m == "RESOURCES" or m == "RESOURCE" or m == "RES":
        log.debug("Blender resource scene objects")
        print_scene_objects("Resources")

    if m == "4" or m == "ALL" or m == "EVERYTHING":
        log.debug("Blender data")
        print_blender_data_attribute("scenes")
        print_blender_data_attribute("objects")
        print_blender_data_attribute("images")

def reset_composite_nodes():
    compositing = bpy.data.scenes['Scene'].node_tree

    for node in compositing.nodes:
            if node.type == 'COMPOSITE':
                node.mute = False


def setup_compositing(type: str, model):
    """Sets up the 'Scene' compositor to use a predefined compositing setup

    Args:
        type (str): Type of setup to use
    """
    bpy.context.scene.render.use_compositing = True
    bpy.context.scene.use_nodes = True

    #change to use the 'Scene' for shadow rendering
    bpy.context.window.scene = bpy.data.scenes['Scene']

    compositing = bpy.data.scenes['Scene'].node_tree

    if type == "shadowmap":
        default_shadow = compositing.nodes['default_shadow']
        squeezed_shadow = compositing.nodes['squeezed_shadow']

        default_shadow.image = bpy.data.images[f"{model.object.name}_shadowmap"]
        squeezed_shadow.image = bpy.data.images[f"{model.object.name}_shadowmap_squeeze"]

        # deselect all nodes from the compositor
        for node in compositing.nodes:
            node.select = False

        #mute all unneeded compositing nodes and only select 'shadow_mix' node
        for node in compositing.nodes:
            if node.type == 'COMPOSITE':
                if node.name != "shadow_mix":
                    node.mute = True
                else:
                    node.select = True
                    bpy.context.scene.node_tree.nodes.active = node
    else:
        log.debug("No other behavior was setup here. Define more if needed!")


def render(file_output:str):
    """Renders an image to the given output for the 'Scene'

    Args:
        file_output (str): Directory and name of the saved image

    Returns:
        Rendered image
    """
    bpy.context.window.scene = bpy.data.scenes['Scene']
    bpy.context.scene.render.image_settings.file_format = 'JPEG'
    bpy.context.scene.render.filepath = file_output
    bpy.ops.render.render(write_still = True)

    image = bpy.data.images.load(file_output)
    reset_composite_nodes()

    bpy.context.window.scene = bpy.data.scenes['Resources']

    return image