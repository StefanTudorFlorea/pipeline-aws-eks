import collections
import math
from typing import Iterable
from mathutils import Vector, Euler
import bpy
import bmesh
from bpy import types as T

from app.core.abstract.decorators import time_decorator
from app.core.utilities.misc import deselect_everything
from app.core.log import Log
from app.core.model import Model

log = Log()
model = Model()


@time_decorator
def world_min_max(blender_objects, reset_z=False):
    """
    returns minimum and maximum corner of one or a list of meshes

    Args:
        blender_object (blender_object):  list of blender_object

    Returns:
        float: Minimum Corner
        float: Maximum Corner
    """
    x_list, y_list, z_list = [], [], []
    for blender_object in blender_objects:
        log.info(blender_object.name)
        coordinates = [
            blender_object.matrix_world @ v.co
            for v in blender_object.data.vertices
        ]
        x_list.extend([co.x for co in coordinates])
        y_list.extend([co.y for co in coordinates])
        z_list.extend([co.z for co in coordinates])

    x_min, x_max = min(x_list), max(x_list)
    y_min, y_max = min(y_list), max(y_list)
    z_min, z_max = min(z_list), max(z_list)
    if not reset_z:
        minimum_corner = Vector((x_min, y_min, z_min))
        maximum_corner = Vector((x_max, y_max, z_max))
    else:
        minimum_corner = Vector((x_min, y_min, 0))
        maximum_corner = Vector((x_max, y_max, 0))
    return minimum_corner, maximum_corner


@time_decorator
def dimensions(blender_object):
    """Returns the dimension of an Object

    Args:
        blender_object (blender_object): Base blender_object Class

    Returns:
        float: Size of the Object
    """
    minimum_corner, maximum_corner = world_min_max(blender_object)
    return maximum_corner - minimum_corner


@time_decorator
def midpoint(blender_object):
    """Returns the Midpoint of an Object

    Args:
        blender_object (blender_object): Base blender_object Class

    Returns:
        float: Midpoint
    """
    min_bound, max_bound = world_min_max(blender_object)
    return min_bound + 0.5 * (max_bound - min_bound)


@time_decorator
def minimum_sphere_radius(blender_object):
    """Returns the Minimum Sphere Radius of an Object

    Args:
        blender_object (blender_object): Base blender_object Class

    Returns:
        float: Minimum Sphere Radius
    """
    mpoint = midpoint(blender_object)
    radius = 0
    radius = max(
        radius,
        max(
            (mpoint - blender_object.matrix_world @ v.co).length
            for v in blender_object.data.vertices
        ),
    )
    return radius


@time_decorator
def estimate_inverse_world_scaling(dimensions):
    """Returns an estimate of the scale by given dimensions

    Args:
        dimensions (bpy Object): Dimensions

    Returns:
        float: Scale
    """
    scale = None
    if dimensions.length < 0.05:  # if it is really small: 0.01% x0.01
        scale = 100
    if dimensions.length > 20:  # 1000% x10
        scale = 0.1
    if dimensions.length > 90:  # if it is really large: 10000% x100
        scale = 0.01
    return scale


@time_decorator
def decimate(highpoly, target_polycount):
    """Decimates the main object to the target_polycount.

    Args:
        highpoly (bpy Object): Blender Object with high polygon count
        target_polycount (int): Maximum Polygon count

    Returns:
        bpy Object: Low Poly Object
    """
    lowpoly = highpoly
    # highpoly = lowpoly.copy()
    # C.collection.objects.link(highpoly)
    bpy.context.view_layer.objects.active = lowpoly
    bpy.ops.object.modifier_add(type="DECIMATE")
    ratio = target_polycount / len(lowpoly.data.polygons)
    bpy.context.active_object.modifiers["Decimate"].ratio = ratio
    bpy.ops.object.modifier_apply(modifier="Decimate")
    # return highpoly


@time_decorator
def scale_normalize(blender_object):
    """Normalize the scale of an Object

    Args:
        blender_object (blender_object): Base blender_object Class
    """
    blender_object.scale = blender_object.scale / max(blender_object.scale)


@time_decorator
def maximum_dimension_value(blender_object):
    """_summary_

    Args:
        blender_object (_type_): _description_

    Returns:
        _type_: _description_
    """
    return max(blender_object.dimensions)


@time_decorator
def dimension(blender_object, size):
    """Change the dimension to size of an Object
        It is not clear on which dimension the size will be applied.
    Args:
        blender_object (blender_object): Base blender_object Class
        size (float): Target dimension value
    """

    # Normalize Object
    max_size = maximum_dimension_value(blender_object)

    # Multiply normalize size with value
    blender_object.dimensions = (blender_object.dimensions / max_size) * size


@time_decorator
def dimension_normalize(blender_object):
    """Normalize the dimensions of an Object
    Args:
        blender_object (blender_object): Base blender_object Class
    """
    dimension(blender_object, 1.0)


@time_decorator
def recenter(blender_objects):
    """Repositions meshes, so the boundingbox is on x-y median and z=0

    Args:
       blender_object (object): list of blender object that are used for recalulating new center
    """

    minimum_corner, maximum_corner = world_min_max(blender_objects)
    for object in blender_objects:
        if object.parent is None:
            deselect_everything()
            object.select_set(True)
            object.location = object.location - Vector(
                (
                    (minimum_corner.x + maximum_corner.x) / 2.0,
                    (minimum_corner.y + maximum_corner.y) / 2.0,
                    minimum_corner.z,
                )
            )
            bpy.ops.object.transform_apply()
    deselect_everything()


@time_decorator
def normalize(blender_object):
    """_summary_

    Args:
        blender_object (_type_): _description_
    """
    deselect_everything()
    blender_object.select_set(True)
    dimension = dimensions(blender_object)
    scale = 1.0 / max(dimension)
    blender_object.scale = (scale, scale, scale)
    bpy.ops.object.transform_apply()
    deselect_everything()


@time_decorator
def rotate(blender_object, x=0, y=0, z=0):
    """Set x,y,z object rotation in world space using degrees.

    Args:
        blender_object (object): the main object
        x (int, optional): an angle to rotate along x-axis. Defaults to 0.
        y (int, optional): an angle to rotate along y-axis. Defaults to 0.
        z (int, optional): an angle to rotate along z-axis. Defaults to 0.

    Returns:
        boolean
    """

    before_rotation = (
        blender_object.rotation_euler[0],
        blender_object.rotation_euler[1],
        blender_object.rotation_euler[2],
    )
    log.info(f"the original rotation is {before_rotation}")
    rotation_vector = (
        math.radians(float(x)),
        math.radians(float(y)),
        math.radians(float(z)),
    )
    log.info(f"the math.radians transformed rotation is {rotation_vector}")
    blender_object.rotation_euler = Euler(
        (rotation_vector[0], rotation_vector[1], rotation_vector[2]), "XYZ"
    )
    after_rotation = (
        blender_object.rotation_euler[0],
        blender_object.rotation_euler[1],
        blender_object.rotation_euler[2],
    )
    log.info(f"the object after rotation is  {after_rotation}")
    if after_rotation == before_rotation:
        log.info("object is not rotated. Check the setting again, please!")
        return False
    else:
        log.info("object is rotated")
        return True


@time_decorator
def join(blender_objects, parent_object):
    """_summary_

    Args:
        blender_objects (_type_): _description_
        parent_object (_type_): _description_
    """
    deselect_everything()
    for obj in blender_objects:
        obj.select_set(True)
    bpy.context.view_layer.objects.active = parent_object
    bpy.ops.object.join()


@time_decorator
def parent_all(blender_objects, parent_object):
    """_summary_

    Args:
        blender_objects (_type_): _description_
        parent_object (_type_): _description_
    """
    for obj in blender_objects:
        if obj != parent_object:
            parent(obj, parent_object)


@time_decorator
def parent(child, new_parent):
    """Parent given child with new parent

    Args:
        child (bpy Object): Children for new Parent
        new_parent (bpy Object): New Parent of Children
    """
    matrix_world = child.matrix_world.copy()
    child.parent = new_parent
    child.matrix_world = matrix_world


@time_decorator
def unparent(child):
    """Unparent a given Child

    Args:
        child (bpy Object): Child to unparent
    """
    matrix_world = child.matrix_world.copy()
    child.parent = None
    child.matrix_world = matrix_world


@time_decorator
def compact(low_poly, high_poly, samples, margin, max_cage_offset, tilesize):
    """Baking textures from highpoly mesh to lowpoly mesh

    Args:
        low_poly (bpy Object): Low Poly Object
        high_poly (bpy Object): High Poly Object
        samples (int): Samples size by Settings
        margin (int): Margin size by Settings
        max_cage_offset (int): Offset by Settings
        tilesize (int): Tilesize by Settings
    """

    to_material = bpy.data.materials.new("Model.0")
    to_material.use_nodes = True
    to_image_node = to_material.node_tree.nodes.new("ShaderNodeTexImage")
    low_poly.data.materials.clear()
    low_poly.data.materials.append(to_material)

    bpy.context.scene.render.bake.margin = margin
    bpy.context.scene.render.bake.cage_extrusion = max_cage_offset
    bpy.context.scene.cycles.samples = samples
    bpy.context.scene.render.tile_x = bpy.context.scene.render.tile_y = tilesize
    bpy.context.scene.render.engine = "CYCLES"

    high_poly.select_set(True)
    low_poly.select_set(True)
    bpy.context.view_layer.objects.active = low_poly
    to_material.node_tree.nodes.active = to_image_node
    for material in high_poly.data.materials:
        size = 1024
        # print("Baking diffuse")
        to_image_node.image = bpy.data.images.new(
            name="diffuse", width=size, height=size
        )  # ToDo use predefined size
        bpy.ops.object.bake(
            type="DIFFUSE",
            pass_filter=set({"COLOR"}),
            use_selected_to_active=True,
        )

        # print("Baking normal")
        to_image_node.image = bpy.data.images.new(
            name="normal", width=size, height=size
        )  # ToDo use predefined size
        bpy.ops.object.bake(type="NORMAL", use_selected_to_active=True)

    # print("Deleting highpoly object.")
    bpy.ops.object.delete({"selected_objects": [high_poly]})
    return low_poly


@time_decorator
def check_if_uv_coords_are_normalized(blender_object: T.Object):
    """_summary_

    Args:
        blender_object (T.Object): _description_

    Returns:
        _type_: _description_
    """
    uv_coords_are_normalized = True
    for loop in blender_object.data.loops:
        coords = blender_object.data.uv_layers.active.data[loop.index].uv
        if not (coords.x >= 0 and coords.x <= 1):
            log.error(
                f"Invalid x-coordinate of loop {loop.index} with value {coords.x}"
            )
            uv_coords_are_normalized = False
        if not (coords.y >= 0 and coords.y <= 1):
            log.error(
                f"Invalid y-coordinate of loop {loop.index} with value {coords.y}"
            )
            uv_coords_are_normalized = False
    return uv_coords_are_normalized


@time_decorator
def create_ground_plane(
    minimum_corner, maximum_corner, margin=0.6, name="shadow_plane"
):
    """Create a Ground Plane underneath the given meshes with a margin in metres for Shadow Plane Creation & Baking

    Args:
        minimum_corner (float): Minimum Corner Vector of Object
        maximum_corner (float): Maximum Corner Vector of Object
        margin (float, optional): Defaults to 0.6.
        name (str, optional): Defaults to "shadow_plane".
    """
    dimensions = maximum_corner - minimum_corner
    log.info(f"DIMENSIONS is {dimensions}")
    bm = bmesh.new()
    dx = dimensions.x * 0.5 + margin
    dy = dimensions.y * 0.5 + margin
    if maximum_corner[2] != 0:
        dz = 0.001
    else:
        dz = 0
    locations = [
        (-dx, -dy, dz),
        (-dx, +dy, dz),
        (+dx, +dy, dz),
        (+dx, -dy, dz),
    ]  # 3, 2, 1, 4
    log.info(f"LOCATIONS are {locations}")
    for v in locations:
        bm.verts.new(v)
    bm.verts.ensure_lookup_table()
    for f in [(3, 2, 1, 0)]:
        bm.faces.new([bm.verts[i] for i in f])
    uv_layer = bm.loops.layers.uv.new("UV")
    uvs = [(1.0, 0.0), (1.0, 1.0), (0.0, 1.0), (0.0, 0.0)]
    for f in bm.faces:
        for i, l in enumerate(f.loops):
            l[uv_layer].uv = uvs[i]

    mesh = bpy.data.meshes.new(name)
    bm.to_mesh(mesh)
    mesh.update()
    ground_plane = bpy.data.objects.new("Plane", mesh)
    bpy.context.collection.objects.link(ground_plane)
    deselect_everything()
    ground_plane.select_set(True)
    ground_plane.location = ground_plane.location + Vector(
        (
            (minimum_corner.x + maximum_corner.x) / 2.0,
            (minimum_corner.y + maximum_corner.y) / 2.0,
            dz,
        )
    )
    bpy.ops.object.transform_apply()
    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="MEDIAN")
    ground_plane.select_set(False)
    return ground_plane


@time_decorator
def apply_rotation(blender_object):
    """Apply the rotation of the main object.

    Args:
        blender_object (object): the main object
    """

    blender_object.select_set(True)
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)


@time_decorator
def set_active(blender_object):
    """Sets the given blender object active

    Args:
        blender_object (_type_): blender object
    """
    blender_object.select_set(True)
    bpy.context.view_layer.objects.active = blender_object


@time_decorator
def getChildren(object):
    list_of_children = [object]

    if len(object.children) > 0:
        # TODO: when using blender 3.1 or higher use object.children_recursive
        for child in object.children:
            list_of_children += getChildren(child)

    return list_of_children
