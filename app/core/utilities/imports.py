
import bpy
from bpy import context as C
from bpy import data as D
from bpy import ops as O
from bpy import types as T
import app.core.utilities.keywords as keywords
from app.core.utilities.material import (
    try_creating_material_from_image_list,
    texture_already_exists,
)
from pathlib import Path
from zipfile import ZipFile
import shutil
import json

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()

# https://stackoverflow.com/a/55597760
module = Path(__file__).resolve().parents[1]
addons = module / "addons"

from app.core.abstract.decorators import time_decorator

@time_decorator
def import_blend(filepath):
    """Import Data from Blend File

    Args:
        filepath (string): Filepath
    """
    with D.libraries.load(filepath) as (data_from, data_to):
        data_to.objects.extend(data_from.objects)
        data_to.materials.extend(data_from.materials)
        data_to.images.extend(data_from.images)

# break down big functions into samll functions for debugging.
@time_decorator
def import_obj(filepath, mtl=False):
    """import .obj file, find the relevant images and materials. But it is not yet links to the categories.

    Args:
        filepath (string): the filepath of the object
        mtl (boolean): a .mtl file exists in the setting.file_path or not

    Raises:
        Exception: Nonetype

    Returns:
        object_dict (dictionary): a dictionary with at least one mandatory key: "imported_objects". Two optional keys: unassigned_textures, materials
    """
    # scan the default objects
    rooomready_default_objs = set(bpy.data.objects)
    filepath = Path(filepath) if isinstance(filepath, str) else filepath
    try:
        bpy.ops.import_scene.obj(filepath=filepath.as_posix())
        object_dict ={} # find the images and the object name
        if not mtl:
            object_dict["imported_objects"]=[] # find the object name
            for obj in bpy.data.objects:
                if obj.type == "MESH" and obj not in rooomready_default_objs:
                    object_dict["imported_objects"].append(obj)
        else:
            # if there is a mtl file, then we should check the material and images.
            object_dict= {"imported_objects":[], "materials":[], "unassigned_textures":[]} # find the object name, material name, and connected image files
            for obj in bpy.data.objects:
                if obj.type == "MESH" and obj not in rooomready_default_objs:
                    object_dict["imported_objects"].append(obj)
                    for mat in obj.material_slots:
                        if mat.material.node_tree:
                            object_dict["materials"].append(mat)
                            nodes = mat.material.node_tree.nodes
                            for node in nodes:
                                if node.type == "TEX_IMAGE":
                                    object_dict["unassigned_textures"].append(node.image)
        return object_dict
    except Exception as e:
        raise e


@time_decorator
def import_fbx_or_glb(filepath, png=False):
    """import fbx, glb, or gltf

    Args:
        filepath (str): the object's file path
        png (bool, optional): if there is an image in the filepath or not. Defaults to False.

    Raises:
        e: exceptions

    Returns:
        dict: a dictionary
    """
    rooomready_default_objs = set(bpy.data.objects)
    filepath = Path(filepath) if isinstance(filepath, str) else filepath
    object_dict = {"imported_objects":[], "unassigned_textures": []}
    try:
        importers = {
            ".fbx": bpy.ops.import_scene.fbx,
            ".glb": O.import_scene.gltf,
            ".gltf": O.import_scene.gltf,
        }
        if importers.get(filepath.suffix.lower()):
            importer = importers[filepath.suffix]
            log.info(f"Use {importer} as the importer")
        importer(filepath = filepath.as_posix())
        # check folder
        if not png:
            # then the textures is packed
            for obj in bpy.data.objects:
                if obj.type == "MESH" and obj not in rooomready_default_objs:
                    object_dict["imported_objects"].append(obj)
                    for mat in obj.material_slots:
                        if mat.material.node_tree:
                            nodes = mat.material.node_tree.nodes
                            for node in nodes:
                                print(f"I found node as {node}, node.type is {node.type}")
                                if node.type == "TEX_IMAGE":
                                    print(f"node.image is {node.image}")
                                    object_dict["unassigned_textures"].append(node.image) #bpy.images
        else:
            # the textures are not packed.
            for obj in bpy.data.objects:
                if obj.type == "MESH" and obj not in rooomready_default_objs:
                    object_dict["imported_objects"].append(obj)
        return object_dict
    except Exception as e:
        raise e

@time_decorator
def import_file(filepath):
    """Import file containing mesh or scene based on the extention of the file
    (Soon to be retired!!!!! by MSW)

    Args:
        filepath (string): Filepath

    Raises:
        Exception: No import function for file format

    Returns:
        Triple(Objects, Materials, Images):
    """
    filepath = Path(filepath) if isinstance(filepath, str) else filepath
    # check the file suffix
    importers = {
        ".ply": O.import_mesh.ply,
        ".stl": O.import_mesh.stl,
        ".x3d": O.import_scene.x3d,
        ".dae": O.wm.collada_import,
        ".abc": O.wm.alembic_import,
    }

    if filepath.suffix.lower() in [".stp", ".step", ".st"]:
        """
        Check for add-on. However, I think this step can be removed if we use Docker.
        """
        if "STEPper" not in C.preferences.addons.keys():
            O.preferences.addon_install(filepath=str(module / "addons" / "STEPper.zip"))
            print("Installed addon STEPper")
        print("Enabling addon STEPper")
        O.preferences.addon_enable(module="STEPper")

    if hasattr(O.import_scene, "occ_import_step"):
        importers["stp"] = importers[".step"] = importers[
            ".st"
        ] = O.import_scene.occ_import_step

    if filepath.suffix.lower() not in importers.keys():
        raise Exception(f"No import function for file format '{filepath.suffix}'")

    objects_before = set(D.objects)
    materials_before = set(D.materials)
    images_before = set(D.images)
    importers[filepath.suffix.lower()](filepath=str(filepath))
    objects_after = set(D.objects)
    materials_after = set(D.materials)
    images_after = set(D.images)

    new_objects = list(objects_after - objects_before)
    new_materials = list(materials_after - materials_before)
    new_images = list(images_after - images_before)

    return new_objects


@time_decorator
def run_fast_scandir(dir, extensions):  # dir: str, ext: list
    """scanning through a given file path. Flatten a two layer nested directory

    Args:
        dir (string): the path of the target directory
        extension (list): a list of extension for checking

    Returns:
        files (string): path of files
    """
    files = []
    dir_path = Path(dir)
    for file in dir_path.iterdir():
        if file.name == "setting.txt":
            files.append(file.absolute().as_posix())
        elif file.is_file() and Path(file).suffix.lower() in extensions:
            files.append(file.as_posix())
        elif file.is_dir():
            files.extend(
                [sf.absolute().as_posix() for sf in file.iterdir() if sf.suffix.lower() in extensions]
            )
    return files



@time_decorator
def import_from_folder(
    folder,
    recursive=False,
    directory_blacklist=[],
    directory_whitelist=[],
    extensions=[]
    ):
    """Scan the directory and find the supported files by the suffixs of files.

    Args:
        folder (dir): an input directory
        recursive (bool, optional): [unused argument]. Defaults to False.
        directory_blacklist (list, optional): [unused argument]. Defaults to keywords.directory_blacklist.
        directory_whitelist (list, optional): [unused argument]. keywords.directory_whitelist.
        extensions (list): A list of extension to search for the files.

    Returns:
        path_dict(dictionary): a dictionary with paths.
    """
    # ToDo: implement folder parsing. Finding of images as a material or as diffuse for planes
    path_dict = {}
    path_dict["export_directory"] = setting.export_directory
    if Path(folder).is_file() and Path(folder).suffix.lower() != ".zip":
        # must be a directory input
        log.error("Input is not a folder")
        raise ValueError
    elif Path(folder).is_file() and Path(folder).suffix.lower() == ".zip":
        export_tmp = Path(setting.export_directory) / "tmp"
        export_tmp.mkdir(parents=True, exist_ok=True)  # create subfolder
        with ZipFile(Path(folder), "r") as archive:
            archive.extractall(export_tmp.as_posix())
        path_dict["file_paths"] = run_fast_scandir(export_tmp.as_posix(), extensions)
    else:
        path_dict["file_paths"] = run_fast_scandir(folder, extensions)
    return path_dict


@time_decorator
def load_babylon(export_directory):
    """load a babylon file from directory

    :param export_directory: where to export
    :type export_directory: dir
    :return: json file
    :rtype: json
    """
    babylon_path = None
    for file in Path(export_directory).iterdir():
        if file.endswith(".babylon"):
            checkValue = True
            babylon_path = Path(export_directory) / file

    if babylon_path:
        file = open(
            babylon_path,
        )
        return json.load(file)
    else:
        return None
