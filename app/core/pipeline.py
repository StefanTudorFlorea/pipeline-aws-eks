import sys, traceback, time

# import bpy
# from app.core.utilities.ascii import get_logo
# Import Task
from app.core.tasks.imports.import_data import ImportDataTask
from app.core.tasks.imports.categorize import CategorizeDataTask
from app.core.tasks.imports.setup_export_dir import SetupExportDirTask

# Geometry Task
from app.core.tasks.geometry.compact import CompactTask
from app.core.tasks.geometry.edge_split import EdgeSplitTask
from app.core.tasks.geometry.recenter import RecenterTask
from app.core.tasks.geometry.remove_doubles import RemoveDoublesTask

from app.core.tasks.geometry.resize import ResizeTask
from app.core.tasks.geometry.rotate import RotateTask
from app.core.tasks.geometry.shade_smooth import ShadeSmoothTask
from app.core.tasks.geometry.recalculate_face_normals import (
    RecalculateFaceNormalsTask,
)
from app.core.tasks.geometry.validate_uvs import ValidateUVsTask

# Material Task
from app.core.tasks.material.merge_materials import MergeMaterialsTask
from app.core.tasks.material.bake_shadow import BakeShadowForGroundPlaneTask
from app.core.tasks.material.bake_shadowmap import BakeShadowForMeshTask
from app.core.tasks.material.normalize_uv_layer import NormalizeUVLayerTask

# from app.core.tasks.material.real_estate_baking import RealEstateBakingTask

# Misc
from app.core.tasks.misc.device import ComputeDeviceTask
from app.core.tasks.misc.imposter import ImposterTask
from app.core.tasks.misc.turntable import TurntableTask
from app.core.tasks.misc.babylon_edit import BabylonEditTask
from app.core.tasks.misc.render_image import RenderImageTask


# Export
from app.core.tasks.export.export_textures import ExportTexturesTask
from app.core.tasks.export.export_geometry import ExportGeometryTask
from app.core.tasks.export.export_meta_data import ExportMetaDataTask
from app.core.tasks.export.validate_model_data import ValidateModelDataTask
from app.core.tasks.export.export_polycount import ExportPolyCountTask

# Essentials
from app.core.setting import Setting
from app.core.model import Model
from app.core.log import Log

setting = Setting()
model = Model()
log = Log()


traceback_template = (
    """File "%(filename)s", line %(lineno)s, in %(name)s %(type)s"""
)


class Pipeline(object):
    """
    Handle Settings File
    Load Objects
    Build Pipeline
    """

    def __init__(self):
        self.queue = []

    def execute(self):
        """
        [Execute the pipeline with progress bar]
        """
        # update log handlers

        log.info(setting.to_dict())
        print(self.queue)
        # Iterate over Tasks while calculating the progress bar
        # with alive_bar(len(self.queue), force_tty=True) as bar:
        for index, task in enumerate(self.queue):
            # Pre Execution
            time_start = time.perf_counter()
            print(f"@--current_task@{str(index + 1)}")
            sys.stdout.flush()

            print(f"@--max_task@{str(len(self.queue))}")
            sys.stdout.flush()

            print(f"@--export_directory@{str(setting.export_directory)}")
            sys.stdout.flush()

            print(f"@--file_path@{str(setting.import_path)}")
            sys.stdout.flush()

            print(f"@--current_task_name@{str(task.__class__.__name__)}")
            sys.stdout.flush()
            # Execution
            try:
                if task.poll() == False:
                    # raise Exception("Task poll returned false!")
                    # Do not raise exception
                    # Task poll false means, precondition not full filled, which however is not necessarily
                    # an error (e.g. JoinGeometries with only one geometry)
                    log.info("SKIP %s" % str(task.__class__.__name__))
                    continue
                else:
                    log.info("PASS POLL %s" % str(task.__class__.__name__))
                log.info("EXECUTE %s" % str(task.__class__.__name__))
                if setting.unit_testing:
                    print("EXECUTE UNIT TEST")
                    task.unit_test()
                else:
                    print("EXECUTE TASK")
                    task.execute()
                # log.export_txt(setting.export_directory)
            except Exception as e:
                msg = "Failed to execute task {}".format(
                    task.__class__.__name__
                )
                log.error(msg)
                log.error(traceback.format_exc())
            finally:  # post execution
                if index > 1:
                    model.print_encode_data(model.get_meta_data())

                time_elapsed = time.perf_counter() - time_start

                log.time(type(task).__name__, round(time_elapsed, 3))

    def append_task(self, task, task_switch=True):
        """Append tasks to the pipeline"""
        if task_switch:
            log.info(f"add {task.__class__.__name__}")
            self.queue.append(task)

    def create_polycount_pipeline(self):
        """
        Import Data and do Polycount
        """
        task = SetupExportDirTask()
        self.queue.append(task)
        log.info("SetupExportDirTask is appended")

        task = ImportDataTask()
        self.queue.append(task)
        log.info("ImportDataTask is appended")

        task = ExportPolyCountTask()
        self.queue.append(task)
        log.info("ExportPolyCountTask is appended")

    def create_preview_pipeline(self):
        self.append_task(ImportDataTask())
        self.append_task(RenderImageTask())

    def create_rooomready_pipeline(self):
        task = SetupExportDirTask()
        self.queue.append(task)
        log.info("SetupExportDirTask is appended")

        task = ImportDataTask()
        self.queue.append(task)
        log.info("ImportDataTask is appended")

        task = CategorizeDataTask()
        self.queue.append(task)
        log.info("CategorizeDataTask is appended")

        if (
            setting.geometry_rotate_x
            or setting.geometry_rotate_y
            or setting.geometry_rotate_z
        ):
            task = RotateTask()
            self.queue.append(task)
            log.info("RotateTask is appended")

        if setting.bake_shadow:
            task = BakeShadowForMeshTask()
            self.queue.append(task)
            log.info("BakeShadowForMeshTask is appended")

        if setting.bake_shadowmap:
            task = BakeShadowForGroundPlaneTask()
            self.queue.append(task)
            log.info("BakeShadowForGroundPlaneTask is appended")

        task = ExportTexturesTask()
        self.queue.append(task)
        log.info("ExportTexturesTask is appended")

        task = ExportGeometryTask()
        self.queue.append(task)
        log.info("ExportGeometryTask is appended")

    def create_autobake_pipeline(self):
        """
        [Evaluates the Settings file to create a Pipeline]
        """
        # preprocessing_tasks = [ResizeTask,ShadowPlaneTask,BlaTask,LalaTask]
        # processing_tasks = []
        # export_task = []
        print("CREATE AUTOBAKE PIPELINE")

        self.append_preprocessing_tasks()

        self.append_processing_tasks()

        self.append_postprocessing_tasks()

        self.append_export_data_tasks()

        self.append_sanity_check_tasks()

        print(f"@--max_task@ {str(len(self.queue))}")
        sys.stdout.flush()

    def append_preprocessing_tasks(self):

        task = ImportDataTask()
        print(task)
        self.queue.append(task)

        task = CategorizeDataTask()
        print(task)
        self.queue.append(task)

    def append_processing_tasks(self):
        msg = "Create Processing Queue"
        log.info(msg)

        if setting.decimate:
            log.info("add CompactTask")
            task = CompactTask()
            self.queue.append(task)

        if setting.geometry_resize:
            log.info("add ResizeTask")
            task = ResizeTask()
            self.queue.append(task)

        if (
            setting.geometry_rotate_x
            or setting.geometry_rotate_y
            or setting.geometry_rotate_z
        ):
            log.info("add RotateTask")
            task = RotateTask()
            self.queue.append(task)

        if setting.geometry_smooth:
            log.info("add ShadeSmoothTask")
            task = ShadeSmoothTask()
            self.queue.append(task)

        if setting.geometry_edge_split:
            log.info("add EdgeSplitTask")
            task = EdgeSplitTask()
            self.queue.append(task)

        if setting.geometry_remove_doubles:
            log.info("add RemoveDoublestask")
            task = RemoveDoublesTask()
            self.queue.append(task)

        if setting.geometry_recenter:
            log.info("add RecenterTask")
            task = RecenterTask()
            self.queue.append(task)

        if setting.geometry_recalc_normals:
            log.info("add RecalculateFaceNormalsTask")
            task = RecalculateFaceNormalsTask()
            self.queue.append(task)

        if setting.normalize_uv_space:
            log.info("add Normalize UV SpaceTask")
            task = NormalizeUVLayerTask()
            self.queue.append(task)

        if setting.validate_uvs:
            log.info("add ValidateUVsTask")
            task = ValidateUVsTask()
            self.queue.append(task)

    def append_postprocessing_tasks(self):
        if setting.bake_shadow:
            task = BakeShadowForGroundPlaneTask()
            self.queue.append(task)

        if setting.bake_shadowmap:
            task = BakeShadowForMeshTask()
            self.queue.append(task)

    def append_sanity_check_tasks(self):
        if setting.validate_shadow_map:
            task = ValidateModelDataTask()
            self.queue.append(task)

    def append_export_data_tasks(self):
        if (
            setting.export_geometry_as_obj
            or setting.export_geometry_as_glb
            or setting.export_geometry_as_babylon
        ):
            task = ExportGeometryTask()
            self.queue.append(task)

        if setting.export_textures:
            task = ExportTexturesTask()
            self.queue.append(task)
        if setting.misc_imposter:
            task = ImposterTask()
            self.queue.append(task)

        if setting.misc_turntable:
            task = TurntableTask()
            self.queue.append(task)

        if setting.export_meta_data:
            task = ExportMetaDataTask()
            self.queue.append(task)
        if setting.export_geometry_as_babylon and setting.misc_edit_babylon:
            task = BabylonEditTask()
            self.queue.append(task)

    def append_sanity_checks_tasks(self):
        pass
