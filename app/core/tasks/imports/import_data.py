from pathlib import Path

import bpy

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.imports import (
    import_fbx_or_glb,
    import_obj,
    import_from_folder,
)
from app.core.utilities.material import texture_already_exists
from app.core.utilities import export, geometry
from app.core.abstract.decorators import time_decorator
import app.core.utilities.keywords as keywords
from app.core.utilities.blender import (
    print_scene_data,
    print_blender_data_attribute,
)

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
setting = Setting()
log = Log()


class ImportDataTask(AbstractTask):
    """Import data to the file paths

    Args:
        AbstractTask (ABC class): inherits from the template in abstract_task.py
    """

    def __init__(self):
        """import data"""
        super().__init__()

    @time_decorator
    def import_files_to_model(self):
        """
        1. Importing geometry to model.imported_objects.
        2. Place textures in the model.unassigned_texture (either in bpy.data for filepath)

        Args:
            filepaths(list): a list of filepaths
        """
        for filepath in setting.file_paths:
            suffix = Path(filepath).suffix.lower()
            assert len(suffix) > 0 and suffix[0] == ".", "Invalid file"
            if suffix not in keywords.supported_geometries:
                continue
            if suffix == ".obj":
                search_mtl = [
                    file
                    for file in setting.file_paths
                    if Path(file).suffix == ".mtl"
                ]
                if search_mtl:
                    log.info(
                        f"Importing geometry {filepath}, remember to categorize the materials later!"
                    )
                    tmp_dict = import_obj(filepath, mtl=True)
                    model.imported_objects = tmp_dict.get("imported_objects")
                    model.unassigned_textures = tmp_dict.get(
                        "unassigned_textures"
                    )  # bpy.images
                    model.file_type = Path(filepath).suffix.lower()
                else:
                    tmp_dict = import_obj(filepath, mtl=False)
                    log.info(f"Importing geometry {filepath} without mtl!")
                    model.imported_objects = tmp_dict.get("imported_objects")
                    model.file_type = Path(filepath).suffix.lower()
                    log.info(f"Model File Type: {model.file_type}")
                continue
            elif suffix in [".fbx", ".glb", ".gltf"]:
                search_img = [
                    file
                    for file in setting.file_paths
                    if Path(file).suffix.lower() in keywords.supported_textures
                ]
                if search_img:
                    log.info(
                        f"Importing geometry {filepath}. Texture images are not packed, remember to import material later!"
                    )
                    tmp_dict = import_fbx_or_glb(filepath, png=True)
                    model.imported_objects = tmp_dict.get("imported_objects")
                    model.file_type = Path(filepath).suffix.lower()
                else:
                    log.info(
                        f"Importing geometry {filepath} together with packed images."
                    )
                    tmp_dict = import_fbx_or_glb(filepath, png=False)
                    model.imported_objects = tmp_dict.get("imported_objects")
                    model.file_type = Path(filepath).suffix.lower()
                    model.unassigned_textures = tmp_dict.get(
                        "unassigned_textures"
                    )
                    log.info(
                        f"the model.unassigned_textures looks like {model.unassigned_textures}"
                    )

        # do some first processing steps on the object to ensure its fitting the requirements
        log.info(f"imported objects are {model.imported_objects}")
        geometry.recenter(model.imported_objects)
        standard_name = [
            Path(x)
            for x in setting.file_paths
            if Path(x).suffix in keywords.supported_geometries
        ][0].stem
        # for filepath in setting.file_paths:
        #     old_name: str = ""
        for object in model.imported_objects:
            index = model.imported_objects.index(object)
            if index == 0:
                # rename main object to provided file name
                model.object = model.imported_objects[0]
                old_name = model.object.name
                model.object.name = standard_name
            else:
                # rename subobjects to fit the main object name schema
                object.name = object.name.replace(old_name, standard_name)
            geometry.apply_rotation(object)
            geometry.set_active(object)

    @time_decorator
    def import_materials(self, filepath):
        """
        import materials.
        Skip files with unspported_textures or object has textures.

        Args:
            filepath(string): filepath

        Return:
        model.unassigned_textures(filepath)
        """
        if model.unassigned_textures:
            if texture_already_exists(filepath):
                log.debug(
                    f"Skipping texture '{filepath}' is already in bpy.data.images"
                )
        else:
            log.info("Check the file path if there is any image.")
            for filepath in setting.file_paths:
                if Path(filepath).suffix.lower() in keywords.supported_textures:
                    model.unassigned_textures.add(Path(filepath))

    @time_decorator
    def import_settings(self, file):
        """import settings

        Args:
            file (str): file path in string
        """
        log.error(f"{file}: Import settings not implemented")

    def execute(self) -> None:
        """execution block"""
        log.debug("Before importing:")
        print_scene_data()
        print_blender_data_attribute("materials")
        log.info(
            f"Execute Data import from setting import_path: '{setting.import_path}'"
        )
        log.debug(f"Include sub directories: {setting.misc_recursive}")
        # end result stores in setting.file_paths
        file_scanning_results = import_from_folder(
            setting.import_path,
            setting.misc_recursive,
            keywords.directory_blacklist,
            keywords.directory_whitelist,
            keywords.supported_geometries
            + keywords.supported_image_library
            + keywords.supported_textures
            + keywords.mask,
        )

        if (
            setting.export_directory
            != file_scanning_results["export_directory"]
        ):
            log.warning(
                "something must went wrong that you don't have a export directory setup."
            )

        setting.file_paths = [
            filepath for filepath in file_scanning_results["file_paths"]
        ]

        log.debug(f"setting.file_paths: {setting.file_paths}")
        # import user settings to setting. But this probably can be removed
        for filepath in setting.file_paths:
            if Path(filepath).name.lower() == "setting.txt":
                self.import_settings(filepath)
                continue
            if any(tag in str(filepath).lower() for tag in keywords.mask):
                model.mask = bpy.data.images.load(
                    str(filepath), check_existing=True
                )
                log.info(
                    "Found mask texture", str(filepath)
                )  # this maybe can delete.

        # import files from setting.file_paths to model.imported_objects and unassigned_texture
        if not setting.import_no_material:
            self.import_files_to_model()
            self.import_materials(filepath)

        log.info(f"Model imported {model}")
        log.debug(f"Model name is {model.name} {model.file_type}")
        log.debug(f"Objects: {model.imported_objects}")
        log.debug("After importing:")
        print_scene_data()
        print_blender_data_attribute("materials")

    def unit_test(self) -> None:
        """unit test"""
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        temp_dict["imported_objects"] = []
        for element in list(model.imported_objects):
            temp_dict["imported_objects"].append(element.name)

        temp_dict["unassigned_textures"] = []
        for element in list(model.unassigned_textures):
            temp_dict["unassigned_textures"].append(element.name)

        # Export Unit Test Data by checking setting
        if setting.unit_testing_stage != "TestImportData":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "import_data"
            / "output"
            / "unit_test.json"
        )
        print(f"unit test file path is {unit_test_file_path}")
        export.save_dict_as_json(temp_dict, unit_test_file_path)
