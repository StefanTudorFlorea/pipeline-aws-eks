from pathlib import Path

from app.core.abstract.abstract_task import AbstractTask
from app.core.abstract.decorators import time_decorator

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
setting = Setting()
log = Log()


class SetupExportDirTask(AbstractTask):
    """Check and setup an export directory.

    Args:
        AbstractTask (ABC class): inherits from the template in abstract_task.py
    """

    def __init__(self):
        """Setup export dir"""
        super().__init__()

    @time_decorator
    def find_or_create_export_directory(self):
        """Check if the export_directory is given correctly. If not, create."""
        if not setting.export_directory:
            new_export_dir = Path(setting.import_path) / "autobake"
            Path(new_export_dir).mkdir(parents=True, exist_ok=True)
            log.info(
                f"create a new export dir at {new_export_dir} because no export_directory is set"
            )
            setting.export_directory = new_export_dir
        elif not Path(setting.export_directory).exists():
            Path(setting.export_directory).mkdir(parents=True, exist_ok=True)
            log.info(
                f"create a new export dir at {setting.export_directory} because export_directory does not exist"
            )
        else:
            log.info(f"{setting.export_directory} is found, we are good to go!")

    def execute(self) -> None:
        """execution block"""
        log.info(f"import setting first")
        log.info(f"check if the export directory is given or is exist")
        self.find_or_create_export_directory()

    def unit_test(self) -> None:
        """unit test"""
        pass
