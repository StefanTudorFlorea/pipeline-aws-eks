"""
import data (images) from the source dir.
This function takes care of import data only.
"""
import sys

from bpy import data as D
from pathlib import Path

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.imports import import_obj, import_from_folder
from app.core.utilities.material import (
    try_creating_material_from_image_list,
    texture_already_exists,
)
from app.core.utilities import export
from app.core.abstract.decorators import time_decorator
import app.core.utilities.keywords as keywords
from app.core.utilities.blender import print_scene_data, print_blender_data_attribute

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class JoinGeometryTask(AbstractTask):
    def __init__(self):
        super().__init__()
    
    def poll(self) -> None:
        pass

    def execute(self) -> None:
        pass