import pathlib

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.material import (
    check_principled_shader,
    find_sub_objects_from_material_name,
)

import app.core.utilities.keywords as keywords
from app.core.utilities import export
from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
settings = Setting()
log = Log()


class CategorizeDataTask(AbstractTask):
    def __init__(self):
        """Categorization task"""
        super().__init__()

    def poll(self):
        """validation

        Returns:
            boolean: True is pass
        """
        if len(model.imported_objects) == 0:
            log.error(
                "Imported main models are zero. There could be a wrong naming convention e.G. 'plane' oder 'glass' is inside the main model name."
            )
            return False
        return True

    def categorize_geometry(self):
        """Categorize imported objects into different categories"""
        for mesh in model.imported_objects:
            log.info(f"categorize_geometry is now processing {mesh}")
            for sub_object in [
                "plane",
                "glass",
            ]:
                if any(
                    tag in mesh.name.lower()
                    for tag in getattr(keywords, sub_object)
                ):
                    log.info(f"{mesh.name} is a {sub_object} object")
                    if sub_object == "plane":
                        model.append("planes", mesh)
                    model.append(sub_object, mesh)
                    model.imported_objects.remove(mesh)
                    if len(mesh.material_slots) < 1:
                        log.info(
                            f"The {mesh.name} has no material slot. Remember to create one later!"
                        )
        if len(model.imported_objects) > 1:
            log.info(f"The model.imported_objects is {model.imported_objects}")
            log.info(
                f"More than one main object is found at this stage. Try to categorize the object from the material name"
            )
            for mesh in model.imported_objects:
                log.info(f"check object: {mesh}")
                for check_material in ["plane", "glass"]:
                    found = find_sub_objects_from_material_name(
                        mesh, check_material
                    )
                    log.info(f"found is {found}")
                    if found:
                        log.info(
                            f"{mesh.name} is a {sub_object} object according to the name of its material"
                        )
                        model.imported_objects.remove(mesh)
                        if check_material == "plane":
                            model.append("planes", mesh)
                        elif check_material == "glass":
                            model.append(sub_object, mesh)

    def categorize_materials_in_meshes(self):
        """categorize materials in mash from unassigned_texture to
        1. Texture.categorize
        2. model.materials
        """
        for mesh in model.imported_objects:
            log.info(f"mesh now is {mesh}")
            for (
                material
            ) in (
                mesh.data.materials
            ):  # materials[mesh.name]=list(mesh.data.materials)
                nodes = material.node_tree.nodes
                bsdf = nodes.get("Principled BSDF")
                if not bsdf:
                    log.info(f"{mesh.name} has no BSDF shader node")
                # if bsdf is true, list out all the images that are used in the bsdf.
                else:
                    used_images = check_principled_shader(nodes)
                    log.info(f"{used_images} are used in the model")
                    log.info(
                        f"model.unassigned_textures is {model.unassigned_textures}"
                    )
                    if not used_images and len(model.unassigned_textures) == 1:
                        log.info(
                            "Only one image in the unassgined texture. Assume it as the baseColor"
                        )
                        img = list(model.unassigned_textures)[0]
                        if isinstance(img, pathlib.Path):
                            model.append("diffuse", img)
                            model.unassigned_textures.remove(img)
                    elif len(used_images) != 0:
                        for img in used_images:
                            log.info(
                                f"img {img.name} found. img category is {img['category']}"
                            )
                            img_category = img["category"]
                            model.append(img_category, img)
                            if img in model.unassigned_textures:
                                model.unassigned_textures.remove(img)
                                log.info(
                                    "remove image from the unassigned_textures"
                                )

    def remove_vertex_color(self):
        """remove vertex color if any"""
        for obj in model.imported_objects:
            vertex_colors = obj.data.vertex_colors
            log.info(f"mesh color is {vertex_colors}")
            while vertex_colors:
                vertex_colors.remove(vertex_colors[0])

    def execute(self) -> None:
        """execution block"""
        # assumption: model.object remains None in case of multiple geometries/objects unless they are joined
        if len(model.imported_objects) == 1:
            log.debug(f"Set model.object to {model.imported_objects[0].name}")
            model.object = model.imported_objects[0]
            self.categorize_materials_in_meshes()
            log.info(
                f"model.diffuse is {model.diffuse}. model.normal is {model.normal}"
            )
            self.remove_vertex_color()
        else:
            log.debug(
                f"Scene contains {len(model.imported_objects)} main objects. We will try to categorize it"
            )
            log.info(f"imported object {model.imported_objects}")
            self.categorize_geometry()
            self.categorize_materials_in_meshes()
            if len(model.imported_objects) == 1:
                log.info(
                    f"Now, the model imported object is {model.imported_objects}. We will move it to the model.object"
                )
                model.object = model.imported_objects[0]
                self.remove_vertex_color()
            else:
                if not settings.join_geometry:
                    log.critical(
                        "I found more than 1 main objects. Please join the geometry and then try again."
                    )

        # change parenting and the name
        for tag in ["planes", "glass"]:
            sub_objects = getattr(model, tag)
            for sub_object in sub_objects:
                main_object_name = model.object.name
                if sub_object:
                    # check naming
                    if main_object_name not in sub_object.name:
                        if tag == "planes":
                            tag = "plane"
                        sub_object.name = f"{main_object_name}_{tag}"
                    # check the parent
                    if sub_object.parent != model.object:
                        sub_object.parent = model.object
        # check for uv maps here
        if not model.object.data.uv_layers:
            log.info(
                f"{model.name} has no uv layers. I will skip exporting the provided texture maps"
            )
            model.diffuse = []
            model.normal = []
            model.emission = []
            model.metallic = []
            model.roughness = []
            model.reflectivity = []

    def unit_test(self) -> None:
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        # check model.object
        temp_dict["object"] = model.object.name

        # check model.unassigned_textures
        if len(list(model.unassigned_textures)) == 0:
            temp_dict["unassigned_textures"] = False
        elif len(list(model.unassigned_textures)) != 0:
            temp_dict["unassigned_textures"] = []
            for element in list(model.unassigned_textures):
                temp_dict["unassigned_textures"].append(element.name)

        # check sub model
        if len(list(model.planes)) == 0:
            temp_dict["planes"] = False
        else:
            temp_dict["planes"] = []
            for element in list(model.planes):
                temp_dict["planes"].append(element.name)

        if len(list(model.glass)) == 0:
            temp_dict["glass"] = False
        else:
            temp_dict["glass"] = []
            for element in list(model.glass):
                temp_dict["glass"].append(element.name)

        # check textures
        for model_attribute in [
            "diffuse",
            "normal",
            "emission",
            "metallic",
            "roughness",
            "reflectivity",
            "shadow",
            "shadowmap",
        ]:
            if len(getattr(model, model_attribute)) == 0:
                temp_dict[model_attribute] = False
            else:
                temp_dict[model_attribute] = []
                for element in getattr(model, model_attribute):
                    temp_dict[model_attribute].append(element.name)
        # output
        if settings.unit_testing_stage != "TestCategorizeData":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            pathlib.Path(settings.unit_testing_export_path)
            / "categorize"
            / "output"
            / "unit_test.json"
        )
        export.save_dict_as_json(temp_dict, unit_test_file_path)
