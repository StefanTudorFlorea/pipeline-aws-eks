from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
from app.core.utilities import misc
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

settings = Setting()

from app.core.log import Log

log = Log()


class ImposterTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def execute(self) -> None:

        misc.link_objects_to_scene(model, settings.scene_name)

        misc.set_scene(settings.export_directory, settings.misc_image_size)

        geometry.dimension_normalize(model.object)

        material.apply_preview_material(model.object)

        misc.set_rotation_keyframes(model.object, settings.misc_imposter_rotation_steps)

        misc.imposter(
            model.imported_objects,
            settings.misc_imposter_image_size,
            settings.misc_imposter_size,
            settings.misc_imposter_samples,
            settings.export_directory,
        )

        misc.unlink_objects_to_scene(model, settings.scene_name)

    def unit_test(self) -> None:
        self.execute()
