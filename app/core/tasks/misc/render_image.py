"""
Imports data into blender and creates a screenshot for preflight task
"""
import math
import os
import bpy
import numpy as np

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import turntable, deselect_everything
from app.core.utilities import geometry
from app.core.utilities import misc
from pathlib import Path

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class RenderImageTask(AbstractTask):
    def __init__(self, x_axis=True, y_axis=False, z_axis=False):
        super().__init__()
        # log.info('Initialize Compute Device Task')

    def in_blender(self):
        mesh_objects = [
            element
            for element in bpy.data.scenes["Scene"].objects
            if element.type == "MESH"
        ]
        arm_objects = [
            element
            for element in bpy.data.scenes["Scene"].objects
            if element.type != "MESH"
        ]
        for arm in arm_objects:
            deselect_everything()
            arm.select_set(True)
            bpy.ops.object.delete()

        center = mesh_objects[0]

        mesh_objects = bpy.data.scenes["Scene"].objects
        deselect_everything()
        center.select_set(True)
        for i, obj in enumerate(mesh_objects):
            obj.select_set(True)
        bpy.context.view_layer.objects.active = center
        bpy.ops.object.join()

        deselect_everything()
        center.select_set(True)
        bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
        scale = max(center.dimensions)
        bpy.ops.transform.resize(
            value=(1.0 / scale, 1.0 / scale, 1.0 / scale), orient_type="GLOBAL"
        )

        _min, _max = geometry.world_min_max(center)
        bpy.ops.transform.translate(
            value=(-center.location[0], -center.location[1], -_min[2]),
            orient_type="GLOBAL",
        )

        # initial image and turntable
        deselect_everything()
        center.select_set(True)
        res = 1024
        bpy.data.scenes["Scene"].render.engine = "CYCLES"
        bpy.data.worlds["World"].light_settings.use_ambient_occlusion = True
        bpy.context.scene.render.image_settings.color_mode = "RGBA"
        bpy.context.scene.render.film_transparent = True
        bpy.context.scene.render.resolution_x = res
        bpy.context.scene.render.resolution_y = res
        bpy.data.scenes["Scene"].render.resolution_x = res
        bpy.data.scenes["Scene"].render.resolution_y = res
        bpy.data.scenes["Resources"].render.resolution_x = res
        bpy.data.scenes["Resources"].render.resolution_y = res
        bpy.context.scene.use_nodes = True
        # bpy.data.scenes["Scene"].render.engine = 'BLENDER_EEVEE'
        # bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0.13,0.38,0.29,0.1)
        center.lock_rotation = (False, False, False)
        center.rotation_mode = "XYZ"
        steps = 16
        for i in range(0, steps):
            center.rotation_euler[2] = 2.0 * i * math.pi / steps
            bpy.data.scenes["Scene"].render.filepath = (
                str(setting.export_directory) + f"/turntable{i:03}.png"
            )
            bpy.ops.render.render(write_still=True)

    def execute(self) -> None:
        self.in_blender()
        misc.export_gif(setting.export_directory)
        if os.path.exists(str(setting.export_directory) + "/preview.png"):
            os.remove(str(setting.export_directory) + "/turntable000.png")
        else:
            os.rename(
                str(setting.export_directory) + "/turntable000.png",
                str(setting.export_directory) + "/preview.png",
            )
