from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
from app.core.utilities import misc
import time
from pathlib import Path

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class TurntableTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        misc.link_objects_to_scene(model, setting.scene_name)

        misc.set_scene(setting.export_directory, setting.misc_image_size)

        geometry.dimension_normalize(model.object)

        material.apply_preview_material(model.object)

        misc.set_rotation_keyframes(model.object, setting.misc_turntable_rotation_steps)

        misc.turntable(
            model.object,
            Path(setting.import_path) / Path(setting.export_directory),
            model.name,
            setting.misc_turntable_name,
            setting.misc_gif,
        )

        if setting.misc_gif:
            log.info("Execute export gif")

            misc.export_gif(
                setting.export_directory, mask=f"{setting.misc_turntable_name}*.png"
            )

        try:

            misc.unlink_objects_to_scene(model, setting.scene_name)
        except Exception as e:
            pass

    def unit_test(self) -> None:
        self.execute()
