from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry

from app.core.model import Model

model = Model()

from app.core.setting import Setting

settings = Setting()

from app.core.log import Log

log = Log()


class ComputeDeviceTask(AbstractTask):
    def __init__(self, x_axis=True, y_axis=False, z_axis=False):
        super().__init__()
        # log.info('Initialize Compute Device Task')

    def execute(self) -> None:

        compute_device = None
        if settings.misc_device_gpu:
            compute_device = activate_gpu(settings.misc_tilesize_gpu)
        if not compute_device:
            compute_device = activate_cpu(settings.misc_tilesize_cpu)

        msg = "Compute Device:\t{}".format(compute_device)
        log.debug(msg)

    def unit_test(self) -> None:
        self.execute()
