from os import name
from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import misc

from pathlib import Path

from app.core.model import Model

model = Model()

from app.core.setting import Setting

settings = Setting()

from app.core.log import Log

log = Log()


class BabylonEditTask(AbstractTask):
    """
    Related to this issue https://git.rooom.com/platform/frontend/-/issues/333
    Did some research:
    1. Babylon Exporter uses the value from e.g. C.object.data.isPickable
    2. bpy.data.worlds["world"].usePBRMaterials is the Flag for a customMaterial. It should'nt make problems when it is False
    TODO: so we just add this piece of copde to the export_geometry.py at some point to get rid of this task
    """

    def __init__(self):
        super().__init__()

    def poll(self):
        return (Path(settings.export_directory) / f"{model.name}.babylon").exists()

    def execute(self) -> None:

        bjs_file = misc.load_babylon(
            Path(settings.export_directory) / f"{model.name}.babylon"
        )

        for key in bjs_file.keys():
            if key == "customType":
                bjs_file.pop(key, None)
            if key == "pickable":
                bjs_file[key] = True

        for mesh in bjs_file["meshes"]:
            for key in mesh.keys():
                if key == "pickable":
                    mesh[key] = True

        for material in bjs_file["materials"]:
            material.pop("customType", None)

        misc.save_babylon(
            bjs_file, Path(settings.export_directory) / f"{model.name}.babylon"
        )

    def unit_test(self) -> None:
        self.execute()
