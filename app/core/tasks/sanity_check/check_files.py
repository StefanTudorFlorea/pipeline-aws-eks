from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
from app.core.utilities import misc
from app.core.utilities import imports
import time
import os

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class CheckFilesTask(AbstractTask):
    def __init__(self):
        log.info("Initialize Filter Meshes Task")

    def execute(self) -> None:
        log.info("Execute Filter Meshes Task")

        babylon_found = False
        obj_found = False
        glb_found = False
        shadowmap_found = False

        for file in os.listdir(setting.export_directory):
            if file.endswith(".babylon"):
                babylon_found = True
            if file.endswith(".obj"):
                obj_found = True
            if file.endswith(".glb"):
                glb_found = True
            if "shadow" in file:
                shadowmap_found = False

        if setting.export_geometry_as_babylon and not babylon_found:
            setting.sanity_check_success = False
        if setting.export_geometry_as_glb and not glb_found:
            setting.sanity_check_success = False
        if setting.export_geometry_as_obj and not obj_found:
            setting.sanity_check_success = False

        if setting.bake_shadow and not shadowmap_found:
            setting.sanity_check_success = False

        log.info("Task completed")
