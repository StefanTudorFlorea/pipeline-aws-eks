from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
from app.core.utilities import misc
from app.core.utilities import imports
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class CheckOBJTask(AbstractTask):
    def __init__(self):
        log.info("Initialize Filter Meshes Task")

    def execute(self) -> None:
        log.info("Execute Filter Meshes Task")

        data = imports.load_babylon(setting.export_directory)
        if data:
            materials = data["materials"]
            meshes = data["meshes"]

            for material in materials:
                if material["name"] == f"{model.name}.0":
                    log.info(f"Sanity check:\tMaterial {model.name}.0 found")
                if material["name"] == f"{model.name}.MS":
                    log.info(f"Sanity check:\tMaterial {model.name}.MS found")
                if len(model.planes) > 0:
                    if material["name"] == f"{model.name}.MP":
                        log.info(f"Sanity check:\tMaterial {model.name}.MP found")

            for mesh in meshes:
                if mesh["name"] == f"{model.name}":
                    log.info(f"Sanity check:\tMesh {model.name} found")
                if mesh["name"] == f"{model.name}_shadow":
                    log.info(f"Sanity check:\tMesh {model.name} shadow found")

                if len(model.planes) > 0:
                    for idx in range(0, len(model.planes)):
                        if mesh["name"] == f"{model.name}_plane_{idx}":
                            log.info(
                                f"Sanity check:\tMesh {model.name} plane {idx} found"
                            )
                        else:
                            log.error(f"Sanity FAILED:\t Plane {idx} not found")

                if len(model.glass) > 0:
                    for idx in range(0, len(model.glass)):
                        if mesh["name"] == f"{model.name}_glass_{idx}":
                            log.info(
                                f"Sanity check:\tMesh {model.name} glass {idx} found"
                            )
                        else:
                            log.error(f"Sanity FAILED:\t Glass {idx} not found")

        else:
            log.error("Babylon File could not be loaded")

        log.info("Task completed")
