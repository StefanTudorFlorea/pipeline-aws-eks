from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
from app.core.utilities import misc
from app.core.utilities import imports
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class CheckBabylonTask(AbstractTask):
    def __init__(self):
        log.info("Initialize Filter Meshes Task")

    def execute(self) -> None:
        log.info("Execute Filter Meshes Task")

        data = imports.load_babylon(setting.export_directory)
        if data:
            materials = data["materials"]
            meshes = data["meshes"]

            sanity_material_model = False
            sanity_material_shadow = False
            sanity_material_plane = False

            sanity_mesh_model = False
            sanity_mesh_shadow = False
            sanity_mesh_planes = False
            sanity_mesh_glass = False
            sanity_mesh_labels = False

            planes = 0

            for material in materials:
                if material["name"] == f"{model.name}.0":
                    sanity_material_model = True

                if material["name"] == f"{model.name}.MS":
                    sanity_material_shadow = True

                if len(model.planes) > 0:
                    if material["name"] == f"{model.name}.MP":
                        planes += 1
                    if planes == len(model.planes):
                        sanity_material_plane = True

            for mesh in meshes:
                if mesh["name"] == f"{model.name}":
                    sanity_mesh_model = True

                if mesh["name"] == f"{model.name}_shadow":
                    sanity_mesh_shadow = True

                if len(model.planes) > 0:
                    planes = 0
                    for idx in range(0, len(model.planes)):
                        if mesh["name"] == f"{model.name}_plane_{idx}":
                            log.info(
                                f"Sanity check:\tMesh {model.name} plane {idx} found"
                            )
                            planes += 1
                    if planes == len(model.planes):
                        sanity_mesh_planes = True

                if len(model.glass) > 0:
                    glass = 0
                    for idx in range(0, len(model.glass)):
                        if mesh["name"] == f"{model.name}_glass_{idx}":
                            log.info(
                                f"Sanity check:\tMesh {model.name} glass {idx} found"
                            )
                            glass += 1
                    if glass == len(model.glass):
                        sanity_mesh_glass = True

                if len(model.labels) > 0:
                    labels = 0
                    for idx in range(0, len(model.labels)):
                        if mesh["name"] == f"{model.name}_label_{idx}":
                            log.info(
                                f"Sanity check:\tMesh {model.name} label {idx} found"
                            )
                            labels += 1
                    if labels == len(model.labels):
                        sanity_mesh_labels = True

            # Check sanity booleans
            if sanity_material_model:
                log.info(f"Sanity check:\tMaterial {model.name}.0 found")
            else:
                setting.sanity_check_success = False
                log.error(f"FAILED Sanity check:\tMaterial not found")

            if sanity_material_shadow:
                log.info(f"Sanity check:\tMaterial {model.name}.MS found")
            else:
                setting.sanity_check_success = False
                log.error(f"FAILED Sanity check:\tMaterial not found")

            if len(model.planes) > 0:
                if sanity_material_plane:
                    log.info(f"Sanity check:\tMaterial {model.name}.MP found")
                else:
                    setting.sanity_check_success = False
                    log.error(f"FAILED Sanity check:\tMaterial not found")

            if sanity_mesh_model:
                log.info(f"Sanity check:\tMesh {model.name} found")
            else:
                setting.sanity_check_success = False
                log.error(f"FAILED Sanity check:\tMesh model {model.name} not found")

            if sanity_mesh_shadow:
                log.info(f"Sanity check:\tMesh shadow {model.name} found")
            else:
                setting.sanity_check_success = False
                log.error(f"FAILED Sanity check:\tMesh shadow {model.name} not found")

            if len(model.planes) > 0:
                if sanity_mesh_planes:
                    log.info(f"Sanity check:\tMesh {model.name} planes found")
                else:
                    setting.sanity_check_success = False
                    log.error(f"FAILED Sanity check:\tplanes uncomplete")

            if len(model.glass) > 0:
                if sanity_mesh_glass:
                    log.info(f"Sanity check:\tMesh {model.name} glass found")
                else:
                    setting.sanity_check_success = False
                    log.error(f"FAILED Sanity check:\tglass uncomplete")

            if len(model.labels) > 0:
                if sanity_mesh_labels:
                    log.info(f"Sanity check:\tMesh {model.name} labels found")
                else:
                    setting.sanity_check_success = False
                    log.error(f"FAILED Sanity check:\tlabels uncomplete")

        else:
            setting.sanity_check_success = False
            log.error("Babylon File could not be loaded")

        log.info("Task completed")
