import math
import os

import bpy
from bpy import context as C
from bpy import data as D
from bpy import ops as O


from app.core.abstract.abstract_task import AbstractTask

from app.core.utilities.material import (
    pack,
    try_creating_material_from_image_list,
)
from app.core.utilities.misc import deselect_everything, activate_gpu


from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
settings = Setting()
log = Log()


class MergeMaterialsTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if model.object is None:
            return False
        return len(model.object.data.materials) > 1

    def execute(self) -> None:
        log.info(f"Joining {len(model.object.data.materials)} materials")
        deselect_everything()

        # Copy the input
        log.info("Create duplicate")
        out_model = model.object
        out_model.select_set(True)
        O.object.duplicate()
        out_model.select_set(False)
        in_model = C.selected_objects[0]
        log.info(f"In: {in_model}")
        log.info(f"Out: {out_model}")

        # Create UV from Packmaster
        log.info("Execute UV Packmaster")
        deselect_everything()
        # self.pack(in_model)
        pack(
            [out_model],
            island_margin=16,
            border_padding=0,
            texture_resolution=4096,
        )

        # Remove all materials in output
        for i in range(len(out_model.data.materials)):
            out_model.data.materials.pop()
        out_model.data.materials.append(D.materials.new(name="dummy"))

        # Bake settings selected into target object
        deselect_everything()
        log.info("Setup Simple Bake")
        C.view_layer.objects.active = in_model
        in_model.select_set(True)
        C.scene.simplebake_advancedobjectselection = False
        C.scene.selected_s2a = True
        C.scene.targetobj = out_model
        # bound_box: 8x3 values
        #   LoX, LoY, LoZ,
        #   LoX, LoY, HiZ,
        #   LoX, HiY, HiZ,
        #   LoX, HiY, LoZ,
        #   HiX, LoY, LoZ,
        #   HiX, LoY, HiZ,
        #   HiX, HiY, HiZ,
        #   HiX, HiY, LoZ

        # use utilities world min,max
        bb = in_model.bound_box
        dX = (bb[0][0] - bb[4][0]) ** 2
        dY = (bb[0][1] - bb[2][1]) ** 2
        dZ = (bb[0][2] - bb[1][2]) ** 2
        diag = math.sqrt(dX + dY + dZ)
        C.scene.ray_distance = 0.1 * diag
        log.info(f"Setting ray distance to {diag}")

        C.scene.selected_col = True
        C.scene.selected_metal = True
        C.scene.selected_rough = True
        C.scene.selected_normal = True
        log.info(f"Setting resolution to '{settings.import_bake_resolution}'")
        C.scene.texture_res = settings.import_bake_resolution
        C.scene.output_res = settings.import_bake_resolution
        C.scene.render.bake.margin = 16
        C.scene.restoreOrigUVmap = False
        # ['fg', 'bg']
        C.scene.bgbake = "fg"

        # if settings.device_gpu:
        if True:
            activate_gpu()
        O.object.simple_bake_mapbake()

        # Get images
        log.info("Getting images")
        images = {
            "diffuse": None,
            "metalness": None,
            "roughness": None,
            "normal": None,
            "reflectivity": None,
            "emission": None,
            "displacement": None,
        }
        # TODO: remove hard-coded parts?
        # utilites.imports.import_file
        prefix = out_model.name + "_Bake1_pbrs2a_"
        log.info(prefix)
        for img in D.images:
            log.info(img.name)
            if not img.name.startswith(prefix):
                continue
            img_key = img.name.split("_")[-1]
            log.debug(f"Produced {img_key}")
            if img_key in images:
                images[img_key] = img
        to_drop = []
        for k, v in images.items():
            if v is None:
                log.warning(f"Image for '{k}' not created")
                to_drop.append(k)
                continue
            v.filepath = os.path.join(
                os.path.realpath("."),
                settings.export_directory,
                # f'MaterialModel{res}_{k}.jpg'
                f"Model_{k}.jpg",
            )
            v.file_format = "JPEG"
            # v.save()
        for k in to_drop:
            del images[k]
        log.info(images.keys())

        # Create combined material
        new_mat = try_creating_material_from_image_list(images.values())
        if new_mat:
            log.info("Created new material")
            model.materials.clear()
            model.materials.add(new_mat)
            out_model.data.materials.pop()
            out_model.data.materials.append(new_mat)
            new_mat.name = "Model.0"
            for area in C.screen.areas:
                if area.type == "VIEW_3D":
                    for space in area.spaces:
                        if space.type == "VIEW_3D":
                            space.shading.type = "MATERIAL"
            deselect_everything()
            in_model.select_set(True)
            O.object.delete()
        else:
            log.warning("Merge materials not successful")

        C.view_layer.objects.active = model.object
        model.object.select_set(True)

        log.info(f"{model.materials}")
        log.info(f"{model.texture_format}")

    def unit_test(self) -> None:
        self.execute()
