from distutils.log import debug
from pathlib import Path
from pydoc import resolve

from numpy import absolute
import bpy

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.material import (
    unwrap,
    bake_shadow_or_shadowmap,
    replace_material_with_rooom_material,
    restore_material,
    delete_an_object,
    attach_imgage_to_node,
    setup_shadowplan,
)
from app.core.utilities import export
from app.core.utilities.blender import (
    print_scene_data,  # for testing purpose
    setup_compositing,
    render
)

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log
model = Model()
setting = Setting()
log = Log()


class BakeShadowForMeshTask(AbstractTask):
    """The shadow on the object"""

    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if model.shadow_plane:
            raise Exception("shadow plane exists!")
        if not model.object:
            raise Exception("model.object is not defined!")
        return True

    def activate_an_uv_layer(self):
        if model.object.data.uv_layers:
            uv_length = len(model.object.data.uv_layers)
            log.info(f" {uv_length} UV layers are found")
            if uv_length == 1:
                model.object.data.uv_layers[0].active = True
                log.info(
                    f"first uv layer is active. name is {model.object.data.uv_layers.active.name}"
                )
            elif uv_length > 1:
                model.object.data.uv_layers[1].active = True
                log.info(
                    f"second uv layer is active. name is {model.object.data.uv_layers.active.name}"
                )
        else:
            log.info(f"NO uv layer is found. Create one")
            unwrap(model.object)

    def create_baked_shadowmap(self, image_type:str):
        '''
        image_type = shadowmap or shadowmap_squezze

        '''
        # this attaches and creates images!!!!
        result_node = attach_imgage_to_node(
            "shadow_shader_V1", f"{model.object.name}_{image_type}",
            512, 512
            # -> shadow_shader_V1 // shadowmap_shader
        )

        if result_node:
            model.object.select_set(True)
            log.info(f"model object is selected")
            self.activate_an_uv_layer()
            bake = bake_shadow_or_shadowmap(
            model, image_type, bake_shadowmap=True, file_extension=".jpg"
            )
            for area in bpy.context.screen.areas:
                if area.type == "NODE_EDITOR":
                    bpy.ops.node.delete()

            return bake
        log.error(f"Could not image {model.object.name}_{image_type} to node. Baking failed")
        # raise Exception(
        #     f"Could not assigne image {model.object.name}_{image_type} to node"
        # )
        return result_node

    def execute(self) -> None:
        model.shadowmap_plane = setup_shadowplan("shadowmap", 5)

        print_scene_data()

        # remember and replace all the slots
        shadowmap_shader = bpy.data.materials.get("shadow_shader_V1")
        log.info(f"Replace existing materials with rooom's shadowmap shader")

        all_objects = [model.object]
        all_objects.extend(
            [
                sub_object
                for sub_object in (model.glass + model.planes)
                if sub_object
            ]
        )

        material_dictionary = replace_material_with_rooom_material(
            all_objects, shadowmap_shader
        )

        for object in model.glass:
            object.cycles_visibility.shadow = (
                setting.bake_user_defined_glass_shadow
            )  # False at this moment.

        for object in model.planes:
            object.cycles_visibility.shadow = (
                setting.bake_user_defined_plane_shadow
            )  # False at this moment.
        #backe 1
        is_shadow_image1_baked = self.create_baked_shadowmap("shadowmap")

        # squeeze
        model.object.scale[2] = 0.1
        #back 2
        is_shadow_image_squeeze_baked = self.create_baked_shadowmap("shadowmap_squeeze")

        # unsqueeze
        model.object.scale[2] = 1

        # render_result = None
        bake_result = False
        if is_shadow_image1_baked and is_shadow_image_squeeze_baked:
            log.info(f"shadow images were baked")
            log.debug(f"{bpy.data.images[ model.object.name+'_shadowmap']}")
            log.debug(f"{bpy.data.images[ model.object.name+'_shadowmap_squeeze']}")

            bake_result = True
            #setup compositing with shadwo images and render
            setup_compositing('shadowmap',model)
            filepath = str(Path(f"{setting.export_directory}/{model.object.name}_shadowmap.jpg").absolute().resolve())
            render_result = render(filepath)

        else:
            raise Exception(
                f"Could not attach image {model.object.name}_shadowmap to node."
            )

        if bake_result:
            log.info(
                f"Shadow for mesh is rendered. The name is {render_result.name}. The filepath is {render_result.filepath_raw}"
            )
            log.info(
                "Shadow for mesh is successfully rendered. Restore the material back."
            )
            model.append("shadowmap", render_result)
            restore_material_result = restore_material(material_dictionary)
            if restore_material_result:
                log.info("Materials are restored.")
            else:
                log.info("Materials cannot be restored.")
        else:
            pass
            raise Exception("Error occurred during baking!")

    def unit_test(self):
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        for img in bpy.data.images:
            log.info(img)
            if "shadowmap" in img.name:
                log.info(f"MY MODEL SHADOW MAP is {img.name}")
        temp_dict["shadowmap"] = model.shadowmap[0].name

        # Export Unit Test Data
        if setting.unit_testing_stage != "TestBakeShadowForMesh":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "bake_shadowmap"
            / "output"
            / "unit_test.json"
        )
        export.save_dict_as_json(temp_dict, unit_test_file_path)
