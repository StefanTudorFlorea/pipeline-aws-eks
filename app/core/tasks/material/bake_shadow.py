import bpy
from pathlib import Path

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.material import (
    attach_imgage_to_node,
    bake_shadow_or_shadowmap,
    setup_shadowplan,
)
from app.core.utilities.export import save_dict_as_json

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class BakeShadowForGroundPlaneTask(AbstractTask):
    """The mesh project to the shadow ground plane"""

    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if not model.object:
            raise Exception("model.object is not defined!")
        return True

    def create_ground_plane_bake_material(self):
        bpy.context.view_layer.objects.active = model.shadow_plane
        log.info(
            f"The active object is {bpy.context.view_layer.objects.active.name}"
        )
        model.object.select_set(True)

        for object in bpy.context.selected_objects:
            if object.name != model.object.name and "shadow" not in object.name:
                object.select_set(False)
                log.info(
                    f"{object.name} is not the main object and not the shadow plane. I deselect it!"
                )

        shadow_shader = bpy.data.materials.get("shadow_shader_V1")

        if model.shadow_plane.data.materials:
            model.shadow_plane.data.materials.pop()
            log.info("remove ground plane's original material")
            model.shadow_plane.data.materials[0] = shadow_shader
        else:
            model.shadow_plane.data.materials.append(shadow_shader)
            log.info(f"Append a shadow to the shadow_plane")

        if model.shadow_plane.data.materials[shadow_shader.name]:
            result_node = attach_imgage_to_node(
                "shadow_shader_V1", f"{model.object.name}_shadow"
            )
            if result_node:
                log.info(f"shadow_shader_V1 is updated")
            else:
                log.error(
                    f"Could not attach image {model.object.name}_shadow to node."
                )
        else:
            # It is basically not possible to happen
            log.info(f"shadow material is not appended to shadow_plane")

    def replace_and_rename_shadow_material(self):
        model.shadow_plane.data.materials[0] = bpy.data.materials[
            "M.0"
        ]  # the default material
        model.shadow_plane.data.materials[0].name = "M.MS"
        bpy.data.materials["M.MS"].node_tree.nodes["Diffuse BSDF"].inputs[
            0
        ].default_value = (0.8, 0.8, 0.8, 0.8)

    def execute(self) -> None:

        # TODO: design something to remove the original shadow plane when re-baking.
        model.shadow_plane = setup_shadowplan(
            "shadow", setting.bake_shadow_margin
        )
        self.create_ground_plane_bake_material()
        log.info(f"{model.shadow_plane} is created for shadow baking ")

        for object in model.glass:
            log.info(
                f"set the glass object's shadow to {setting.bake_user_defined_glass_shadow}"
            )
            object.cycles_visibility.shadow = (
                setting.bake_user_defined_glass_shadow
            )
        bake_result = bake_shadow_or_shadowmap(
            model, bake_shadowmap=False, file_extension=".jpg"
        )

        if bake_result:
            model.append("shadow", bake_result)
            log.info(
                f"Shadow is baked. The name is {bake_result.name}. The filepath is {bake_result.filepath_raw}. The filepath is {bake_result.filepath}"
            )
            self.replace_and_rename_shadow_material()
            log.info(
                f"Replace the material with default material and renamed as M.MS"
            )
        else:
            log.info(f"Shadow cannot be baked")

        # Turn back on the deselected objects from the create_ground_plane_bake_material
        for object in bpy.data.objects:
            if model.object.name in object.name:
                if object.visible_get():
                    object.select_set(True)

    def unit_test(self) -> None:
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        temp_dict["shadow"] = model.shadow[0].name

        # Export Unit Test Data
        if setting.unit_testing_stage != "TestBakeShadowForGroundPlane":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "bake_shadow"
            / "output"
            / "unit_test.json"
        )
        save_dict_as_json(temp_dict, unit_test_file_path)
        # save_dict_as_json(temp_dict, setting.unit_testing_export_path)
