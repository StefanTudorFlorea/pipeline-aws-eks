from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import material
from app.core.utilities.misc import deselect_everything

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
settings = Setting()
log = Log()


class NormalizeUVLayerTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if model is None:
            return False
        if model.object is None:
            return False
        if model.object.data is None:
            return False
        if len(model.object.data.uv_layers) == 0:
            return False
        return True

    def execute(self) -> None:
        if model.object.data.uv_layers.active is None:
            model.object.data.uv_layers.active = model.object.data.uv_layers[0]
        material.normalize_uv_layer(model, settings.export_directory)

        log.info("Execute Job deselect_everything")

        deselect_everything()

    def unit_test(self) -> None:
        self.execute()
