import bpy
from bpy import context as C
from bpy import ops as O
from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import deselect_everything
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class ResizeTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        return bool(model.object)

    def execute(self) -> None:
        log.warning("Task is broken")
        return
        model.object.select_set(True)
        for component in [*model.planes, *model.glass, *model.labels]:
            component.select_set(True)
        C.view_layer.objects.active = model.object
        O.transform.resize(value=(value, value, value))
        O.object.transform_apply(location=False, rotation=True, scale=True)
        deselect_everything()

    def unit_test(self) -> None:
        self.execute()
