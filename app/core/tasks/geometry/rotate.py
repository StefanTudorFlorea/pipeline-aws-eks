from pathlib import Path
from mathutils import Euler
from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry, export

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
settings = Setting()
log = Log()


class RotateTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        # make sure all geometries are parented to the object but not the shadow catcher -> in future the shadow catcher shall be unparented
        all_geometries_parented = all(
            model.object is element.parent
            for element in [*model.planes, *model.labels, *model.glass]
        )
        shadow_catcher_already_parented = (
            model.object is model.shadow_plane.parent
            if model.shadow_plane
            else False
        )
        return all_geometries_parented and not shadow_catcher_already_parented

    def execute(self) -> None:
        log.info("set context rotation mode as xyz")
        # make sure using the main object
        model.object.rotation_mode = "XYZ"

        # Keep in mind, the users system is in WebGL, so axes needs to be switched!
        check_state = geometry.rotate(
            model.object,
            x=settings.geometry_rotate_x_value,
            y=settings.geometry_rotate_z_value,
            z=settings.geometry_rotate_y_value,
        )
        # the rotation of the parent object needs to be applyed,
        # bacause the blender babylonjs exporter cannot handle
        # inverse matrix for child objects
        geometry.apply_rotation(model.object)

        # move the object back to hit the ground
        # for calculating the new origin also, take child objects into account
        objects_for_recentering = geometry.getChildren(model.object)
        geometry.recenter(objects_for_recentering)

        if check_state:
            log.info("We confirm the rotation has been applied")
        else:
            log.info("Wrong rotation values, try again!")

    def unit_test(self) -> None:
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        temp_dict["setting_x_value"] = float(settings.geometry_rotate_x_value)
        temp_dict["setting_y_value"] = float(settings.geometry_rotate_y_value)
        temp_dict["setting_z_value"] = float(settings.geometry_rotate_z_value)
        # get the rotation result
        temp_dict["rotated_x_value"] = model.object.rotation_euler[0]
        temp_dict["rotated_y_value"] = model.object.rotation_euler[1]
        temp_dict["rotated_z_value"] = model.object.rotation_euler[2]

        if settings.unit_testing_stage != "TestRotate":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(settings.unit_testing_export_path)
            / "rotate"
            / "output"
            / "unit_test.json"
        )
        print(f"unit test file path is {unit_test_file_path}")
        export.save_dict_as_json(temp_dict, unit_test_file_path)
