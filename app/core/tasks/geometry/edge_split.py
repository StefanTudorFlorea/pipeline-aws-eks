from bpy import ops as O
from bpy import context as C

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import deselect_everything
from math import radians

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class EdgeSplitTask(AbstractTask):
    """Duplicates Edge which are sharp and split the mesh"""

    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        deselect_everything()
        model.object.select_set(True)
        C.view_layer.objects.active = model.object
        O.object.modifier_add(type="EDGE_SPLIT")
        C.object.modifiers["EdgeSplit"].split_angle = radians(
            setting.geometry_edge_split_angle
        )
        O.object.modifier_apply(modifier="EdgeSplit")
        deselect_everything()

    def unit_test(self) -> None:
        self.execute()
