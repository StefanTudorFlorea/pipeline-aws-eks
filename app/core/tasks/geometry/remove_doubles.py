import bmesh

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class RemoveDoublesTask(AbstractTask):
    """
    Merging vertices by distance to remove non-manifold geometry
            https://blender.stackexchange.com/questions/7910/what-is-non-manifold-geometry
        https://docs.blender.org/api/current/O.mesh.html?highlight=bpy%20ops%20mesh%20remove_doubles#O.mesh.remove_doubles
    """

    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        bm = bmesh.new()
        bm.from_mesh(model.object.data)
        bmesh.ops.remove_doubles(
            bm, verts=bm.verts, dist=setting.geometry_remove_distance
        )
        bm.to_mesh(model.object.data)

    def unit_test(self) -> None:
        self.execute()
