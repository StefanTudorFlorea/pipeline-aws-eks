from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.geometry import check_if_uv_coords_are_normalized

import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class ValidateUVsTask(AbstractTask):
    """
    Checks if uvs are in bounds of 0 to 1
    In future:
    - checks if shadow map uvs are not overlapping
    - checks if all uvs are using an ok amount of area
    """

    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if model is None:
            return False
        if model.object is None:
            return False
        if model.object.data is None:
            return False
        if len(model.object.data.uv_layers) == 0:
            return False
        return True

    def execute(self) -> None:
        check_if_uv_coords_are_normalized(model.object)

    def unit_test(self) -> None:
        self.execute()
