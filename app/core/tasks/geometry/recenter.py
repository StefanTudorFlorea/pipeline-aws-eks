import bpy
from bpy import ops as O
from mathutils import Vector

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import deselect_everything
from app.core.utilities.geometry import world_min_max

from app.core.model import Model

model = Model()

from app.core.setting import Setting

settings = Setting()

from app.core.log import Log

log = Log()


class RecenterTask(AbstractTask):
    """Position the mesh on x-y median and z=0"""

    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        """
        deselect_everything()
        model.object.select_set(True)
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
        _min, _max = world_min_max(center)
        bpy.ops.transform.translate(value=(-model.object.location[0], -model.object.location[1], -_min[2]), orient_type='GLOBAL')
        """
        minimum_corner, maximum_corner = world_min_max(model.object)
        deselect_everything()
        model.object.select_set(True)
        model.object.location = model.object.location - Vector(
            (
                (minimum_corner.x + maximum_corner.x) / 2.0,
                (minimum_corner.y + maximum_corner.y) / 2.0,
                minimum_corner.z,
            )
        )
        O.object.transform_apply()
        deselect_everything()

    def unit_test(self) -> None:
        self.execute()
