import bmesh

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import deselect_everything

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class RecalculateFaceNormalsTask(AbstractTask):
    """Recalculating face normals so they may face towards the outside of the mesh hull"""

    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        bm = bmesh.new()
        bm.from_mesh(model.object.data)
        bmesh.ops.recalc_face_normals(bm, faces=bm.faces)
        bm.to_mesh(model.object.data)

    def unit_test(self) -> None:
        self.execute()
