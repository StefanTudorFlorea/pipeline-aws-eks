from bpy import ops as O

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities.misc import deselect_everything

from app.core.model import Model

model = Model()

from app.core.setting import Setting

settings = Setting()

from app.core.log import Log

log = Log()


class ShadeSmoothTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        deselect_everything()
        model.object.data.use_auto_smooth = False
        model.object.select_set(True)
        O.object.shade_smooth()
        deselect_everything()

    def unit_test(self) -> None:
        self.execute()
