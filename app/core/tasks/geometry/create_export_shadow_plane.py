from app.core.abstract.abstract_task import AbstractTask
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()

from app.core.utilities.geometry import world_min_max, create_ground_plane, parent
from app.core.utilities.blender import print_scene_data


class CreateExportShadowPlaneTask(AbstractTask):
    """Creates a plane underneath the object that matches the pre-baked shadow for export purposes"""

    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        return True

    def execute(self) -> None:
        target_object = model.object
        minimum_corner, maximum_corner = world_min_max(target_object)
        plane = create_ground_plane(
            minimum_corner, maximum_corner, setting.bake_shadow_margin
        )
        parent(plane, target_object)
        plane["category"] = "shadow"
        plane.name = target_object.name + "_shadow"
        model.shadow_plane = plane

    def unit_test(self) -> None:
        self.execute()
