from bpy import ops as O

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import geometry
from app.core.utilities import material
import time

from app.core.model import Model
from app.core.utilities.misc import deselect_everything

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class CompactTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        return len(model.object.data.polygons) > setting.geometry_polygon_target

    def execute(self) -> None:
        # high_poly = geometry.decimate(model.object, setting.geometry_polygon_target)
        # high_poly["lowpoly"] = model.object
        # model.object["highpoly"] = high_poly
        geometry.decimate(model.object, setting.geometry_polygon_target)

        log.info("Execute function remove_all_uv_layers")

        material.remove_all_uv_layers(model.object)

        log.info("Execute function unwrap")

        material.unwrap(model.object)

        """
        log.info("Execute function join_geometry")

        model.object = geometry.compact(
            model.object,
            high_poly,
            setting.misc_samples,
            setting.bake_margin,
            setting.max_cage_offset,
            setting.misc_tilesize,
        )
        """
        deselect_everything()
        # high_poly.select_set(True)
        # O.object.delete()

    def unit_test(self) -> None:
        self.execute()
