import json

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import export
from pathlib import Path


from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

setting = Setting()
model = Model()
log = Log()

class ExportPolyCountTask(AbstractTask):
    def __init__(self):
        super().__init__()
        # log.info('Initialize Export Polycount Task')

    def poll(self):
        if len(model.imported_objects) == 0:
            raise Exception("model.imported_object is null!")
        return True

    def execute(self) -> None:
        # log.info('Execute Export Polycount Task')

        meta_data = {"polycount": 0}
        for obj in model.imported_objects:
            if obj.type.lower() == "mesh":
                try:
                    meta_data["polycount"] += len(obj.data.polygons)
                except:
                    raise Exception

        json_filepath = str(Path(setting.export_directory) / "polycount.json")

        export.save_dict_as_json(meta_data, json_filepath)

    def unit_test(self) -> None:
        """unit test"""
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        export_folder = (
            Path(setting.unit_testing_export_path)
            / "export_polycount"
            / "output"
        )

        polycount_json = False
        for file in Path(export_folder).iterdir():
            if Path(file).name == "polycount.json":
                polycount_json = True
                temp_dict["json"] = True

        if polycount_json:
            polycount_output=json.load(open(Path(export_folder)/"polycount.json"))
            temp_dict["polycount"] = polycount_output["polycount"]
        else:
            temp_dict["json"] = False
        # Export Unit Test Data
        if setting.unit_testing_stage != "TestExportPolyCount":
            return False

        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "export_polycount"
            / "output"
            / "unit_test.json"
        )
        export.save_dict_as_json(temp_dict, unit_test_file_path)
