import shutil
import bpy

from pathlib import Path

import app.core.utilities.keywords as keywords
from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import export, misc
from app.core.utilities.export import export_image_to_filepath
from app.core.utilities.material import create_image_from_separate_channels

from app.core.model import Model
from app.core.setting import Setting
from app.core.log import Log

model = Model()
setting = Setting()
log = Log()


class ExportTexturesTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if not model.name:  # name is not defined
            log.info("no model name")
            return False
        if not setting.export_directory:  # export_directory is not defined
            log.info("no export_directory")
            return False
        if (
            not model.object
        ):  # In order to export properly to rooom there must be one single object!
            log.info("no object")
            return False
        if (
            not model.object.data.materials
        ):  # Object has no material to export textures from!
            log.info("no model materials")
            return False

    def export_img(self, img, category: str, model: Model, ext):
        """Exports a given image with its categroy and extension to the model output directory

        Args:
            img (_type_): The model.image to be exported
            category (str): the category of the image. e.g. baseColor, shadow,....
            model (Model): the model to export
            ext (_type_): the file extension. Only jpg, jpeg or png are supported
        """
        export_path = Path(
            setting.export_directory / f"{model.object.name}_{category}.{ext}"
        ).resolve()
        if category == "reflectivity":
            img_roughness = None
            img_metallness = None
            if len(model.roughness) > 0:
                img_roughness = model.roughness[0]
                img = img_roughness
            if len(model.metallic) > 0:
                img_metallness = model.metallic[0]
                img = img_metallness

            if (
                img_roughness
                and img_metallness
                and len(model.reflectivity) == 0
            ):
                log.info("Create combined reflectivity")
                reflectivity = create_image_from_separate_channels(
                    filepath=export_path.as_posix(),
                    green=img_roughness,
                    blue=img_metallness,
                    #resolution=setting.texture_resolution,
                )
                reflectivity.name = f"{model.object.name}_{category}"
                reflectivity["category"] = category
                img = reflectivity
            elif len(model.reflectivity) > 0:
                log.info(
                    f"Reflectivity image found. Export it without combination"
                )
            else:
                log.info(
                    f"Found only {'roughness' if img_roughness else 'metallic'}. I will rename it as reflectivity"
                )

        export_image_to_filepath(
            img, model.object.name, export_path.as_posix(), category
        )

    def execute(self) -> None:
        """pipeline trigger"""
        # TODO: check the output file name as the
        export_name = [
            Path(x)
            for x in setting.file_paths
            if Path(x).suffix in keywords.supported_geometries
        ][0].stem
        model.object.name = export_name

        if len(model.diffuse) == 1:
            img = model.diffuse[0]
            if isinstance(img, Path):
                # for unpacked images
                ext = img.suffix
                the_export_name = model.object.name
                export_file = Path(
                    setting.export_directory
                    / f"{the_export_name}_baseColor.{ext}"
                ).resolve()
                shutil.copy(img, export_file)
            else:
                # for packed images
                log.info(f"packed img is {img}")
                self.export_img(img, "baseColor", model, setting.texture_format)
        elif len(model.diffuse) > 1:
            """TODO
            If there are cases with more than 1 diffuse in the array of model.diffuse.
            Make sure that the pipeline is not crashed.
            For now, we assume the first one is the diffuse.
            Later, there will be functions for texture merging.
            """
            log.info(
                "more than one image in the array of diffuse texture. Take the first one"
            )
            img = model.diffuse[0]
            self.export_img(img, "baseColor", model, setting.texture_format)

        if len(model.normal) == 1:
            img = model.normal[0]
            self.export_img(img, "normal", model, setting.normal_format)
        elif len(model.normal) > 1:
            log.info(
                "more than one image in the array of normal texture. Take the first one"
            )
            img = model.normal[0]
            self.export_img(img, "normal", model, setting.normal_format)

        if len(model.emission) == 1:
            img = model.emission[0]
            self.export_img(img, "emission", model, setting.texture_format)
        elif len(model.emission) > 1:
            log.info(
                "more than one image in the array of emission texture. Take the first one"
            )
            img = model.emission[0]
            self.export_img(img, "emission", model, setting.texture_format)

        if len(model.shadow) == 1:
            img = model.shadow[0]
            raw_image_name = img.name
            new_image_name = f"{model.object.name}_shadow"
            bpy.data.images[raw_image_name].name = new_image_name
            self.export_img(
                bpy.data.images[new_image_name], "shadow", model, "jpg"
            )

        if len(model.shadowmap) == 1:
            img = model.shadowmap[0]
            raw_image_name = img.name
            new_image_name = f"{model.object.name}_shadowmap"
            bpy.data.images[raw_image_name].name = new_image_name
            self.export_img(
                bpy.data.images[new_image_name], "shadowmap", model, "jpg"
            )
        if (
            len(model.roughness) == 1
            or len(model.metallic) == 1
            and len(model.reflectivity) == 0
        ):
            log.info(f"found roughness or metallic. Combine!")
            img = None
            self.export_img(
                img, "reflectivity", model, setting.reflectivity_format
            )
        elif len(model.reflectivity) == 1:
            img = model.reflectivity[0]
            self.export_img(
                img, "reflectivity", model, setting.reflectivity_format
            )
        else:
            log.info(f"No metallic or roughness")

    def unit_test(self) -> None:
        """unit test"""
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        export_folder = (
            Path(setting.unit_testing_export_path)
            / "export_textures"
            / "output"
        )
        temp_dict["files"] = []
        for file in Path(export_folder).iterdir():
            if Path(file).suffix in keywords.supported_textures:
                file_name = str(Path(file).stem)
                temp_dict["files"].append(file_name)

        # Export Unit Test Data
        if setting.unit_testing_stage != "TestExportTextures":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "export_textures"
            / "output"
            / "unit_test.json"
        )
        export.save_dict_as_json(temp_dict, unit_test_file_path)
