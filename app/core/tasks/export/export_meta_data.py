import json

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import export, misc
from pathlib import Path
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class ExportMetaDataTask(AbstractTask):
    META_DATA_FILE = "Model_info.json"

    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        meta_data = misc.mesh_to_dict(model)

        if setting.export_meta_data_path:
            path = Path(setting.export_meta_data_path)
            if path.name != 'Model_info.json':
                log.warning(f"Meta data should be exported to '{self.META_DATA_FILE}' not '{path.name}'")
                path = path.parent/self.META_DATA_FILE
        else:
            path = Path(setting.export_directory) / self.META_DATA_FILE
        export.save_dict_as_json(meta_data, str(path))

    def unit_test(self) -> None:
        temp_dict = {}
        executed = False
        try:
            self.execute()
            temp_dict['exported'] = misc.mesh_to_dict(model)
            temp_dict['reloaded'] = json.load(open(setting.export_meta_data_path, 'rt'))
            executed = True
        except Exception as e:
            log.error(f"Unit test failed: {e}")

        # Export Unit Test Data
        temp_dict[str(self.__class__.__name__)] = executed
        export.save_dict_as_json(temp_dict, setting.unit_testing_export_path + "/unit_test.json")

