"""
What is the purpose of this script?
"""

from app.core.abstract.abstract_task import AbstractTask

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class ValidateModelDataTask(AbstractTask):
    def execute(self) -> None:
        probably_invalid = model.baked_shadow.pixels[0:3] == (0.0, 0.0, 0.0)
        if probably_invalid:
            log.debug(
                "The baked shadow is probably invalid because \
                 the first pixel is completely black!"
            )

    def unit_test(self) -> None:
        self.execute()
