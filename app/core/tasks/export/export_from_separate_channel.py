from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import export, misc, material
import time

from app.core.model import Model

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()


class ExportFromSeparateChannelTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def execute(self) -> None:
        material.create_image_from_separate_channels(setting.export_directory)

        # log.info("Link Objects")
        # misc.unlink_objects_to_scene(model, setting.scene_name)

    def unit_test(self) -> None:
        self.execute()
