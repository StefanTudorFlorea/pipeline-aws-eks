from shutil import rmtree
from pathlib import Path
import bpy
from bpy import context as C
from bpy import ops as O
from bpy import data as D

from app.core.abstract.abstract_task import AbstractTask
from app.core.utilities import export, misc
from app.core.utilities.misc import (
    unlink_all_objects_from_current_scene,
    link_model_to_current_scene,
    overwrite_mtl,
    link_all_objects_to_scene,
)
from app.core.utilities.export import convert_obj_material, RGBMaterial

import time

from app.core.model import Model
import app.core.utilities.keywords as keywords

model = Model()

from app.core.setting import Setting

setting = Setting()

from app.core.log import Log

log = Log()

# need to remove
from os import unlink
import os
from os.path import isfile, isdir

#####


class ExportGeometryTask(AbstractTask):
    def __init__(self):
        super().__init__()

    def poll(self) -> bool:
        if model.object:
            return True
        if len(model.imported_objects) > 1:
            return False

    def export_obj(self):
        log.info("Execute Export Geometry as OBJ")
        if setting.re_bake:
            for obj in model.imported_objects:
                obj.select_set(True)

            if (Path(setting.export_directory) / f"{model.name}.obj").exists():
                (Path(setting.export_directory) / f"{model.name}.obj").unlink()
            O.export_scene.obj(
                filepath=str(setting.export_directory / f"{model.name}.obj"),
                use_selection=True,
            )

            for obj in model.imported_objects:
                obj.select_set(False)
        else:
            export.export_geometry_as_obj(
                model.object, model.name, setting.export_directory
            )

    def replace_with_standardized_material(self):
        """Replace the material with our pre-set RGB color or pre-set material"""
        object_material_dict = {
            "planes": {"material_name": "M.MP", "rgb_material": [0.5, 0.5, 1]},
            "glass": {"material_name": "M.MG", "rgb_material": [0, 0.63, 1]},
        }
        log.info(f"Reset the metallic and roughness to 0.0")
        for mat in model.object.data.materials:
            bsdf = mat.node_tree.nodes.get("Principled BSDF")
            if bsdf is not None:
                bsdf.inputs[4].default_value = 0.0
                bsdf.inputs[7].default_value = 0.0
        # deal with the main
        converted_material = convert_obj_material(
            model.object.name, material_name="M.0", export_color=[0.8, 0.8, 0.8]
        )
        model.object.material_slots[0].material = converted_material

        # deal with plane, glass, label
        log.info(
            f"Model name is {model.object.name}. Plane name is {model.planes}. Glass name is {model.glass}"
        )
        for sub_object in ["planes", "glass"]:
            material_name = object_material_dict[sub_object]["material_name"]
            rgb_material = object_material_dict[sub_object]["rgb_material"]
            for sub_mesh in getattr(model, sub_object):
                standardized_sub_material = convert_obj_material(
                    sub_mesh.name,
                    material_name=material_name,
                    export_color=rgb_material,
                )
                if len(sub_mesh.material_slots) < 1:
                    bpy.data.objects[sub_mesh.name].data.materials.append(
                        standardized_sub_material
                    )
                else:
                    sub_mesh.material_slots[
                        0
                    ].material = standardized_sub_material
        # mesh name should be the same as the object name
        for object in bpy.context.selected_objects:
            object_names = (
                [model.object.name]
                + [obj.name for obj in model.planes]
                + [obj.name for obj in model.glass]
            )
            if object.name in object_names:
                if object.data.name != object.name:
                    log.info(
                        f"object {object.name} has a mesh named {object.data.name}. I will change it"
                    )
                    object.data.name = object.name
                    log.info(
                        f"object {object.name} has a mesh named {object.data.name} now"
                    )

    def export_glb(self):
        """export object as glb file. TODO: ask if these are the right settings."""
        if not C.scene.world:
            log.info("no scene world is found.")
            C.scene.world = D.worlds[0]
        log.info(f"I found scene world! {C.scene.world}")
        C.scene.world.positionsPrecision = 8
        C.scene.world.normalsPrecision = 8
        C.scene.world.UVsPrecision = 8
        C.scene.world.vColorsPrecision = 8
        C.scene.world.mWeightsPrecision = 8
        C.scene.world.inlineTextures = False
        C.scene.world.usePBRMaterials = True
        C.scene.world.light_settings.use_ambient_occlusion = True
        # model.object.isPickable = True
        C.scene.world.textureDir = ""
        C.view_layer.objects.active = model.object
        model.object.select_set(True)
        output_model_name = [
            Path(x)
            for x in setting.file_paths
            if Path(x).suffix in keywords.supported_geometries
        ][0].stem
        log.info("Execute Export Geometry as GLB")
        log.info(f"the object name {model.object.name}")
        C.view_layer.objects.active = model.object
        model.object.select_set(True)
        output_model_name = [
            Path(x)
            for x in setting.file_paths
            if Path(x).suffix in keywords.supported_geometries
        ][0].stem
        O.export_scene.gltf(
            filepath=str(setting.export_directory / f"{output_model_name}.glb")
        )
        log.info("output glb file! done!")

    def export_babylon(self):
        """export geometry to babylon"""
        if not C.scene.world:
            log.info("no scene world is found.")
            C.scene.world = D.worlds[0]
        log.info(f"Scene world is {C.scene.world}")
        C.scene.world.positionsPrecision = 8
        C.scene.world.normalsPrecision = 8
        C.scene.world.UVsPrecision = 8
        C.scene.world.vColorsPrecision = 8
        C.scene.world.mWeightsPrecision = 8
        C.scene.world.inlineTextures = False
        C.scene.world.usePBRMaterials = True
        C.scene.world.light_settings.use_ambient_occlusion = True
        # model.object.isPickable = True
        C.scene.world.textureDir = ""
        C.view_layer.objects.active = model.object
        model.object.select_set(True)
        output_model_name = [
            Path(x)
            for x in setting.file_paths
            if Path(x).suffix in keywords.supported_geometries
        ][0].stem
        bpy.ops.export.bjs(
            filepath=str(
                setting.export_directory / f"{output_model_name}.babylon"
            )
        )
        log.info("output babylon file! done!")

    def execute(self) -> None:
        # safety reason
        unlink_all_objects_from_current_scene()
        if setting.re_bake:
            link_all_objects_to_scene(model.imported_objects)
        else:
            link_model_to_current_scene(model)
        # process
        self.replace_with_standardized_material()
        # actual export
        log.info(
            f"This is export task. And I need to know the x rotation is {model.object.rotation_euler[0]}"
        )
        if setting.export_geometry_as_obj:
            self.export_obj()

        if setting.export_geometry_as_glb:
            self.export_glb()

        if setting.export_geometry_as_babylon:
            self.export_babylon()

    def unit_test(self) -> None:
        """unit test"""
        temp_dict = {}
        executed = False
        try:
            self.execute()
            executed = True
        except Exception as e:
            log.debug(e)

        export_folder = (
            Path(setting.unit_testing_export_path)
            / "export_geometry"
            / "output"
        )
        temp_dict["files"] = []
        for file in Path(export_folder).iterdir():
            if Path(file).suffix in [".babylon", ".glb"]:
                file_name = str(Path(file).name)
                temp_dict["files"].append(file_name)

        # Export Unit Test Data
        if setting.unit_testing_stage != "TestExportGeometry":
            return False
        temp_dict[str(self.__class__.__name__)] = executed
        unit_test_file_path = (
            Path(setting.unit_testing_export_path)
            / "export_geometry"
            / "output"
            / "unit_test.json"
        )
        export.save_dict_as_json(temp_dict, unit_test_file_path)
