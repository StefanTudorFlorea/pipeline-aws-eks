""" Initial script of the rooomready blender addon. """
import sys, os
from pathlib import Path

os.environ["PYTHONIOENCODING"] = "utf-8"
sys.path.append(os.getcwd())
sys.stdout.reconfigure(encoding="utf-8")
module = Path(__file__).parent.resolve()
sys.path.append(str(module))

from app.cli import CommandLineParser
from app.core.pipeline import Pipeline
from app.core.setting import Setting
from app.core.log import Log

parser = CommandLineParser()
setting = Setting()
log = Log()

def handle_arguments():
    log.debug("handle_arguments")

    log.debug(str(sys.argv))
    import_found = False
    for index, mode in enumerate(sys.argv):
        if mode == "-cli" or mode == "--cli":
            # Set Settings by Command Line
            log.info("Import Settings by CLI")
            parser.parse_arguments()
            import_found = True

        elif (
            mode == "-settings"
            or mode == "--settings"
            or mode == "-setting"
            or mode == "--setting"
        ):
            # Set Settings by File

            settings_path = sys.argv[index + 1]
            if os.path.isdir(settings_path):
                for file in os.listdir(settings_path):
                    if "setting" in file or "settings" in file:
                        settings_path = Path(settings_path) / file

            file_type = os.path.splitext(settings_path)[1]

            if file_type == ".json":
                log.info("Import Settings by JSON")
                setting.import_json_from_file(settings_path)
                import_found = True

            elif file_type == ".txt":
                log.info("Import Settings by TXT File")
                setting.import_txt_from_file(settings_path)
                import_found = True

            elif file_type == ".yaml" or file_type == ".yml":
                log.info("Import Settings by YAML File")
                setting.import_yaml_from_file(settings_path)
                import_found = True

        elif mode == "--export_settings" or mode == "-export_settings":
            log.info("Export Settings as JSON")
            settings_path = sys.argv[index + 1]
            print("COMMAND PATHS: " + settings_path)
            if os.path.isdir(settings_path):
                setting.export_commands_json(settings_path)

        elif mode == "-test" or mode == "--unit_testing":
        #elif "-test" in mode:
            #setting.import_json_from_file("./examples/example_bake.json")
            import_found = True
    return import_found


def main():
    # debugpy.listen(5678)
    # print('Waiting for VS-Code debugger')
    # debugpy.wait_for_client()
    # print('Continue debugging')
    try:
        import_found = handle_arguments()

        if import_found:
            pipeline = Pipeline()
            if setting.export_polycount:
                pipeline.create_polycount_pipeline()
            # elif setting.re_bake:
            #     pipeline.create_real_estate_pipeline()
            elif setting.preview:
                pipeline.create_preview_pipeline()
            elif setting.rooomready:
                print("setting pipeline rooomready")
                pipeline.create_rooomready_pipeline()
            else:
                print("EXECUTE PIPELINE")
                pipeline.create_autobake_pipeline()
            print("EXECUTE PIPELINE")
            pipeline.execute()
        else:
            log.info("No pipeline")
            print("@--NO_PIPELINE@" + str(True))
            sys.stdout.flush()

    except Exception as e:
        log.info("Exception occured during main function")
        log.info(str(e))

    finally:
        pass
        # if not setting.export_directory:
        #     setting.export_directory = Path(setting.import_path) / "autobake"

        # if not os.path.isdir(setting.export_directory):
        #     os.mkdir(setting.export_directory)
        """
        if setting.export_log_type == "json":
            log.export_json(setting.export_directory)
        elif setting.export_log_type == "csv":
            log.export_csv(setting.export_directory)
        elif setting.export_log_type == "txt":
            log.export_txt(setting.export_directory)
        elif setting.export_log_type == "excel":
            log.export_excel(setting.export_directory)
        else:
            log.export_txt(setting.export_directory)
        """
        print("@--complete@" + str(True))
        sys.stdout.flush()

        # log.print_all_pretty()
        print("@END_PIPELINE")
        sys.stdout.flush()


if __name__ == "__main__":
    main()
