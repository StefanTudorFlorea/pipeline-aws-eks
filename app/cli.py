"""
	Following the proposed style guidelines from https://www.python.org/dev/peps/pep-0008/

	https://stackoverflow.com/questions/12826291/raise-two-errors-at-the-same-time
	https://stackoverflow.com/questions/2052390/manually-raising-throwing-an-exception-in-python

	from types import SimpleNamespace as Bunch #https://stackoverflow.com/a/45178188
	https://stackoverflow.com/questions/335695/lists-in-configparser
	https://stackoverflow.com/a/53274707
	#https://stackoverflow.com/a/48057478

	https://stackoverflow.com/questions/9916878/importing-modules-in-python-best-practice
	https://stackoverflow.com/questions/15727420/using-python-logging-in-multiple-modules
	https://stackoverflow.com/questions/14058453/making-python-loggers-output-all-messages-to-stdout-in-addition-to-log-file
	https://stackoverflow.com/questions/2777169/using-tabulation-in-python-logging-format
"""
import sys
import os
from argparse import ArgumentParser, _StoreTrueAction as StoreTrueAction, Namespace

import pathlib
import json
import configparser
import io

from app.core.setting import Setting
from app.core.log import Log
settings = Setting()
log = Log()

# https://stackoverflow.com/a/55597760
module = pathlib.Path(__file__).resolve().parents[1]
addons = module / "addons"
sys.path.append(str(pathlib.Path(__file__).resolve().parent))
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))


class CommandLineParser:
    def __init__(self):
        self.set_sys_arguments()
        self.arguments = sys.argv
        # log.info("Import Cli Arguments: "+"%s "*len(self.arguments), self.arguments)
        log.info("Import Cli Arguments: ", self.arguments)

    def set_sys_arguments(self):
        sys.argv = [
            pathlib.Path(__file__).resolve().name,
            *sys.argv[sys.argv.index("--") + 1 :],
        ]
        sys.argv = [el for el in sys.argv if el]
        sys.argv = [el for el in sys.argv if el != "\n"]

    def parse_arguments(self):
        arguments_dict = dict()
        for index, item in enumerate(sys.argv):
            log.info(f"index and item : {index} and {item}")
            if item.startswith("--"):
                if index != len(sys.argv) - 1 and not sys.argv[index + 1].startswith(
                    "--"
                ):
                    arguments_dict[item] = sys.argv[index + 1]
                else:
                    arguments_dict[item] = True
        settings.import_dict(arguments_dict)
