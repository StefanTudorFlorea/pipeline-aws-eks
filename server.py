from os import path, makedirs
from os import system
import logging
import time

import argparse
import json

from enum import Enum
from logging.handlers import RotatingFileHandler


from fastapi import FastAPI, Request, Response, File, Form, UploadFile, HTTPException
from fastapi.responses import HTMLResponse

# from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

# from utils import store, logs, get_log_formatter

from app.api_services.dispatcher import Dispatcher
from app.api_services.srv_types import (
    WebReadyArgs,
    PolycountArgs,
    PreflightArgs,
    RooomReadyArgs,
    ExtraOptions,
)
from app.api_services.srv_types import presets, DictModel, ObjectStates
from app.api_services.utilities import store, logs, get_log_formatter
from app.api_services.utilities import open_dir, split_id

from app.core.setting import Setting, parse_list
from app.core.log import Log
setting = Setting()
logger = Log()
# logger = logging.getLogger('Server')

import uvicorn

app = FastAPI()
# app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

open_dir(root_path=logs, keep_files=False)
open_dir(root_path=store, keep_files=False)
backlog = logs / "server.log"


@app.on_event("startup")
def startup():
    Dispatcher.get_instance().run_TMS()


setting_names = {
    "extras_name": "misc_name",
    "import_materials": "import_no_material",
    "import_merge_materials_format": "texture_format",
    "import_merge_materials_resolution": "texture_resolution",
    "import_merge_materials_bake_resolution": "import_bake_resolution",
    "import_merge_materials_island_margin": "island_margin",
    "import_join_geometry": "join_geometry",
    "geometry_resize_scale": "geometry_resize_value",
    "geometry_compact_max_cage_offset": "max_cage_offset",
    "geometry_compact_tilesize": "misc_tilesize",
    "geometry_compact_bake_margin": "bake_margin",
    "geometry_compact_samples": "misc_samples",
    "geometry_compact_target_polycount": "geometry_polygon_target",
    "materials_normalize_uv": "normalize_uv_space",
    "materials_validate_uvs": "validate_uvs",
    "materials_shadow": "bake_shadow",
    "export_edit_babylon": "misc_edit_babylon",
    "export_default_texture_format": "texture_default_format",
    "export_default_texture_resolution": "texture_default_resolution",
}


def args_to_cli(args, root_name=None):
    if args is None:
        return []
    if not isinstance(args, DictModel):
        raise "Use this function to convert fastAPI dict models to command line arguments"
    ret = []
    for k, v in args.__dict__.items():
        if k in ["objectId", "other"]:
            continue
        dict_name = k if root_name is None else f"{root_name}_{k}"
        if isinstance(v, DictModel):
            ret += args_to_cli(v, dict_name)
        else:
            setting_name = setting_names.get(dict_name, dict_name)
            if not hasattr(setting, setting_name):
                print(f"WARNING: not a settings key: {setting_name}")
            if isinstance(v, Enum):
                v = str(v.value)
            else:
                v = str(v)
            ret += [f"--{setting_name}", v]
    logger.debug("Created cli arguments")
    logger.info(ret)
    return ret


def get_cmds(args):
    # commands
    # 1. initialize with commands
    if args.commands is None:
        cmds = []
    else:
        cmds = args.commands
    # 2. evaluate preset and append cmds
    if args.preset is not None:
        cmds += [cmd for cmd in presets[args.preset]["operations"] if cmd not in cmds]
        for k, v in presets[args.preset]["settings"].items():
            if k not in cmds:
                cmds.append(k)
                cmds.append(v)
    # 3. override with json body
    root_names = ["import", "geometry", "materials", "export"]
    for i, dict_model in enumerate(
        [args.imports, args.geometry, args.materials, args.export]
    ):
        dict_args = args_to_cli(dict_model, root_names[i])
        for k, v in parse_list(dict_args).items():
            if k not in cmds:
                cmds.append(k)
                cmds.append(v)
    return cmds


def check_backend_args(args):
    # objectId set
    if args.extras.objectId is None:
        raise HTTPException(status_code=422, detail="ObjectId missing")
    # options set - FastAPI
    # options.domain set
    if args.extras.domain is None:
        raise HTTPException(status_code=422, detail="Domain missing")
    # options.token set
    if args.extras.authtoken is None:
        raise HTTPException(status_code=422, detail="Token missing")
    # extras ignored


def handle_zip(zipped, json_args):
    js = json.loads(json_args)
    if js is None:
        raise HTTPException(status_code=422, detail="json body missing")
    if zipped is None:
        raise HTTPException(status_code=422, detail="no zip file")
    return js


def load_log(path=None):
    if path in [None, ""]:
        path = "server.log"
    path = logs / path
    if path.exists() and path.is_file():
        with open(path, "rt") as log:
            return [l.rstrip() for l in log.readlines()]
    return None


@app.get("/log", status_code=200)
async def log(objectId: str, response: Response):
    log = load_log(f"{objectId}_log.txt")
    if log is None:
        response.status_code = 404
        return f"No log for '{objectId}' found"
    return log


@app.post("/rooomready", status_code=200)
async def add(args: RooomReadyArgs):
    check_backend_args(args)
    cmds = get_cmds(args)
    Dispatcher.get_instance().enqueue(
        mode="rooomready",
        id=args.extras.objectId,
        domain=args.extras.domain,
        token=args.extras.authtoken,
        callback_url=args.callback_url,
        cmds=cmds,
        extras=args.extras.server_data,
    )

@app.post("/webready", status_code=200)
async def add(args: WebReadyArgs):
    # preset valid enum member - FastAPI
    check_backend_args(args)
    cmds = get_cmds(args)
    Dispatcher.get_instance().enqueue(
        mode="autobake",
        id=args.extras.objectId,
        domain=args.extras.domain,
        token=args.extras.authtoken,
        callback_url=args.callback_url,
        cmds=cmds,
        extras=args.extras.server_data,
    )

@app.put("/rooomready", status_code=200)
async def add(zipped: UploadFile = File(...), json_args: str = Form(...)):
    js = handle_zip(zipped, json_args)
    args = RooomReadyArgs()
    args.update(js)
    cmds = get_cmds(args)
    args.extras["objectId"] = path.splitext(zipped.filename)[0]
    Dispatcher.get_instance().enqueue(
        mode="rooomready",
        id=args.extras["objectId"],
        domain=args.extras["domain"],
        token=args.extras["authtoken"],
        callback_url=args.callback_url,
        cmds=cmds,
        extras=args.extras.get("server_data", None),
        zip=zipped.file,
    )

@app.put("/webready", status_code=200)
async def add_zip(zipped: UploadFile = File(...), json_args: str = Form(...)):
    js = handle_zip(zipped,json_args)
    args = WebReadyArgs()
    args.update(js)
    cmds = get_cmds(args)
    args.extras["objectId"] = path.splitext(zipped.filename)[0]
    Dispatcher.get_instance().enqueue(
        mode="autobake",
        id=args.extras["objectId"],
        domain=args.extras["domain"],
        token=args.extras["authtoken"],
        callback_url=args.callback_url,
        cmds=cmds,
        extras=args.extras.get("server_data", None),
        zip=zipped.file,
    )

@app.put("/polycount", status_code=200)
async def add_polycount_zip(zipped: UploadFile = File(...), json_args: str = Form(...)):
    js = handle_zip(zipped, json_args)
    args = PolycountArgs()
    args.update(js)
    args.extras["objectId"] = path.splitext(zipped.filename)[0]
    Dispatcher.get_instance().enqueue(
        mode="polycount",
        id=args.extras["objectId"],
        domain=args.extras["domain"],
        token=None,
        callback_url=args.callback_url,
        extras=args.extras.get("server_data", None),
        zip=zipped.file,
    )


@app.post("/polycount", status_code=200)
async def add_polycount(args: PolycountArgs):
    check_backend_args(args)
    Dispatcher.get_instance().enqueue(
        mode="polycount",
        id=args.extras.objectId,
        domain=args.extras.domain,
        token=args.extras.authtoken,
        callback_url=args.callback_url,
        extras=args.extras.server_data,
    )


@app.put("/preview", status_code=200)
async def add_preview_zip(zipped: UploadFile = File(...), json_args: str = Form(...)):
    js = handle_zip(zipped, json_args)
    args = PreflightArgs()
    args.update(js)
    args.extras["objectId"] = path.splitext(zipped.filename)[0]
    Dispatcher.get_instance().enqueue(
        mode="preview",
        id=args.extras["objectId"],
        domain=args.extras["domain"],
        token=None,
        callback_url=args.callback_url,
        extras=args.extras.get("server_data", None),
        zip=zipped.file,
    )


@app.post("/preview", status_code=200)
async def add_preview(args: PreflightArgs):
    check_backend_args(args)
    Dispatcher.get_instance().enqueue(
        mode="preview",
        id=args.extras.objectId,
        domain=args.extras.domain,
        token=args.extras.authtoken,
        callback_url=args.callback_url,
        extras=args.extras.server_data,
    )





@app.get("/clear")
async def clear_storage():
    Dispatcher.get_instance().clear_db()
    open_dir(store, "Temp", keep_files=False)
    return "Cleared database but some processes might still be running"


@app.get("/status", status_code=200)
async def status():
    """
    Shows all currently processed items as json

    :return: Simple json dictionary with <id>: <status> pairs
    """
    data = {
        "status": "ok",
        "processing": Dispatcher.get_instance().active,
        "waiting": Dispatcher.get_instance().waiting_items(),
    }
    return data


@app.get("/ms_status", status_code=200)
async def ms_status():
    ret = Dispatcher.get_instance().get_ms_status()
    if ret is not None:
        ret['opt'] = await status()
    return ret


@app.get("/queues", status_code=200)
async def queues():
    """
    Shows all items which are currently processed

    :return: json dictionary
    """
    # return Dispatcher.get_instance().waiting_items(True)

    autobake = []
    polycount = []
    preview = []
    rooomready = []
    try:
        for row in (
            Dispatcher.get_instance()
            .storage.data.loc[:, ["ObjectId", "CalcTime", "Progress", "Status"]]
            .to_records(index=False)
        ):
            id, mode = split_id(row.ObjectId)
            progress = int(row.Progress * 100)
            if mode == "autobake":
                autobake.append(
                    {
                        "objectId": id,
                        "duration": row.CalcTime,
                        "percent": progress,
                        "inprogress": row.Status.value >= ObjectStates.WAITING.value
                        and row.Status.value < ObjectStates.SENT.value,
                    }
                )
            elif mode == "polycount":
                polycount.append(
                    {
                        "objectId": id,
                        "duration": row.CalcTime,
                        "percent": progress,
                        "inprogress": row.Status.value >= ObjectStates.WAITING.value
                        and row.Status.value < ObjectStates.SENT.value,
                    }
                )
            elif mode == "preview":
                preview.append(
                    {
                        "objectId": id,
                        "duration": row.CalcTime,
                        "percent": progress,
                        "inprogress": row.Status.value >= ObjectStates.WAITING.value
                        and row.Status.value < ObjectStates.SENT.value,
                    }
                )
            elif mode == "rooomready":
                rooomready.append(
                    {
                        "objectId": id,
                        "duration": row.CalcTime,
                        "percent": progress,
                        "inprogress": row.Status.value >= ObjectStates.WAITING.value
                        and row.Status.value < ObjectStates.SENT.value,
                    }
                )
    except Exception as err:
        return err

    return {
        "autobake_queue": autobake,
        "polycount_queue": polycount,
        "preview_queue": preview,
        "rooomready_queue": rooomready,
    }

@app.get("/info", status_code=200)
async def info():
    """
    Get info about the deployment

    :return: json dictionary with all relevant details about deployment
    """
    data = {
        "pipeline-id": "%%CI_PIPELINE_IID%%",
        "commit-short-sha": "%%CI_COMMIT_SHORT_SHA%%",
        "branch": "%%CI_COMMIT_BRANCH%%",
        "timestamp": "%%CI_COMMIT_TIMESTAMP%%"
    }
    return data

def main():
    parser = argparse.ArgumentParser(description="Run server.")
    parser.add_argument(
        "port", type=str, help="define the port on which the server runs", default=None
    )
    parser.add_argument(
        "--proxy_port", type=int, help="port used by docker image", default=10080, required=False
    )
    parser.add_argument(
        "--debug", action="store_true", help="run server in debug mode", default=False
    )
    parser.add_argument(
        "--development",
        action="store_true",
        help="run server in develop environment",
        default=False,
    )
    parser.add_argument(
        "--production",
        action="store_true",
        help="run server in develop environment",
        default=False,
    )
    parser.add_argument(
        "--TMS",
        type=str,
        required=False,
        default=None,
        help="TMS Server when using application as micro service",
    )
    arguments, unknown = parser.parse_known_args()

    # system(f"title Webready server")

    logger.info("Starting...")
    dispatcher = Dispatcher.get_instance(arguments.debug)

    if arguments.debug:
        uvicorn.run("server:app", host="127.0.0.1", port=int(arguments.port))
    else:
        if arguments.TMS is not None and arguments.TMS != "":
            dispatcher.setup_TMS(arguments.TMS, arguments.port, arguments.proxy_port)
        uvicorn.run("server:app", host="0.0.0.0", port=int(arguments.port))
    logger.info("Shutting down")
    dispatcher.shutdown()
    logger.info("Bye")


if __name__ == "__main__":
    main()
