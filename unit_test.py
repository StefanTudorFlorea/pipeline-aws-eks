# from app.unit_testing.pipeline_cases.template.test_template import TestTemplate
# from app.unit_testing.pipeline_cases.polycount.test_polycount import TestPolyCount
# from app.unit_testing.pipeline_cases.preflight_no_materials.test_preflight_no_materials import (
#     TestPreflightNoMaterials,
# )
from app.unit_testing.pipeline_tasks.import_data.test_import_data import (
    TestImportData,
)
from app.unit_testing.pipeline_tasks.categorize.test_categorize import (
    TestCategorizeData,
)
from app.unit_testing.pipeline_tasks.rotate.test_rotate import TestRotate
from app.unit_testing.pipeline_tasks.bake_shadow.test_bake_shadow import (
    TestBakeShadowForGroundPlane,
)
from app.unit_testing.pipeline_tasks.bake_shadowmap.test_bake_shadowmap import (
    TestBakeShadowForMesh,
)
from app.unit_testing.pipeline_tasks.export_textures.test_export_textures import (
    TestExportTextures,
)
from app.unit_testing.pipeline_tasks.export_geometry.test_export_geometry import (
    TestExportGeometry,
)

from app.unit_testing.pipeline_tasks.export_polycount.test_export_polycount import (
    TestExportPolyCount,
)

# from app.unit_testing.pipeline_cases.geometry_with_textures.test_geometry_with_textures import TestGeometryTextures
# from app.unit_testing.pipeline_cases.geometry_with_textures_glass.test_geometry_with_textures_glass import TestGeometryWithGlassTextures
# from app.unit_testing.pipeline_cases.geometry_with_triple_mask_textures.test_geometry_with_tm_textures import TestGeometryTripleMaskTextures
# from app.unit_testing.pipeline_cases.geometry_multiplanes.test_geometry_multiplanes import TestGeometryMultiplanes
# from app.unit_testing.pipeline_tasks.export_metadata.test_export_metadata import TestExportMetaData


def start_test():
    # print("========== ROOOMREADY CASE UNIT TEST ==========\n")

    # print("---------- TEMPLATE ----------")
    # test_template = TestTemplate()
    # test_template.unit_test()

    print("========== ROOOMREADY POLYCOUNT PIPELINE UNIT TEST ==========\n")

    print("---------- POLYCOUNT ----------")
    test_polycount = TestExportPolyCount()
    test_polycount.unit_test()

    # print("---------- PREFLIGHT NO MATERIALS ----------")
    # test_polycount = TestPreflightNoMaterials()
    # test_polycount.unit_test()

    print("========== ROOOMREADY PIPELINE UNIT TEST ==========\n")

    print("---------- IMPORT DATA ----------")
    test_import_data = TestImportData()
    test_import_data.unit_test()

    print("---------- CATEGORIZE ----------")
    test_categorize_data = TestCategorizeData()
    test_categorize_data.unit_test()

    print("----------  ROTATE   ----------")
    test_rotate_geometry = TestRotate()
    test_rotate_geometry.unit_test()

    print("----------  BAKE SHADOW  ----------")
    test_bake_shadow = TestBakeShadowForGroundPlane()
    test_bake_shadow.unit_test()

    print("----------  BAKE SHADOWMAP  ----------")
    test_bake_shadowmap = TestBakeShadowForMesh()
    test_bake_shadowmap.unit_test()

    print("----------  EXPORT TEXTURES  ----------")
    test_export_textures = TestExportTextures()
    test_export_textures.unit_test()

    print("----------  EXPORT GEOMETRY  ----------")
    test_export_geometry = TestExportGeometry()
    test_export_geometry.unit_test()

    # print("---------- GLASS OBJECT ----------")
    # test_geometry_glass = TestGeometryWithGlassTextures()
    # test_geometry_glass.unit_test()

    # print("---------- TRIPLE MASK TEXTURES ----------")
    # test_geometry_tm = TestGeometryTripleMaskTextures()
    # test_geometry_tm.unit_test()

    # print("---------- MULTIPLANES SINGLE FILE----------")
    # test_geometry_mp = TestGeometryMultiplanes()
    # test_geometry_mp.unit_test(False)

    # print("---------- MULTIPLANES SEPARATE FILES----------")
    # test_geometry_mp = TestGeometryMultiplanes()
    # test_geometry_mp.unit_test(True)

    # print("---------- EXPORT DATA ----------")
    # test_export_metadata = TestExportMetaData()
    # test_export_metadata.unit_test()


if __name__ == "__main__":
    start_test()
