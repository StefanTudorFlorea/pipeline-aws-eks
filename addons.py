from pathlib import Path, PurePath
from shutil import unpack_archive
import bpy

def install_addons():
    print("INSTALLING ADDONS")
    if bpy.app.background:
        file_path = Path(__file__)
    else:
        file_path = bpy.data.texts["addons.py"].filepath
    directory_path = PurePath(file_path).parent
    addons_path = directory_path / "lib"

    # uvpackmaster2
    bpy.ops.preferences.addon_install(
        filepath=str(addons_path.joinpath("uvpackmaster2-addon-2.5.6.zip"))
    )
    bpy.ops.preferences.addon_install(
        filepath=str(addons_path.joinpath("uvp-engine-pro-2.5.6.zip"))
    )
    bpy.ops.preferences.addon_enable(module="uvpackmaster2")
    unpack_archive(addons_path.joinpath("uvp-engine-pro-2.5.6.zip"), addons_path)
    bpy.ops.uvpackmaster2.select_uvp_engine(
        filepath=str(addons_path.joinpath("uvp-engine-pro-2.5.6/release-2.5.6.uvpini"))
    )

    # STEPer
    bpy.ops.preferences.addon_install(filepath=str(addons_path.joinpath("STEPper.zip")))
    bpy.ops.preferences.addon_enable(module="STEPper")

    # BabylonJS
    bpy.ops.preferences.addon_install(
        filepath=str(addons_path.joinpath("Blender2Babylon-2.93x.zip"))
    )
    bpy.ops.preferences.addon_enable(module="babylon_js")

    # save preference
    bpy.ops.wm.save_userpref()


def main():
    install_addons()


if __name__ == "__main__":
    main()
