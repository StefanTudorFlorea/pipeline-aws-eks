FROM  rooom/blender:latest

ENV BLENDERPIP "/bin/2.93/python/bin/pip3"
ENV BLENDERPY "/bin/2.93/python/bin/python3.9"

# Workaround to delete weird nvidia keys :)
RUN rm /etc/apt/sources.list.d/cuda.list
RUN rm /etc/apt/sources.list.d/nvidia-ml.list

RUN apt-get update

# create an app/ folder and copy the core python scripts to the app/
RUN mkdir /rooomready
COPY . /rooomready/.

# install pip in Blender python
RUN ${BLENDERPY} -m ensurepip
RUN ${BLENDERPIP} install --upgrade pip
RUN ${BLENDERPIP} install -r rooomready/app/requirements.txt

# To force use of system python
RUN blender --background --python rooomready/addons.py

RUN cd /bin/2.93/python/bin && ln -s python3.9 python


# set working directory
WORKDIR /rooomready

# Change PORT
EXPOSE 9999
CMD python server.py 9999 --proxy_port ${PROXY_PORT} --${ENVIRONMENT} --TMS "${TMS}"
